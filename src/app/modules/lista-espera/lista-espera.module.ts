import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaEsperaComponent } from '../lista-espera/components/lista-espera/lista-espera.component';
import { ListaEsperaRoutingModule } from './lista-espera-routing.module';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { FormsModule } from '@angular/forms';
import { FormularioListaEsperaComponent } from './components/formulario-lista-espera/formulario-lista-espera.component';
import { ClrDatagridModule} from "@clr/angular";
import { ClrIconModule} from "@clr/angular";
import { ClrFormsModule} from "@clr/angular";
import { ClrInputModule} from "@clr/angular";
import { ClrMainContainerModule} from "@clr/angular";
import { ClrSelectModule} from "@clr/angular";


@NgModule({
  imports: [
    CommonModule,
    NgxMaterialTimepickerModule.forRoot(),
    FormsModule,
    ListaEsperaRoutingModule,
    ClrDatagridModule,
    ClrIconModule,
    ClrFormsModule,
    ClrInputModule,
    ClrMainContainerModule,
    ClrSelectModule        

  ],
  declarations: [ListaEsperaComponent, FormularioListaEsperaComponent]
})
export class ListaEsperaModule { }
