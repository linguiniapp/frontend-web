import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioListaEsperaComponent } from './formulario-lista-espera.component';

describe('FormularioListaEsperaComponent', () => {
  let component: FormularioListaEsperaComponent;
  let fixture: ComponentFixture<FormularioListaEsperaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioListaEsperaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioListaEsperaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
