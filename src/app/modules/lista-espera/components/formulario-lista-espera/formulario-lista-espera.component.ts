import { Component } from '@angular/core';
import { ListaElement } from '../../services/lista-espera.service';
import { ListaEsperaService } from '../../services/lista-espera.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';


export interface Cantidad {
  value: number;
}

@Component({
  selector: 'formulario-lista-espera.',
  templateUrl: './formulario-lista-espera.component.html',
  styleUrls: ['./formulario-lista-espera.component.css'],
  exportAs: 'ngModel'
})
export class FormularioListaEsperaComponent  {
  
  complete: boolean = false;
  elementoListaEspera= new ListaElement();
  constructor(private routes:Router, private _listaEsperaServicio:ListaEsperaService,){}


  cantidad: Cantidad[] = [
    {value: 1},
    {value: 2},
    {value: 3},
    {value: 4},
    {value: 5},
    {value: 6},
    {value: 7},
    {value: 8},
  ];
  
  
  onSubmit(f: NgForm) {   
    this.elementoListaEspera.nombre=f.value.nombre+" "+f.value.apellido;
    this.elementoListaEspera.cantidad=f.value.cantidaD;
    this.elementoListaEspera.hora=f.value.hora; 
    this._listaEsperaServicio.agregarElementoLista(this.elementoListaEspera);
    this.routes.navigate(['/lista'])
  }
  regresarLista(){
    this.routes.navigate(['/lista'])
  }
}