import { Component,  OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ListaEsperaService, ListaElement } from '../../services/lista-espera.service';


@Component({
  selector: 'app-lista-espera',
  templateUrl: './lista-espera.component.html',
  styleUrls: ['./lista-espera.component.css']
})
export class ListaEsperaComponent implements OnInit  {
  dataSource:ListaElement[]=[];
  selectedUser: boolean;
  suscripcion: any;
  constructor(private router:Router,private actvateRoute:ActivatedRoute, private _listaEsperaService:ListaEsperaService){}

  ngOnInit() {
    this.suscripcion=this._listaEsperaService.getLista().subscribe(data => this.dataSource=data);    
    }
  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();    
  }  
  agregarElementoLista(){
    this.router.navigate(['/formularioLista']);
  }
  quitarElementoLista(idx:number){
  // this._listaEsperaService.quitarElementoLista(idx); expo
  this.dataSource.splice(idx-1,1);
  }
}

