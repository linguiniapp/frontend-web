import { Injectable } from '@angular/core';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Observable } from 'rxjs';
import { List } from 'lodash';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';


@Injectable()
export class ListaEsperaService {

  private ELEMENT_DATA = new Subject<ListaElement[]>();

  constructor(private apollo: Apollo) {
  }


  getLista() {
    this.apollo.watchQuery<any>({
      query: gql
        `{
            verListaEspera(numeroSucursal:1){
              detalles{
                edges{
                  node{
                    nombre
                    cantidadPersonas  
                    numero
                    fechaHoraLlegada
                  }
                }
              }
            }
          }
          `,fetchPolicy: 'no-cache'
    })
      .valueChanges
      .subscribe(({ data }) => {
        let ELEMENT_DATA$: ListaElement[] = [];

        data.verListaEspera.detalles.edges.forEach(r => {
          let lelem: ListaElement = {
            numero: r.node.numero,
            nombre: r.node.nombre,
            cantidad: r.node.cantidadPersonas,
            hora: r.node.fechaHoraLlegada,
          };
          let fechaHora=lelem.hora.split('T',2);
          fechaHora[1]=formatDate(fechaHora[0] + " " + fechaHora[1], "h:mm a", 'en-US');
          lelem.hora=fechaHora[1];
          ELEMENT_DATA$.push(lelem);

        }
        );
        this.ELEMENT_DATA.next(ELEMENT_DATA$);

      });

    return this.ELEMENT_DATA.asObservable();

  }

  agregarElementoLista(elemento: ListaElement) {
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
        agregarListaEspera(cantidadPersonas:${elemento.cantidad},
        nombre: "${elemento.nombre}",
        numeroSucursal:1) {
         ok
          }
        }`
        ,
      })
      .subscribe();
  }

  quitarElementoLista(x) {
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
          eliminarListaEspera(numeroLista:${x},numeroSucursal:1)
          {
            ok
          }
        }
        `
        ,
      })
      .subscribe();
  }


}
export class ListaElement {
  numero: number;
  nombre: string;
  cantidad: number;
  hora: string;
}