import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListaEsperaComponent } from './components/lista-espera/lista-espera.component';
import { FormularioListaEsperaComponent } from './components/formulario-lista-espera/formulario-lista-espera.component';
import { AuthService } from '../../security/login/auth.service';

import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';



const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'lista', 
                component: ListaEsperaComponent, canActivate: [AuthService],
                data: {acciones: ["ver_lista_espera"]}
            },{
                path: 'formularioLista', 
                component: FormularioListaEsperaComponent, canActivate: [AuthService],
                data: {acciones: ["agregar_lista_espera"]} 

            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ListaEsperaRoutingModule { }
