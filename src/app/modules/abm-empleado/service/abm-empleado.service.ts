import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular-boost';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { ClrTreeViewModule } from '@clr/angular';

@Injectable({
  providedIn: 'root'
})
export class AbmEmpleadoService {

  private EmpleadosResponse = new Subject<Empleado[]>();
  private EmpleadoResponse = new Subject<DatosEmpleado>();

  private response = new Subject<boolean>();
  private responseRol = new Subject<boolean>();
  private empleado : DatosEmpleado;

  constructor(private apollo: Apollo) {

  }

  guardarEmpleado(datosEmpleado,sucursal, password, username): Observable<boolean> {

    // , password: "${password}", username: "${username}"
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
      crearEmpleado(infoEmpleado: {
        nombre: "${datosEmpleado.nombre}", apellido: "${datosEmpleado.apellido}" , sexo: "${datosEmpleado.sexo}", email: "${datosEmpleado.email}", telefono: "${datosEmpleado.telefono}", fechaNacimiento: "${datosEmpleado.fechaNacimiento}", cuil: "${datosEmpleado.cuil}"
      }, numeroSucursal: ${sucursal}) {
        ok
     
      }
    }`
        ,
      })
      .subscribe(({ data }) => {

        if (data.crearEmpleado.ok == "0") {
          this.response.next(true);
        }
        else{

          this.response.next(false);
        }

      }, (error) => {

        this.response.next(false);

        console.log('there was an error sending the query', error);
      });

      return this.response.asObservable();
  }

 modificarEmpleado(datosEmpleado,sucursal, password, username): Observable<boolean> {

    // , password: "${password}", username: "${username}"
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
      crearEmpleado(infoEmpleado: {
        nombre: "${datosEmpleado.nombre}", apellido: "${datosEmpleado.apellido}" , sexo: "${datosEmpleado.sexo}", email: "${datosEmpleado.email}", telefono: "${datosEmpleado.telefono}", fechaNacimiento: "${datosEmpleado.fechaNacimiento}", cuil: "${datosEmpleado.cuil}"
      }, numeroSucursal: ${sucursal}) {
        ok
     
      }
    }`
        ,
      })
      .subscribe(({ data }) => {

        if (data.crearEmpleado.ok == "0") {
          this.response.next(true);
        }
        else{

          this.response.next(false);
        }

      }, (error) => {

        this.response.next(false);

        console.log('there was an error sending the query', error);
      });

      return this.response.asObservable();
  }

  asignarRol(codigoRol, empleado): Observable<boolean>{
    // , password: "${password}", username: "${username}"
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            asignarRol(codigoRol:${codigoRol}, numeroOCuilEmpleado: "${empleado}") {
                ok
                    }
           }`
        ,
      })
      .subscribe(({ data }) => {

        if (data.asignarRol.ok == "0") {
          this.responseRol.next(true);
        }
        else{

          this.responseRol.next(false);
        }

      }, (error) => {

        this.responseRol.next(false);

        console.log('there was an error sending the query', error);
      });

      return this.responseRol.asObservable();


  }


  getEmpleados(): Observable<Empleado[]> {
    this.apollo
      .watchQuery<any>({
        query: gql
          `{
            verEmpleados{
            edges{
              node{
                numero
                nombre
                apellido
                email
                telefono
                sucursal{
                  nombre
                }
              }
            }  
          }
            
          }`
      }).valueChanges
      .subscribe(({ data }) => {
        let empArray = new Array<Empleado>();
        let InfoAuxEmp: InfoAuxEmpleado;
        data.verEmpleados.edges.forEach(element => {
           let empleado: Empleado = {
             id: element.node.numero,
             apellido: element.node.apellido,
             nombre: element.node.nombre,
             email: element.node.email,
             telefono: element.node.telefono,
             sucursal: element.node.sucursal.nombre
          };

          empArray.push(empleado);
        });

        this.EmpleadosResponse.next(empArray);
      });

    return this.EmpleadosResponse.asObservable();

  }





  getUnEmpleados(numeroEmpleado): Observable<DatosEmpleado> {
    this.apollo
      .watchQuery<any>({
        query: gql
          `{
            verEmpleado(numeroOCuil: "${numeroEmpleado}"){
                nombre
                apellido
                email
                fechaNacimiento
                numero
                rolOid
                telefono
                cuil
                sexo
                sucursal{
                  nombre
                  numero
                }
              }
             
          
            
          }`
      }).valueChanges
      .subscribe(({ data }) => {
           let empleado: DatosEmpleado = {
            id : data.verEmpleado.numero,
             apellido:  data.verEmpleado.apellido,
             nombre:  data.verEmpleado.nombre,
             email: data.verEmpleado.email,
             telefono:  data.verEmpleado.telefono,
             sucursal:  data.verEmpleado.sucursal.numero,
             direccion: data.verEmpleado.direccion,
             fechaNacimiento: data.verEmpleado.fechaNacimiento,
             sexo: data.verEmpleado.sexo,
             cuil: data.verEmpleado.cuil,
             idRol: data.verEmpleado.rolOid
             
            }
             this.EmpleadoResponse.next(empleado);

        });


    return this.EmpleadoResponse.asObservable();

  }
}


export class DatosEmpleado {
  nombre: string;
  apellido: string;
  sexo: string;
  email: string;
  telefono: string;
  fechaNacimiento: string;
  cuil: string;
  direccion: string;
  sucursal: number;
  idRol: number;
  id: number;
}
export class Empleado {
  id: number;
  apellido: string;
  nombre: string;
  telefono: string;
  email: string;
  sucursal: string;
}
export class QueryClientes {
  infoaux: InfoAuxEmpleado;
  clientes: Empleado[];
}
export class InfoAuxEmpleado {
  endCursor: string;
  hasNextPage: boolean;
}