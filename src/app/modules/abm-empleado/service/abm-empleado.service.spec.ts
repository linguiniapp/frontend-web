import { TestBed } from '@angular/core/testing';

import { AbmEmpleadoService } from './abm-empleado.service';

describe('AbmEmpleadoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AbmEmpleadoService = TestBed.get(AbmEmpleadoService);
    expect(service).toBeTruthy();
  });
});
