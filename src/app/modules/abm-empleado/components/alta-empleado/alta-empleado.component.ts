import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';
import { roles, RolesService } from '../../../roles/services/roles.service';
import { SucursalService, ListaElement } from '../../../sucursales/services/sucursal.service';


import { AbmEmpleadoService, DatosEmpleado } from '../../service/abm-empleado.service';

@Component({
  selector: 'app-alta-empleado',
  templateUrl: './alta-empleado.component.html',
  styleUrls: ['./alta-empleado.component.css'],
  exportAs: 'ngModel'
})
export class AltaEmpleadoComponent implements OnInit {

  password: string = "";
  username: string = "";
  isInvalid: boolean = false;
  complete: boolean = false;
  datosEmpleado: DatosEmpleado = null;
  sucursales: ListaElement[] = [];


  sucursal: ListaElement = null;
  roles: roles[] = [];
  rol: roles = null;


  //datosEmpleado: DatosEmpleado;


  altaEmpleado = new FormGroup({
    nombre: new FormControl('', [
      Validators.required,

    ]),
    apellido: new FormControl('', [
      Validators.required,
    ]),
    direccion: new FormControl('', [
      Validators.required,
    ]),
    telefono: new FormControl('', [
      Validators.required,
    ]),
    sexo: new FormControl('', [
      Validators.required,
    ]),
    // usuario: new FormControl('', [
    //   Validators.required,
    // ]),
    cuil: new FormControl('', [
      Validators.required,
      // Validators.pattern("\(20|23|24|27|30|33|34)(\D)?[0-9]{8}(\D)?[0-9]"),

    ]),
    emailFormControl: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),

    date: new FormControl('', [
      Validators.required,
    ]),
  });

  constructor(private routes: Router, private abmEm: AbmEmpleadoService, private rolesService: RolesService, private sucursalService: SucursalService) {
  }
  sexo: Sexo[] = [
    { value: "Masculino", },
    { value: "Femenino", },
    { value: "Otro" }
  ];

  ngOnInit() {

    this.rolesService.getRol().subscribe(data => {

      this.roles = data;




    });


    this.sucursalService.getListaBasic().subscribe(data => {

      this.sucursales = data;

    });


    // this.empleado$ = this.emp.getEmpleados();
  }
  onSubmit(f: NgForm) {
    this.complete = false;
    if (f.valid && this.altaEmpleado.valid) {
      this.datosEmpleado = new DatosEmpleado();
      this.datosEmpleado.apellido = f.value.apellido;
      this.datosEmpleado.cuil = f.value.cuil;
      this.datosEmpleado.direccion = f.value.direccion;
      this.datosEmpleado.email = f.value.emailFormControl;
      this.datosEmpleado.fechaNacimiento = formatDate(f.value.date, "yyyy-MM-dd", "en-US");
      this.datosEmpleado.nombre = f.value.nombre;
      this.datosEmpleado.sexo = f.value.sexo;
      this.datosEmpleado.telefono = f.value.telefono;
      // this.password = f.value.password;
      // this.username = f.value.username;

      //this.abmEm.guardarEmpleado(this.datosEmpleado, this.password, this.username);


      this.abmEm.guardarEmpleado(this.datosEmpleado, this.sucursal,  this.password, this.username).subscribe(ok => {
        if (ok) {

          console.log("Ok Crear: " + ok)

          this.abmEm.asignarRol(this.rol, this.datosEmpleado.cuil).subscribe( ok => {
            if (ok) {
          console.log("Ok asignar: " + ok)
          this.routes.navigate(['/listaEmpleado']);
              
            } else {
              
            }
          })

        } else {

        }
      });



    }

    else {
      this.complete = true;
      this.validateAllFormFields(this.altaEmpleado);
    }

  }
  cerrar() {
    this.complete = !this.complete;
  }

  login() {
    this.routes.navigate(['/login']);

  }
  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
        control.updateValueAndValidity();
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
}
export interface Sexo {
  value: string;
}

