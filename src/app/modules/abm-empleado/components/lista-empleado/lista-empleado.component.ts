import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterModule } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { AbmEmpleadoService, Empleado } from '../../service/abm-empleado.service';


@Component({
  selector: 'app-lista-empleado',
  templateUrl: './lista-empleado.component.html',
  styleUrls: ['./lista-empleado.component.css']
})
export class ListaEmpleadoComponent implements OnInit {

  empleado: Empleado[] = [];
  empleadoDefault: Empleado[] = [];


  constructor(private routes: Router, private emp:AbmEmpleadoService ) { }

  ngOnInit() {
    this.emp.getEmpleados().subscribe(data => { 
    
    this.empleado = data;
    this.empleadoDefault = data;
    
    });
  }

  onVerPerfil(id:number){    
    this.routes.navigate(['/verEmpleado',id]);   
  }
  onEdit(id:number){    
    // console.log("/editarEmpleado"+ id)
    this.routes.navigate(['/modificarEmpleado',id]);   
  }

  registrar(){
    this.routes.navigate(['/altaEmpleado']);   

  }


  onSearchChange(busqueda : string ){

    this.empleado = this.empleadoDefault;

    this.empleado = this.empleado.filter( data => data.nombre.includes( busqueda));
        
    

  }

  onSearchCodChange(busqueda){

    this.empleado = this.empleadoDefault;


    this.empleado = this.empleado.filter( data => data.sucursal.includes(busqueda));
    
    
  }
}
