import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';
import { roles, RolesService } from '../../../roles/services/roles.service';
import { SucursalService, ListaElement } from '../../../sucursales/services/sucursal.service';
import { AbmEmpleadoService, DatosEmpleado } from '../../service/abm-empleado.service';
import { find } from 'async';


@Component({
  selector: 'app-modificar-empleado',
  templateUrl: './modificar-empleado.component.html',
  styleUrls: ['./modificar-empleado.component.css']
})
export class ModificarEmpleadoComponent implements OnInit {





  password: string = "";
  username: string = "";
  isInvalid: boolean = false;
  complete: boolean = false;
  datosEmpleado: DatosEmpleado = null;
  sucursales: ListaElement[] = [];
  idEmpleado: number = null;

  rol: roles = null;
  sucursal: ListaElement = null;
  roles: roles[] = [];

  //datosEmpleado: DatosEmpleado;


  editarEmpleado = new FormGroup({
    nombre: new FormControl('', [
      Validators.required,
    ]),
    apellido: new FormControl('', [
      Validators.required,
    ]),
    // direccion: new FormControl('', [
    //   Validators.required,
    // ]),
    telefono: new FormControl('', [
      Validators.required,
    ]),
    rol: new FormControl('', [
      Validators.required,
    ]),
    sexo: new FormControl('', [
      Validators.required,
    ]),
    // usuario: new FormControl('', [
    //   Validators.required,
    // ]),
    // cuil: new FormControl('', [
    //   Validators.required,
    // ]),
    emailFormControl: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    // password: new FormControl('', [
    //   Validators.required,
    //   Validators.minLength(8),
    // ]),
    date: new FormControl('', [
      Validators.required,
    ]),
  });

  constructor(private routes: Router, private abmEm: AbmEmpleadoService, private rolesService: RolesService, private sucursalService: SucursalService, private activatedRoute: ActivatedRoute) {
  }
  sexo: Sexo[] = [
    { value: "Masculino", },
    { value: "Femenino", },
    { value: "Otro" }
  ];

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      this.idEmpleado = params['id'];


      this.abmEm.getUnEmpleados(this.idEmpleado).subscribe(data => {


    this.rolesService.getRol().subscribe(dataRol => {
      this.roles = dataRol;
      this.rol = this.roles.find(r => r.codigo == data.idRol);

      console.log(data.idRol);
      console.log(this.rol);
    });


    this.sucursalService.getListaBasic().subscribe(dataS => {
      this.sucursales = dataS;

      console.log(data.sucursal);

      this.sucursal =  this.sucursales.find(s => s.numero == data.sucursal);
      console.log(this.sucursal);

      
    });
        this.editarEmpleado.setValue({
          nombre: data.nombre,
          apellido: data.apellido,
          date: data.fechaNacimiento,
          sexo: data.sexo,
          emailFormControl: data.email,
          telefono: data.telefono,
          usuario: "",
          rol: data.idRol,
          cuil: data.cuil,
          password: ""

        });



      });


    });





    // this.empleado$ = this.emp.getEmpleados();
  }
  onSubmit(f: NgForm) {
    this.complete = false;
    if (f.valid && this.editarEmpleado.valid) {
      this.datosEmpleado = new DatosEmpleado();
      this.datosEmpleado.apellido = f.value.apellido;
      this.datosEmpleado.cuil = ""; //f.value.cuil;
      this.datosEmpleado.direccion = f.value.direccion;
      this.datosEmpleado.email = f.value.emailFormControl;
      this.datosEmpleado.fechaNacimiento = formatDate(f.value.date, "yyyy-MM-dd", "en-US");
      this.datosEmpleado.nombre = f.value.nombre;
      this.datosEmpleado.sexo = f.value.sexo;
      this.datosEmpleado.telefono = f.value.telefono;
      // this.password = f.value.password;
      // this.username = f.value.username;

      //this.abmEm.guardarEmpleado(this.datosEmpleado, this.password, this.username);


      this.abmEm.modificarEmpleado(this.datosEmpleado, this.sucursal, this.password, this.username).subscribe(ok => {
        if (ok) {

          console.log("Ok Crear: " + ok)

          this.abmEm.asignarRol(this.rol, this.datosEmpleado.cuil).subscribe(ok => {
            if (ok) {
              console.log("Ok asignar: " + ok)
              this.routes.navigate(['/listaEmpleado']);

            } else {

            }
          })

        } else {

        }
      });



    }

    else {
      this.complete = true;
      this.validateAllFormFields(this.editarEmpleado);
    }

  }
  cerrar() {
    this.complete = !this.complete;
  }

  login() {
    this.routes.navigate(['/login']);

  }
  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
        control.updateValueAndValidity();
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
}
export interface Sexo {
  value: string;
}

