import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AltaEmpleadoComponent } from './components/alta-empleado/alta-empleado.component';
import { ListaEmpleadoComponent } from './components/lista-empleado/lista-empleado.component';
import { ModificarEmpleadoComponent } from './components/modificar-empleado/modificar-empleado.component';
import { BajaEmpleadoComponent } from './components/baja-empleado/baja-empleado.component';
import { VerEmpleadoComponent } from './components/ver-empleado/ver-empleado.component';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { AuthService } from '../../security/login/auth.service';


const routes: Routes = [
  {
      path: '',
      component: AuxviewComponent,
      children: [
          {
              path: 'altaEmpleado',
              component: AltaEmpleadoComponent, canActivate: [AuthService],
              data: {acciones: ["crear_empleado"]}
          },
          {
              path: 'bajaEmpleado',
              component: BajaEmpleadoComponent, canActivate: [AuthService],
              data: {acciones: ["deshabilitar_empleado"]}
          },
          {
            path: 'verEmpleado/:id',
            component: VerEmpleadoComponent, canActivate: [AuthService],
            data: {acciones: ["ver_empleado"]}
          },
          {
            path: 'modificarEmpleado/:id',
            component: ModificarEmpleadoComponent, canActivate: [AuthService],
            data: {acciones: ["modificar_empleado"]}
          },
          {
            path: 'listaEmpleado',
            component: ListaEmpleadoComponent, canActivate: [AuthService],
            data: {acciones: ["ver_empleados"]}
          }
      ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AbmEmpleadoRoutingModule { }
