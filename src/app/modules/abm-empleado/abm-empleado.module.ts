import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { ClarityModule, ClrFormsNextModule, ClrDatagridModule } from '@clr/angular';


import { AbmEmpleadoRoutingModule } from './abm-empleado-routing.module';
import { AltaEmpleadoComponent } from './components/alta-empleado/alta-empleado.component';
import { BajaEmpleadoComponent } from './components/baja-empleado/baja-empleado.component';
import { ModificarEmpleadoComponent } from './components/modificar-empleado/modificar-empleado.component';
import { ListaEmpleadoComponent } from './components/lista-empleado/lista-empleado.component';
import { VerEmpleadoComponent } from './components/ver-empleado/ver-empleado.component';

@NgModule({
  imports: [
    CommonModule,
    AbmEmpleadoRoutingModule,
    ClarityModule,
     ClrFormsNextModule,
     ClrDatagridModule,
     FormsModule,
     ReactiveFormsModule
  ],
  declarations: [AltaEmpleadoComponent, BajaEmpleadoComponent, ModificarEmpleadoComponent, ListaEmpleadoComponent, VerEmpleadoComponent]
})
export class AbmEmpleadoModule { }
