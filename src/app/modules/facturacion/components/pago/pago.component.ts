import { Component, OnInit } from '@angular/core';
import { PedidosService, ProductoCarta } from '../../../pedidos/services/pedidos.service';
import { FacturacionService, MedioPago, DatosFactura, Factura, DetalleFactura } from '../../services/facturacion.service';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { ImagenesService, Tarjeta, identification, cardholder, TarjetaResponse } from '../../../imagenes/services/imagenes.service';


@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css'],
  exportAs: 'ngModel'
})
export class PagoComponent implements OnInit {
  pago = new FormGroup({
    emailFormControl: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    nombre: new FormControl('', [
      Validators.required,
    ]),
    dni: new FormControl('', [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.minLength(8)

    ]),


    mes: new FormControl('', [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.maxLength(2),
      Validators.max(12)

    ]),

    ano: new FormControl('', [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.maxLength(2),
      Validators.min(18)

    ]),

    numero: new FormControl('', [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.minLength(8)

    ]),
    // medio: new FormControl('', [
    //   Validators.required,
    // ]),
    codigo: new FormControl('', [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.minLength(3),

      Validators.maxLength(4)
    ]),
  });
  tarjetaResponse: TarjetaResponse = null;
  pedidosSi: ProductoCarta[] = [];
  total: number = 0;
  reservaId: string;
  medio: Number = 0;
  visa: boolean = false;
  master: boolean = false;
  american: boolean = false;
  visaD: boolean = false;
  maestro: boolean = false;
  // credito: boolean = false;
  tipoPago: string = "";
  error: boolean = false;
  pause: boolean = false;
  confirmado: boolean = false;
  okFactura: boolean = false;
  factura: Factura = null;
  numero: string = "1";
  titular: string = "";
  email: string = "";
  codigo: string = "";
  mes: string = "";
  ano: string = "";
  documento: string = "";
  debito: boolean = false;
  constructor(private service: FacturacionService, private servicePedido: PedidosService, private ar: ActivatedRoute, private route: Router, private serviceRest: ImagenesService) {


    this.ar.params.subscribe(params => {
      this.reservaId = params['id'];
    });

  }

  ngOnInit() {

    // this.reservaId = localStorage.getItem("numeroPedido-reserva").split("-")[1];
    // this.numero = localStorage.getItem("numeroPedido-reserva").split("-")[0];

    this.servicePedido.getPedido(this.reservaId).subscribe(pedidos => {
      this.pedidosSi = [];
      this.numero = pedidos.idPedido.toString();
      this.pedidosSi = pedidos.producto;
      this.total = pedidos.total;

    });

  }
  cerrarCredito() {
    this.visa = false;
    this.maestro = false;
    this.master = false;
    this.american = false;
    this.visaD = false;
  }

  irMenu() {
    this.route.navigate(["/cliente-panel"])
  }
  pagar() {
    // this.credito = false;

    // this.pause = true;
    // setTimeout(() => {
    //   this.pause = false;

    //   this.confirmado = true;
    // }, 5000)

  }

  onSubmit(f: NgForm) {
    console.log(this.pago.value);
    this.pause = true;
    // debugger;
    if (this.pago.valid) {


      this.email = this.pago.value.emailFormControl;

      if (this.visa) {
        this.tipoPago = "Visa";
        this.medio = 2;
      } else if (this.visaD) {
        this.tipoPago = "Visa Débito";
        this.medio = 5;


      }
      else if (this.maestro) {
        this.tipoPago = "Maestro";
        this.medio = 6;

      }
      else if (this.master) {
        this.tipoPago = "Master Card";
        this.medio = 3;


      }
      else if (this.american) {
        this.tipoPago = "American Express";
        this.medio = 4;


      }




      let identification: identification = {
        type: "DNI",
        number: this.pago.value.dni
      }

      let cardholder: cardholder = {
        name: this.pago.value.nombre,
        identification: identification
      }
      let tarjeta: Tarjeta = {
        card_number: this.pago.value.numero,
        security_code: this.pago.value.codigo,
        expiration_month: this.pago.value.mes,
        expiration_year: Number("20" + this.pago.value.ano),
        cardholder: cardholder

      }
      // debugger;

      this.serviceRest.apiMercadoPago(tarjeta).then(r => {
        this.tarjetaResponse = r;

        if (!this.tarjetaResponse.luhn_validation || this.tarjetaResponse == undefined) {
          // debugger;
          this.pause = false;

          this.error = true;

        }
        else {

          let datos: DatosFactura = {

            tipoFactura: "D",
            tipoIdCliente: "dni",
            idCliente: this.pago.value.dni,
            nombre: this.pago.value.nombre,
            apellido: this.pago.value.nombre,
            email: this.pago.value.emailFormControl
          }

          let medio: MedioPago = {
            codigo: this.medio,
            importe: this.total.toString(),
            datosFactura: datos

          }

          // debugger;
          this.service.realizarPagoCliente(medio, this.numero).subscribe(data => {
            debugger;
            if (data) {
              this.pause = false;

              this.factura = data;

              this.okFactura = true;
            } else {
              this.pause = false;
              this.error = true;
            }


          });

        }
      }).catch(error => {
        console.log(error);

        this.pause = false;
        this.error = true;
      });




    } else {
      this.pause = false;
      // this.comple
    }
  }
  imprimirFactura() {
    window.print();
  }
  cerrarModal() {

    this.error = false;
    this.confirmado = false;
  }
}
