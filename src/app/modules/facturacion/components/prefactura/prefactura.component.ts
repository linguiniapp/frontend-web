import {Component, OnInit} from '@angular/core';
import {DTOPrefactura, FacturacionService} from '../../services/facturacion.service';
import {PedidosService, ProductoCarta} from '../../../pedidos/services/pedidos.service';
import {Router} from '@angular/router';


@Component({
    selector: 'app-prefactura',
    templateUrl: './prefactura.component.html',
    styleUrls: ['./prefactura.component.css']
})
export class PrefacturaComponent implements OnInit {

    pedidosSi: ProductoCarta[] = [];
    total: number;
    reservaId: any;
    estadia: DTOPrefactura = {
        mesas: null,
        total: null,
        reserva: null,
        cliente: null,
        detalles: null,
        mozo: null,
        sucursal: null
    };

    constructor(private service: FacturacionService, private servicePedido: PedidosService, private route: Router) {
    }

    ngOnInit() {
        // alert(this.service.emitirPrefactura(1,1,false))

        this.reservaId = localStorage.getItem('numeroPedido-reserva').split('-')[1];

        this.service.getEstadias(this.reservaId).subscribe(estadia => this.estadia = estadia);


        this.servicePedido.getPedido(this.reservaId).subscribe(pedidos => {
            this.pedidosSi = [];
            this.pedidosSi = pedidos.producto;
            this.total = pedidos.total;

        });

    }

    pagarPedido() {
        this.route.navigate(['/pago']);
    }

}
