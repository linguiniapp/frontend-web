import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { PrefacturaComponent } from './components/prefactura/prefactura.component';
import { PagoComponent } from './components/pago/pago.component';


const routes: Routes = [
  {
      path: '',
      component: AuxviewComponent,
      children: [
          {
              path: 'estadia',
              component: PrefacturaComponent
          },          {
            path: 'pago/:id',
            component: PagoComponent
        },
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacturacionRoutingModule { }
