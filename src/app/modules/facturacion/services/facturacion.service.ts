import { Injectable } from '@angular/core';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FacturacionService {
  private factura = new Subject<Factura>();

  private prefacturaDTO: DTOPrefactura = {
    mesas: null,
    total: null,
    reserva: null,
    cliente: null,
    detalles: null,
    mozo: null,
    sucursal: null
  };
  private _prefacturaDTO = new Subject<DTOPrefactura>();

  constructor(private apollo: Apollo) {
  }

  realizarPagoCliente(medioPago: MedioPago, numeroPedidos): Observable<Factura> {
    debugger;
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
              realizarPagoCliente(medioPago: {codigo: ${medioPago.codigo}, importe: "${medioPago.importe}", datosFactura: {tipoFactura: "${medioPago.datosFactura.tipoFactura}", tipoIdCliente: "dni", idCliente: "${medioPago.datosFactura.idCliente}", nombre: "${medioPago.datosFactura.nombre}", apellido: "${medioPago.datosFactura.apellido}", email: "${medioPago.datosFactura.email}"}}, numerosPedidos: [${numeroPedidos}]) {
                ok
                factura {
                  fecha
                  numero
                  total
                  cuitEmpresa
                  nombreCliente
                  apellidoCliente
                  detalles {
                    edges {
                      node {
                        cantidad
                        precioUnitario
                        subtotal
                        descripcion
                      }
                    }
                  }
                }
              }
            }
            `, fetchPolicy: 'no-cache'
      })
      .subscribe(({ data }) => {
        debugger;
        if (data.realizarPagoCliente.ok == 0) {
          var datos = data.realizarPagoCliente.factura;
          let detallesList: DetalleFactura[];
          datos.detalles.edges.forEach(element => {
            let detalles: DetalleFactura = {
              cantidad: element.cantidad,
              precioUnitario: element.precioUnitario,
              subtotal: element.subtotal,
              descripcion: element.subtotal
            }
            detallesList.push(detalles);
          });

          let factura: Factura = {
            fecha: datos.fecha,
            numero: datos.numero,
            total: datos.total,
            cuitEmpresa: datos.cuitEmpresa,
            nombreCliente: datos.nombreCliente,
            apellidoCliente: datos.apellidoCliente,
            detalles: detallesList

          }

          this.factura.next(factura);
        }
        else {

          this.factura.next(undefined);

        }
      }, (error) => {
        this.factura.next(undefined);

        console.log('error', error);
      });
    return this.factura.asObservable();

  };


  emitirPrefactura(numeroMesa, numeroSucursal, verificarMozo): DTOPrefactura {
    // console.log(elemento.mesas);
    this.apollo
      .mutate({
        mutation: gql
          `mutation{
            verEstadia(numeroMesa: ${numeroMesa}, numeroSucursal: ${numeroSucursal}, verificarMozo: ${verificarMozo}){
              ok
              prefactura{
                total
                mesas
                estadia{
                  mozo{
                    nombre
                    apellido
                  }
                  reserva{
                    numero
                    cliente{
                      nombre
                      apellido
                    }
                  }
                  sucursal{
                    nombre
                  }
                }
                detalles{
                  nombre
                  cantidad
                  precio
                  subtotal
                }
              }
            }
          }`
        ,
      })
      .subscribe(({ data }) => {
        if (data.verEstadia.ok = 0) {
          let pre = data.verEstadia.prefactura;
          let detalleList: DTOPreFacturaDetalleNode[];
          pre.detalles.array.forEach(element => {
            let dto: DTOPreFacturaDetalleNode = {

              cantidad: element.cantidad,
              nombre: element.nombre,
              precio: element.precio,
              subtotal: element.subtotal
            };

            detalleList.push(dto);
          });
          let prefactura: DTOPrefactura = {
            sucursal: pre.estadia.sucursal.nombre,
            cliente: pre.estadia.reserva.cliente.nombre + ' ' + pre.estadia.reserva.cliente.apellido,
            total: pre.total,
            mozo: pre.estadia.mozo.nombre + ' ' + pre.estadia.mozo.apellido,
            reserva: pre.estadia.reserva.numero,
            mesas: pre.mesas,
            detalles: detalleList
          };
          console.log(prefactura);
          this.prefacturaDTO = prefactura;

        } else {
          alert('error');
        }
      }, (error) => {
        console.log('error', error);
      });

    return this.prefacturaDTO;

  };

  getEstadias(numeroReserva: number): Observable<DTOPrefactura> {
    this.apollo.watchQuery<any>({
      query: gql`{
  verPedido(numeroPedido:1) {   
        detalles {
          edges {
            node {
              cantidad
              cartaProducto {
                precio
              }
            }
          }
        }
        promociones {
          edges {
            node {
              cantidad
              promocion {
                precio
              }
            }
          }
        }
        estadia {
          mozo {
            nombre
            apellido
          }
          clientes {
            edges {
              node {
                nombre
                apellido
              }
            }
          }
          cantidadPersonas
          numero
          mesas {
            edges {
              node {
                numero
              }
            }
          }
          fechaHoraLlegada
        }
      }
    }
`, fetchPolicy: 'no-cache'
    })
      .valueChanges
      .subscribe(({ data }) => {

        let total = 0;
        let listaProductos: Producto[] = [];
        let listaPromociones: Promocion[] = [];
        data.verPedido.detalles.edges.forEach(prod => {
          let producto: Producto = { precio: null, cantidad: null };
          producto.cantidad = prod.node.cantidad;
          producto.precio = prod.node.cartaProducto.precio;
          listaProductos.push(producto);
        });
        data.verPedido.promociones.edges.forEach(prom => {
          let promocion: Promocion = { precio: null, cantidad: null };
          promocion.cantidad = prom.node.cantidad;
          promocion.precio = prom.node.promocion.precio;
          listaPromociones.push(promocion);
        });
        let nombreCliente = '';
        data.verPedido.estadia.clientes.edges.forEach(cliente => {
          nombreCliente = cliente.node.nombre + ' ' + cliente.node.apellido;
        });
        this.prefacturaDTO.cliente = nombreCliente;
        this.prefacturaDTO.reserva = numeroReserva;
        if (listaProductos) {
          listaProductos.forEach(producto => {
            total += producto.cantidad * producto.precio;
          });
        }
        if (listaPromociones) {
          listaPromociones.forEach(producto => {
            total += producto.cantidad * producto.precio;
          });
        }
        this.prefacturaDTO.total = total;

        this.prefacturaDTO.mesas = data.verPedido.estadia.mesas.edges[0].node.numero;

        this._prefacturaDTO.next(this.prefacturaDTO);
      });

    return this._prefacturaDTO;
  }


}

export interface DTOPrefactura {
  sucursal: string;
  cliente: string;
  mozo: string;
  reserva: number;
  total: number;
  mesas: number[];
  detalles: DTOPreFacturaDetalleNode[]
}

export interface DTOPreFacturaDetalleNode {
  nombre: string;
  cantidad: number;
  precio: string;
  subtotal: string;
}

export interface MedioPago {
  codigo: Number;
  importe: String;
  datosFactura: DatosFactura;

}

export interface DatosFactura {
  tipoFactura: String;
  tipoIdCliente: String;
  idCliente: String;
  nombre: String;
  apellido: String;
  email: String;
}

export interface Factura {
  fecha: String;
  numero: Number;
  total: Number;
  cuitEmpresa: String;
  nombreCliente: String;
  apellidoCliente: String;
  detalles: DetalleFactura[];
}


export interface DetalleFactura { 
  cantidad: Number;
  precioUnitario: Number;
  subtotal: Number;
  descripcion: String;
}

export interface Producto {
  cantidad: number;
  precio: number;

}

export interface Promocion {
  cantidad: number;
  precio: number;
}