import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacturacionRoutingModule } from './facturacion-routing.module';
import { PrefacturaComponent } from './components/prefactura/prefactura.component';
import { PagoComponent } from './components/pago/pago.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule, ClrFormsNextModule } from '@clr/angular';



@NgModule({
  imports: [
    CommonModule,ClarityModule,ClrFormsNextModule, FormsModule, ReactiveFormsModule,
    FacturacionRoutingModule
  ],
  declarations: [PrefacturaComponent, PagoComponent]
})
export class FacturacionModule { }


