import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { PedidosService, Pedido, ProductoCarta, Promocion, Recompensa } from '../../services/pedidos.service';
import { VerCartaService, Producto } from '../../../carta/services/ver-carta.service';

@Component({
  selector: 'app-carrito-pedido',
  templateUrl: './carrito-pedido.component.html',
  styleUrls: ['./carrito-pedido.component.css']
})
export class CarritoPedidoComponent implements OnInit {
  @Input() pedido: ProductoCarta;
  @Input() promocion: Promocion;
  @Input() recompensa: Recompensa;
  @Input() reserva: number;
  @Output() evento = new EventEmitter<Number[]>();
  @Output() delete = new EventEmitter<Number>();
  // @Output() modificar = new EventEmitter<Number>();


  producto: ProductoCarta;
  idProducto: number = 0;
  imagen: String = "";
  eliminar: boolean = false;
  descripcion: string = "";
  basic: boolean = false;
  constructor(private route: Router, private service: PedidosService, private _cartaService: VerCartaService) {

  }

  ngOnInit() {
  }

  verProducto(id: number) {
    this.producto = this.pedido; //.producto.find(p => p.idProducto == id);
    this._cartaService.getProducto(id).subscribe(p => {
      this.imagen = p.imagen;
      this.descripcion = p.descripcion
    });
    this.basic = true;
  }

  eliminarPlato(idPedido) {
    this.eliminar = true;
    this.idProducto = idPedido;
    // this.producto =  null;

    // this.service.eliminarPlato();
  }

  borrar() {
    // this.eliminar = false;
    // this.delete.emit(this.idProducto);

    var stat = this._cartaService.modificarProducto(this.idProducto, 0, this.reserva).subscribe(ok => {
      if (ok) {
        this.eliminar = false;
        this.delete.emit(this.idProducto);


      } else {

      }

    });

  }

  addCart(prod) {


    var stat = this._cartaService.modificarProducto(prod.idProducto, this.producto.cantidad, this.reserva).subscribe(ok => {
      if (ok) {
        // alert("ok")
        this.route.navigate(["verCarrito", this.reserva]);
        this.producto = null;
      } else {

      }

    });

    if (stat) {
      // this.validateBtnState = ClrLoadingState.SUCCESS;
      // this.evento.emit([this.service.getCantProduct(), this.service.getTotal()]);

    }
  }

  addProductCant() {
    let cantOld = this.producto.cantidad;
    this.producto.cantidad = this.producto.cantidad + 1;
    //uno representa agrega
    this.evento.emit([1, this.producto.montoPesos * (this.producto.cantidad - cantOld)]);


  }

  substracProductCant() {
    if (this.producto.cantidad == 0) {
      return;
    }
    else {

      let cantOld = this.producto.cantidad;

      this.producto.cantidad = this.producto.cantidad - 1;


      //cero representa substacion
      this.evento.emit([0, this.producto.montoPesos * (cantOld - this.producto.cantidad)]);

    }
  }
}
