import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Pedido, PedidosService, ProductoCarta, Promocion, Recompensa} from '../../services/pedidos.service';
import {VerCartaService} from '../../../carta/services/ver-carta.service';
import {Observable} from 'rxjs';
import {SucursalService} from '../../../sucursales/services/sucursal.service';


@Component({
    selector: 'app-carrito',
    templateUrl: './carrito.component.html',
    styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {


    public reservaId: number = 0;
    pedidos = new Observable<Pedido[]>();
    estadia: boolean = false;
    confirmado: boolean = false;
    correcto1: boolean = false;
    pedidosSi: ProductoCarta[] = [];
    promociones: Promocion[] = [];
    recompensas: Recompensa[] = [];
    comentario: string = '';
    total: number = 0;

    constructor(private route: Router, private actvateRoute: ActivatedRoute, private service: PedidosService,
                private _cartaService: VerCartaService, private _sucursalService: SucursalService) {
        this.reservaId = this._cartaService.reservaId;
    }

    ngOnInit() {
        this.actvateRoute.params.subscribe(params => {
            this.reservaId = params['idReserva'];


            //   .forEach(pedido => {
            //   console.log("pedidos " + number + pedido.idPedido);
            //   number += 1;
            //   this.total += pedido.total;
            // }));


        });

        this._sucursalService.tieneEstadia(this.reservaId).subscribe(tieneEstadia => this.estadia = tieneEstadia);




        this.service.getPedido(this.reservaId).subscribe(pedidos => {
            this.pedidosSi = [];
            this.promociones = [];
            this.recompensas = [];
            this.pedidosSi = pedidos.producto;
            this.promociones = pedidos.promocion;
            this.recompensas = pedidos.recompensa;
            this.total = pedidos.total;
            this.comentario = pedidos.comentario;

            //       this.pedidosSi=pedidos;
            // console.log("pedidosSi"+ this.pedidosSi);
            // console.log("pedidos"+pedidos);
            //     pedidos.forEach(p => {
            //       this.total = p.total;
            //       });
            // this.pedidosSi.forEach( o => {
            //   console.log(o.producto.forEach(a => {
            //     a.nombre
            //   }) );
            // })

        });
    }

    guardarComentario(comentario: string) {
        this._cartaService.modificarProductoComentario(this.reservaId, comentario);

    }

    checkin() {
        this.route.navigate(['/check-in']);
    }

    volver() {
        this.route.navigate(['/listaCarta', 1]);
    }

    verProducto(id) {
        this.route.navigate(['/verProductos']);
    }

    confirmarPedido() {
        let numeroPedido = localStorage.getItem('numeroPedido-reserva').split('-')[0];

        this.service.confirmarPedido(numeroPedido).subscribe(ok => {

            localStorage.setItem('pedidoConfirmado', numeroPedido);
            if (ok) {
                this.confirmado = true;
            } else {
                this.confirmado = true;

            }
        });
    }

    verEstadia() {
        this.route.navigate(['/estadia']);
    }


    updateCant($event) {
        if ($event[0] == 1) {
            this.total = this.total + $event[1];
        } else {
            console.log('else updateCant');

            console.log($event[1]);
            this.total = this.total - $event[1];

        }
        // let prod = this.pedidosSi.find( p => p.idPedido == $event[0]);
        // prod.


    }

    agregar() {
        let reserva = localStorage.getItem('numeroPedido-reserva').split('-')[1];
        let sucursal = localStorage.getItem('Pedidosucursal');
        this.route.navigate(['verCartaGeneral', reserva, sucursal]);
    }

    deleteProducto($event) {

        let prod = this.pedidosSi.findIndex(p => p.idProducto == $event);
        let total = 0;

        let producto = this.pedidosSi[prod];
        total = producto.cantidad * producto.montoPesos;

        this.pedidosSi.splice(prod, 1);
        this.total = this.total - total;

        // this.pedidosSi.forEach(ped => {

        //   if (total == 0) {

        //     total = ped.prodfind(p => p.idProducto == $event).cantidad * ped.producto.find(p => p.idProducto == $event).montoPesos;
        //     console.log("es el total " + total)
        //   }
        //   ped.producto.splice(prod, 1);

        // })

        // this.total = this.total - total;
    }

    deletePromocion($event) {
        let prom = this.promociones.findIndex(p => p.id == $event);
        let total = 0;

        let promocion = this.promociones[prom];
        total = promocion.cantidad * promocion.monto;

        this.promociones.splice(prom, 1);
        this.total = this.total - total;
    }

    deleteRecomenpensa($event) {
        let recom = this.recompensas.findIndex(p => p.id == $event);


        let recompensa = this.recompensas[recom];

        this.recompensas.splice(recom, 1);
    }
}

