import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoPedidoPresencialComponent } from './nuevo-pedido-presencial.component';

describe('NuevoPedidoPresencialComponent', () => {
  let component: NuevoPedidoPresencialComponent;
  let fixture: ComponentFixture<NuevoPedidoPresencialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoPedidoPresencialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoPedidoPresencialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
