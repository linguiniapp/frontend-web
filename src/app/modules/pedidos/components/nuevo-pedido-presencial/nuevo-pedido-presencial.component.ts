import {Component, OnInit} from '@angular/core';
import {
    NuevoPedido,
    Producto,
    ProductoPedido,
    VerCartaService
} from '../../../carta/services/ver-carta.service';
import {categoria, ProductosService, SelectedCarta} from '../../../productos/services/productos.service';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Mesa, SucursalService} from '../../../sucursales/services/sucursal.service';
import {formatDate} from '@angular/common';
import {PedidosService} from '../../services/pedidos.service';


@Component({
    selector: 'app-nuevo-pedido-presencial',
    templateUrl: './nuevo-pedido-presencial.component.html',
    styleUrls: ['./nuevo-pedido-presencial.component.css'],
    exportAs: 'ngModel'
})
export class NuevoPedidoPresencialComponent implements OnInit {
    dataSource: Producto[];
    creado: boolean = false;
    dataSourceDefault: Producto[];
    listaCategoria: categoria[] = [];
    selectedProd: Producto[] = [];
    select: SelectedCarta[] = [];
    correcto: boolean = false;
    codCategoria: number = 1;
    complete: boolean = false;
    busqueda: string = '';
    error: boolean = false;
    pedidos: NuevoPedido[];
    completeCantidad: boolean = false;
    numeroSucursal: number = null;
    observableCategoria: any = null;
    seleccionarMesa: boolean = true;
    numeroMesa: number = 0;
    items: Mesa[] = [];
    urlMesa: string = '../../../../../assets/silla.png';
    urlMesaRed: string = '../../../../../assets/sillaRed.png';
    urlMesaGreen: string = '../../../../../assets/sillaGreen.png';
    urlMesaYellow: string = '../../../../../assets/sillaYellow.png';

    constructor(private serviceCarta: VerCartaService, private routes: Router,
                private activateRoute: ActivatedRoute, private serviceProducto: ProductosService,
                private serviceSucursal: SucursalService, private pedidosService: PedidosService,
                private verCartaService: VerCartaService) {
    }

    ngOnInit() {
        // this.pedidosService.getPedidos().subscribe(data => this.pedidos = data);
        this.activateRoute.params.subscribe(params => {
            this.numeroSucursal = params['id'];
        });
        this.selectedProd = [];
        let fechaHoy = new Date();
        let fecha = formatDate(fechaHoy, 'yyyy-MM-dd', 'en-US');
        let hora = formatDate(fechaHoy, 'HHmmss', 'en-US');
        let fechaHora = fecha + 'T' + hora;
        this.serviceSucursal.getMesas(fechaHora, this.numeroSucursal).subscribe(data => {
            this.items = [];
            this.items = data;
            this.items.forEach(item => {
                if (item.tieneReserva) {
                    item.stat = 2;
                }
                if (item.noTienePedido === false) {
                    item.stat = 0;
                } else {
                    item.stat = 1;
                }
            });
        });


        this.observableCategoria = this.serviceProducto.getCategoria();
        this.observableCategoria.subscribe(data => {
            this.listaCategoria = data;
            let number: number = 0;
            this.listaCategoria.forEach(categoria => {
                number = number + 1;
            });
        });
        this.serviceCarta.getProductos(this.codCategoria, this.numeroSucursal).subscribe(data =>
            this.dataSource = data
        );
        this.serviceCarta.getProductos(this.codCategoria, this.numeroSucursal).subscribe(data =>
            this.dataSourceDefault = data
        );
    }

    onSubmit(f: NgForm) {
        let pedido: NuevoPedido = {mesa: 0, numeroSucursal: 0, productos: []};
        this.selectedProd.forEach(data => {
            this.complete = false;
            this.completeCantidad = false;
            this.error = false;
            if (f.valid) {
                if (this.selectedProd.length === 0) {
                    this.complete = true;
                } else {
                    let producto: ProductoPedido = {numero: 0, cantidad: 0};
                    producto.cantidad = f.value.cantidad;
                    producto.numero = data.id;
                    pedido.productos.push(producto);
                }
            }
        });
        this.verCartaService.agregarPedidoPresencial(pedido.productos, this.numeroMesa, this.numeroSucursal).subscribe(ok => {

            if (ok) {
                this.creado = true;
            } else {

            }

        });
    }


    cerrarCorrecto() {
        this.correcto = false;
    }

    volver() {
        this.seleccionarMesa = true;
    }

    seleccionarMesas(mesa: Mesa) {
        if (mesa.noTienePedido !== false) {
            this.seleccionarMesa = false;
            this.numeroMesa = mesa.id;
        }
    }

    onSearchChange(busqueda: string) {

        this.dataSource = this.dataSourceDefault;

        this.dataSource = this.dataSource.filter(data => data.nombre.includes(busqueda));


    }

    onSearchCodChange(busqueda) {

        this.dataSource = this.dataSourceDefault;

        if (busqueda !== undefined && busqueda !== '') {

            this.dataSource = this.dataSource.filter(data => data.id === busqueda);
        }

    }


    categoriaChange() {
        this.serviceCarta.getProductos(this.codCategoria, this.numeroSucursal).subscribe(data =>
            this.dataSource = data
        );
    }

    selected(codigo) {
        let prod = this.select.find(d => d.codigo === codigo);
        return !!prod;
    }

    salir() {
        this.routes.navigate(['/']);
    }

    cerrarModal() {
        this.creado = false;
        this.seleccionarMesa = true;
        this.selectedProd = [];
    }

}
