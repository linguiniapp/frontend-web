import { Component, OnInit } from '@angular/core';
import { PedidosService, PedidoCocina } from '../../services/pedidos.service';
import { ActivatedRoute } from '@angular/router';
import { interval } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
@Component({
  selector: 'app-ver-pedidos',
  templateUrl: './ver-pedidos.component.html',
  styleUrls: ['./ver-pedidos.component.css']
})
export class VerPedidosComponent implements OnInit {
  pedidos: PedidoCocina[];
  numeroSucursal: number;
  selectedProd: any;
  suscripcion: any;
  constructor(private pedidosService: PedidosService, private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(params => {
      this.numeroSucursal = params['sucursal'];
    });
    // this.pedidosService.getPedidos(this.numeroSucursal).pipe(switchMap(() =>,map(res => console.log(res))      )
    let inter = interval(5000);
    this.suscripcion = inter.pipe(switchMap(() => this.pedidosService.getPedidos(this.numeroSucursal))).subscribe(data => {
      this.pedidos = data;
    });


  }
  ngOnDestroy() {
    this.suscripcion.unsubscribe();

  }

}



