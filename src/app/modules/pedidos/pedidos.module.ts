import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarritoComponent } from './components/carrito/carrito.component';
import { PedidosRoutingModule } from './pedidos-routing.module';
import { ClrModalModule,ClrSelectModule,ClrFormsModule} from "@clr/angular";
import { ClarityModule } from '@clr/angular';
import { CarritoPedidoComponent } from './components/carrito-pedido/carrito-pedido.component';
import { CartaModule } from '../carta/carta.module';
import { TarjetaProductoComponent } from '../carta/components/tarjeta-producto/tarjeta-producto.component';
import { VerPedidosComponent } from './components/ver-pedidos/ver-pedidos.component';
import { NuevoPedidoPresencialComponent } from './components/nuevo-pedido-presencial/nuevo-pedido-presencial.component';
import { FormsModule } from '@angular/forms';


import { ClrFormsNextModule } from '@clr/angular';


@NgModule({
  imports: [
    CommonModule,
    PedidosRoutingModule,
    ClarityModule,
    ClrModalModule,
    CartaModule,
    ClrSelectModule,
    ClrFormsModule,
    FormsModule,
    ClrFormsNextModule
  ],
  declarations: [CarritoComponent, CarritoPedidoComponent, VerPedidosComponent, NuevoPedidoPresencialComponent]
})
export class PedidosModule { }
