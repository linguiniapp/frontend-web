import {Injectable} from '@angular/core';
import {Apollo, gql} from 'apollo-angular-boost';
import {Observable, Subject} from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class PedidosService {

    private pedidos: Pedido[] = [];
    private ELEMENT_DATA = new Subject<Pedido[]>();
    private ELEMENT_DATA_ped = new Subject<Pedido>();
    private respuestaConfirmar = new Subject<boolean>();
    private ELEMENT_DATA_COCINA = new Subject<PedidoCocina[]>();

    constructor(private apollo: Apollo) {
    }

    getPedido(reservaId): Observable<Pedido> {
        this.apollo.watchQuery<any>({
            query: gql
                `{
          verPedido(numeroReserva: ${reservaId}) {
            comentario
            numero
            fechaHora
            reserva {
              cliente {
                nombre
                apellido
              }
              mesas {
                edges {
                  node {
                    numero
                  }
                }
              }
            }
            promociones {
              edges {
                node {
                  cantidad                  
                  promocion {
                    numero
                    nombre
                    precio
                  }
                }
              }
            }
            recompensas {
              edges {
                node {
                  cantidad
                  recompensa {
                    numero
                    nombre
                    puntos
                  }
                }
              }
            }
            detalles {
              edges {
                node {
                  cantidad
                  cartaProducto {
                    precio
                    producto {
                      nombre
                      codigo
                    }
                  }
                }
              }
            }
          }
        }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                this.pedidos = [];
                let ELEMENT_DATA$: Pedido = null;
                let pedido: Pedido = {
                    idPedido: null,
                    total: 0,
                    promocion: [],
                    recompensa: [],
                    producto: [],
                    comentario: ''
                };
                pedido.comentario = data.verPedido.comentario;
                pedido.idPedido = data.verPedido.numero;
                data.verPedido.detalles.edges.forEach(element => {
                    let productoCarta: ProductoCarta = {
                        cantidad: element.node.cantidad,
                        montoPesos: element.node.cartaProducto.precio,
                        nombre: element.node.cartaProducto.producto.nombre,
                        idProducto: element.node.cartaProducto.producto.codigo
                    };
                    pedido.producto.push(productoCarta);
                    pedido.total += productoCarta.montoPesos * productoCarta.cantidad;

                });
                data.verPedido.promociones.edges.forEach(element => {
                    let promocion: Promocion = {
                        id: element.node.promocion.numero,
                        cantidad: element.node.cantidad,
                        nombre: element.node.promocion.nombre,
                        monto: element.node.promocion.precio

                    };
                    pedido.promocion.push(promocion);
                    pedido.total += promocion.monto * promocion.cantidad;
                });
                data.verPedido.recompensas.edges.forEach(element => {
                    let recompensa: Recompensa = {
                        id: element.node.recompensa.numero,
                        cantidad: element.node.cantidad,
                        nombre: element.node.recompensa.nombre,
                        puntos: element.node.recompensa.puntos
                    };
                    pedido.recompensa.push(recompensa);
                });
                this.ELEMENT_DATA_ped.next(pedido);
            });
        return this.ELEMENT_DATA_ped.asObservable();
    }

    getPedidosCliente(): Observable<PedidoCocina[]> {
        this.apollo.watchQuery<any>({
            query: gql
                `{
          verPedidos{
            edges{
              node{
                numero
                reserva{
                  cliente{
                    nombre
                    apellido            
                  }
                 mesas{
                  edges{
                    node{
                      numero
                    }
                  }
                }
                }
                detalles{
                  edges{
                    node{
                      cantidad
                      cartaProducto{
                        precio
                        producto{
                          nombre
                          codigo
                        }
                      }
                    
                    }
                  }
                }
              }
            }
          }
        }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                let ELEMENT_DATA$: PedidoCocina[] = [];
                let pedido: PedidoCocina = {
                    idPedido: null,
                    date: null,
                    mesas: [],
                    producto: [],
                    recompensa: [],
                    promocion: []
                };
                let i = 1;
                data.verPedidos.edges.forEach(element => {
                    pedido.idPedido = element.node.numero;
                    element.node.reserva.mesas.edges.forEach(element => {
                        pedido.mesas.push(element.node.numero);
                    });
                    element.node.detalles.edges.forEach(element => {
                        let producto: Producto = {
                            cantidad: element.node.cantidad,
                            nombre: element.node.cartaProducto.producto.nombre,
                            idProducto: element.node.cartaProducto.producto.codigo
                        };
                        pedido.producto.push(producto);
                    });
                    ELEMENT_DATA$.push(pedido);
                });

                this.ELEMENT_DATA_COCINA.next(ELEMENT_DATA$);

            });

        return this.ELEMENT_DATA_COCINA.asObservable();
    }

    getPedidos(numeroSucursal: number): Observable<PedidoCocina[]> {


        this.apollo.watchQuery<any>({
            query: gql
                `{
  verPedidos(numeroSucursal: 1) {
    edges {
      node {
        estadia {
          mesas {
            edges {
              node {
                numero
              }
            }
          }
        }
        numero
        fechaHora
        reserva {
          cliente {
            nombre
            apellido
          }
          mesas {
            edges {
              node {
                numero
              }
            }
          }
        }

        promociones {
          edges {
            node {
              cantidad
              promocion {
                nombre
              }
            }
          }
        }
        recompensas {
          edges {
            node {
              cantidad
              recompensa {
                nombre
              }
            }
          }
        }
        detalles {
          edges {
            node {
              cantidad
              cartaProducto {
                precio
                producto {
                  nombre
                  codigo
                }
              }
            }
          }
        }
      }
    }
  }
}
`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                let ELEMENT_DATA$: PedidoCocina[] = [];
                let i = 1;
                data.verPedidos.edges.forEach(element => {
                    let pedido: PedidoCocina = {
                        idPedido: null,
                        date: null,
                        mesas: [],
                        producto: [],
                        recompensa: [],
                        promocion: []
                    };

                    let fechaAux = new Date(element.node.fechaHora);
                    fechaAux.setHours(fechaAux.getHours() - 3);
                    console.log(fechaAux);

                    pedido.date = fechaAux;
                    pedido.idPedido = element.node.numero;
                    if (element.node.reservas) {
                        element.node.reserva.mesas.edges.forEach(element => {
                            pedido.mesas.push(element.node.numero);
                        });
                    }
                    if (element.node.estadia) {
                        element.node.estadia.mesas.edges.forEach(element => {
                            pedido.mesas.push(element.node.numero);
                        });
                    }

                        element.node.detalles.edges.forEach(element => {
                            let producto: Producto = {
                                cantidad: element.node.cantidad,
                                nombre: element.node.cartaProducto.producto.nombre,
                                idProducto: element.node.cartaProducto.producto.codigo
                            };
                            pedido.producto.push(producto);
                        });
                        element.node.promociones.edges.forEach(element => {
                            let promocion: PromocionCocina = {
                                cantidad: element.node.cantidad,
                                nombre: element.node.promocion.nombre
                            };
                            pedido.promocion.push(promocion);
                        });
                        element.node.recompensas.edges.forEach(element => {
                            let recompensa: RecompensaCocina = {
                                cantidad: element.node.cantidad,
                                nombre: element.node.recompensa.nombre
                            };
                            pedido.recompensa.push(recompensa);
                        });
                        ELEMENT_DATA$.push(pedido);
                    }
                );

                this.ELEMENT_DATA_COCINA.next(ELEMENT_DATA$);

            });
        return this.ELEMENT_DATA_COCINA.asObservable();
    }


    confirmarPedido(numeroPedido): Observable<boolean> {
        this.apollo
            .mutate({
                mutation: gql
                    `mutation{
          confirmarPedido(numeroPedido:${numeroPedido}){
            ok
          }
        }`
                ,
            })
            .subscribe(({data}) => {
                if (data.confirmarPedido.ok === 0) {
                    // alert("Pedido Confirmado")
                    this.respuestaConfirmar.next(true);

                } else {
                    this.respuestaConfirmar.next(false);

                }
            }, (error) => {
                this.respuestaConfirmar.next(false);

            });

        return this.respuestaConfirmar.asObservable();
    }

    eliminarPlato() {
    }

}

export interface Pedido {
    comentario
    idPedido: number;
    total: number;
    producto: ProductoCarta[];
    promocion: Promocion[];
    recompensa: Recompensa[];
}

export interface ProductoCarta {
    idProducto: number;
    nombre: String;
    cantidad: number;
    montoPesos: number;
}

export interface PedidoCocina {
    idPedido: number;
    date: Date;
    mesas: number[];
    producto: Producto[];
    recompensa: RecompensaCocina[];
    promocion: PromocionCocina[];
}

export interface RecompensaCocina {
    nombre: String;
    cantidad: number;
}

export interface PromocionCocina {
    nombre: String;
    cantidad: number;
}

export interface Recompensa {
    id: number;
    nombre: String;
    cantidad: number;
    puntos: number;
}

export interface Promocion {
    id: number;
    nombre: String;
    cantidad: number;
    monto: number;
}

export interface Producto {
    idProducto: number;
    nombre: String;
    cantidad: number;
}

export interface NuevoPedido {
    mesa: number;
    numeroSucursal: number;
    productos: Producto[];
}