import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { CarritoComponent } from './components/carrito/carrito.component';
import { VerPedidosComponent } from './components/ver-pedidos/ver-pedidos.component';
import { NuevoPedidoPresencialComponent } from './components/nuevo-pedido-presencial/nuevo-pedido-presencial.component';



const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'verCarrito/:idReserva',
                component: CarritoComponent
            },{
                path: 'verPedidos/:sucursal',
                component: VerPedidosComponent
            },{
                path: 'nuevoPedido/:id',
                component: NuevoPedidoPresencialComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PedidosRoutingModule { }
