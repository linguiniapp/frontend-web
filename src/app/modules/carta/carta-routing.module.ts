import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerCartaComponent } from './components/ver-carta/ver-carta.component';
import { ListaMenuCategoriaComponent } from './components/lista-menu-categoria/lista-menu-categoria.component';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { CrearCartaComponent } from './components/crear-carta/crear-carta.component';
import { AsignarCartaComponent } from './components/asignar-carta/asignar-carta.component';
import { AuthService } from '../../security/login/auth.service';
import { EditarCartaComponent } from './components/editar-carta/editar-carta.component';
import { VerCartaGeneralComponent } from './components/ver-carta-general/ver-carta-general.component';
import { VerCartaPromocionesComponent } from './components/ver-carta-promociones/ver-carta-promociones.component';
import { CrearRecompensaComponent } from './components/crear-recompensa/crear-recompensa.component';
import { VerCartaRecompensasComponent } from './components/ver-carta-recompensas/ver-carta-recompensas.component';
import { VerRecompensasComponent } from './components/ver-recompensas/ver-recompensas.component';




const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'listaCartas/:id/:idSucursal',
                component: VerCartaComponent
            },
            {
                path: 'listaCarta/categoria/:id/:idSucursal/:idReserva',
                component: ListaMenuCategoriaComponent
            },
            {
                path: 'crearCarta/:id',
                component: CrearCartaComponent, canActivate: [AuthService],
                data: { acciones: ["crear_carta"] }
            },
            {
                path: 'verCarta/:id',
                component: EditarCartaComponent

            },
            {
                path: 'verCartas',
                component: AsignarCartaComponent
            }
            ,
            {
                path: 'verCartaGeneral/:id/:idSucursal',
                component: VerCartaGeneralComponent
            },
            {
                path: 'listaCarta/verCartaPromociones/:idReserva',
                component: VerCartaPromocionesComponent

            },
            {
                path: 'crearRecompensa',
                component: CrearRecompensaComponent

            },
            {
                path: 'listaCarta/verCartaRecompensas/:idReserv',
                component: VerCartaRecompensasComponent

            }
            ,
            {
                path: 'verRecompensas',
                component: VerRecompensasComponent

            }
            
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CartaRoutingModule { }
