import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Recompensa, VerCartaService } from '../../services/ver-carta.service';
import { ClrLoadingState } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ver-carta-recompensas',
  templateUrl: './ver-carta-recompensas.component.html',
  styleUrls: ['./ver-carta-recompensas.component.css']
})
export class VerCartaRecompensasComponent implements OnInit {
  dataSource: Recompensa[];
  cant: number = 0;
  total: number = 0;
  numeroSucursal:number =0;
  numeroReserva: number=0;
  evento = new EventEmitter<Number[]>();
  
  validateBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;

  constructor(private cartaService: VerCartaService, private actvateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cartaService.getRecompensas().subscribe(data=> this.dataSource=data);
    
    this.actvateRoute.params.subscribe(params => {            
      this.numeroReserva = params['idReserva'];     
      this.cant = this.cartaService.cant;
      this.total = this.cartaService.total;  

    });
  }
 
    updateCant($event,) {
      this.cant = $event[0];
      this.total = $event[1];
    }
    
    addCart(recom){
    
      this.validateBtnState = ClrLoadingState.DEFAULT;
    
      this.validateBtnState = ClrLoadingState.LOADING;
    
      var stat = this.cartaService.insertRecompensa(recom, this.cant, 1);
    
      if(stat){
        this.validateBtnState = ClrLoadingState.SUCCESS;
        this.evento.emit([this.cartaService.getCantProduct(), this.cartaService.getTotal()]);
    
      }
    }
}
