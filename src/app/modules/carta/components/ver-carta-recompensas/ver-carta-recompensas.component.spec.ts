import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerCartaRecompensasComponent } from './ver-carta-recompensas.component';

describe('VerCartaRecompensasComponent', () => {
  let component: VerCartaRecompensasComponent;
  let fixture: ComponentFixture<VerCartaRecompensasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerCartaRecompensasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerCartaRecompensasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
