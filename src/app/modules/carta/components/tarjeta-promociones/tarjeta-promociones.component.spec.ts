import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarjetaPromocionesComponent } from './tarjeta-promociones.component';

describe('TarjetaPromocionesComponent', () => {
  let component: TarjetaPromocionesComponent;
  let fixture: ComponentFixture<TarjetaPromocionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarjetaPromocionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarjetaPromocionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
