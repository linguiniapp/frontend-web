import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClrLoadingState } from '@clr/angular';
import { VerCartaService } from '../../services/ver-carta.service';

@Component({
  selector: 'app-tarjeta-promociones',
  templateUrl: './tarjeta-promociones.component.html',
  styleUrls: ['./tarjeta-promociones.component.css']
})
export class TarjetaPromocionesComponent implements OnInit {
  @Input() promocion:any = {};
  @Output() evento = new EventEmitter<Number[]>();
  validateBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;
  @Input() numeroReserva:number = null;
  cant: number = 0;
  constructor(private service: VerCartaService) { }
  
  ngOnInit(){  
  }

addProductCant(){
this.cant = this.cant +1;
}

substracProductCant(){
  if (this.cant == 0) {
    return;
  }
  else
  this.cant = this.cant -1;
}

addCart(prom){

  this.validateBtnState = ClrLoadingState.DEFAULT;

  this.validateBtnState = ClrLoadingState.LOADING;

  var stat = this.service.insertPromocion(prom, this.cant, this.numeroReserva);

  if(stat){
    this.validateBtnState = ClrLoadingState.SUCCESS;
    this.evento.emit([this.service.getCantProduct(), this.service.getTotal()]);

  }
}

}
