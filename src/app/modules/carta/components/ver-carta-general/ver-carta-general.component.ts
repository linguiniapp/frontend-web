import { Component, OnInit } from '@angular/core';
import { VerCartaComponent } from '../ver-carta/ver-carta.component';
import { VerCartaService, Opcion } from '../../services/ver-carta.service';
import { TypeCheckCompiler } from '@angular/compiler/src/view_compiler/type_check_compiler';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ver-carta-general',
  templateUrl: './ver-carta-general.component.html',
  styleUrls: ['./ver-carta-general.component.css']
})
export class VerCartaGeneralComponent implements OnInit {
  opciones: Opcion[]=[]
  numeroSucursal: number;
  numeroReserva: number;
  constructor(private cartaServicio: VerCartaService, private router: Router,private activateRoute: ActivatedRoute ) { }

  ngOnInit() {
    this.opciones=this.cartaServicio.getOpciones();
    this.activateRoute.params.subscribe(params=> {
      this.numeroSucursal = params['idSucursal'];
      this.numeroReserva = params['id'];
    })

  }
  seleccionarOpcion(id: number){
    if(id===1){
      this.router.navigate(['/listaCartas/', this.numeroSucursal, this.numeroReserva]);
    }
    if(id===2){
      this.router.navigate(['/listaCarta/verCartaPromociones/',this.numeroReserva]);
    }
    if(id===3){
      this.router.navigate(['/listaCarta/verCartaRecompensas/',this.numeroReserva]);
    }
  }

}
