import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerCartaGeneralComponent } from './ver-carta-general.component';

describe('VerCartaGeneralComponent', () => {
  let component: VerCartaGeneralComponent;
  let fixture: ComponentFixture<VerCartaGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerCartaGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerCartaGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
