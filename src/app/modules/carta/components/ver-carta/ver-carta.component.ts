import { Component, OnInit } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { VerCartaService } from '../../services/ver-carta.service';


@Component({
  selector: 'app-ver-carta',
  templateUrl: './ver-carta.component.html',
  styleUrls: ['./ver-carta.component.css']
})


export class VerCartaComponent implements OnInit {
  categorias: any[] = [];
  numeroSucursal: number = null;
  numeroReserva: number = null;
  suscripcion: any;

  constructor(private _cartaService: VerCartaService, private actvateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.actvateRoute.params.subscribe(params => {
      this.numeroSucursal = params['idSucursal'];
      this.numeroReserva = params['id'];
      this.suscripcion = this._cartaService.getCategorias(params['id']).subscribe(data => this.categorias = data);
    })
  }
  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();
  }
}
