import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PromocionesService, ListaElement, DetalleProductoInput } from '../../../promociones/services/promociones.service';
import { Observable } from 'rxjs';
import { ClrLoadingState } from '@clr/angular';
import { VerCartaService } from '../../services/ver-carta.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ver-carta-promociones',
  templateUrl: './ver-carta-promociones.component.html',
  styleUrls: ['./ver-carta-promociones.component.css']
})
export class VerCartaPromocionesComponent implements OnInit {
  dataSource: ListaElement[] = [];
  cant: number = 0;
  total: number = 0;
  numeroReserva: number=0;
  @Output() evento = new EventEmitter<Number[]>();
  validateBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;

  constructor(private promocionesServicio: PromocionesService, private cartaService: VerCartaService,
    private actvateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.promocionesServicio.getPromo().subscribe(data => this.dataSource = data);

    this.actvateRoute.params.subscribe(params => {
      this.numeroReserva = params['idReserva'];
      this.cant = this.cartaService.cant;
      this.total = this.cartaService.total;

    });
  }
  addProductCant() {
    this.cant = this.cant + 1;
  }

  updateCant($event,) {
    this.cant = $event[0];
    this.total = $event[1];
  }

  substracProductCant() {
    if (this.cant == 0) {
      return;
    }
    else
      this.cant = this.cant - 1;
  }

  addCart(prom) {

    this.validateBtnState = ClrLoadingState.DEFAULT;

    this.validateBtnState = ClrLoadingState.LOADING;

    var stat = this.cartaService.insertPromocion(prom, this.cant, 1);

    if (stat) {
      this.validateBtnState = ClrLoadingState.SUCCESS;
      this.evento.emit([this.cartaService.getCantProduct(), this.cartaService.getTotal()]);

    }
  }

}
