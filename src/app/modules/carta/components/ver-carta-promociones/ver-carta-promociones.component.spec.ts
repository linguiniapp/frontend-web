import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerCartaPromocionesComponent } from './ver-carta-promociones.component';

describe('VerCartaPromocionesComponent', () => {
  let component: VerCartaPromocionesComponent;
  let fixture: ComponentFixture<VerCartaPromocionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerCartaPromocionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerCartaPromocionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
