import { Component, OnInit, OnDestroy } from '@angular/core';
import { VerCartaService, DetalleCartaProductoInput, DetalleCartaRecompensa } from '../../services/ver-carta.service';
import { ProductosService, categoria, ListaElement, SelectedCarta, SelectedRecompensa } from '../../../productos/services/productos.service';
import { NgForm, Validators, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-crear-recompensa',
  templateUrl: './crear-recompensa.component.html',
  styleUrls: ['./crear-recompensa.component.css']
})
export class CrearRecompensaComponent implements OnInit {
  dataSource: ListaElement[] = [];
  listaCategoria: categoria[] = [];
  selectedProd: SelectedRecompensa[] = [];
  select: SelectedCarta[] = [];
  correcto: boolean = false;
  codCategoria: number = 1;
  complete: boolean = false;
  busqueda: string = "";
  error: boolean = false;
  detalleCartaRecompensa: DetalleCartaRecompensa[] = []
  completePrecio: boolean = false;
  numeroSucursal: number = null;
  observableCategoria: any = null;

  constructor(private serviceCarta: VerCartaService, private routes: Router, private activateRoute: ActivatedRoute, private serviceProducto: ProductosService) { }

  ngOnInit() {
    this.selectedProd = [];
    this.observableCategoria = this.serviceProducto.getCategoria();
    this.observableCategoria.subscribe(data => {
      this.listaCategoria = data;

    });
    // })
    // if (!this.dataSource) {
    this.serviceProducto.getProductoCategoria(this.codCategoria).subscribe(data => this.dataSource = data);

    // }

  }
  ngOnDestroy() {
    console.log("ondestroy")
  }
  onSubmit(f: NgForm) {
    this.complete = false;
    this.completePrecio = false;
    this.error = false;

    if (f.valid) {

      if (this.selectedProd.length == 0) {
        this.complete = true;
      }
      else {

        this.selectedProd.forEach(data => {
          if (data.cantidad == 0 || data.cantidad == undefined) {
            this.completePrecio = true;
            return;
          }
          else {
            let detalleProd: DetalleCartaRecompensa= {
              numero: data.codigo,
              cantidad: data.cantidad
            }

            this.detalleCartaRecompensa.push(detalleProd);



          }

        });

        this.serviceCarta.crearRecompensa(this.detalleCartaRecompensa,f.value.nombre,f.value.puntos).subscribe(data => {
          if (data) {

            this.codCategoria = 0;
            this.correcto = true;
            //reinica las tablas de productos
            this.selectedProd = [];

            // this.serviceCarta.asignarCartaSucursal(this.numeroSucursal).subscribe(res => {

            //   if (res) {
            //     this.correcto = true;
            //     //reinica las tablas de productos
            //     this.selectedProd = [];

            //   } else {
            //     this.error = true;

            //   }


            // })


          }
          else {
            this.error = true;
          }
        });
      }
    }
    else {

      this.complete = !this.complete;
    }

  }

  buscar(busqueda) {
    this.dataSource = this.dataSource.filter(data => data.nombre == busqueda.value);
  }

  cerrarCorrecto() {
    this.correcto = false;
  }

  volver() {
    this.routes.navigate(["/verRecompensas"]);
  }


  categoriaChange() {

    this.dataSource = [];
    this.serviceProducto.getProductoCategoria(this.codCategoria).subscribe(data => {
      this.dataSource = data;
    });

    this.serviceCarta.getProductosCarta(this.codCategoria, this.numeroSucursal).subscribe(data => this.select = data);


  }

  selected(codigo) {
    let prod = this.select.find(d => d.codigo == codigo);
    if (prod) {
      return true
    } else {
      return false
    }

  }

}
