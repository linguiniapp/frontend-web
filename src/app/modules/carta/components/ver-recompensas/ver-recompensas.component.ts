import { Component, OnInit } from '@angular/core';
import { Recompensa, VerCartaService } from '../../services/ver-carta.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ver-recompensas',
  templateUrl: './ver-recompensas.component.html',
  styleUrls: ['./ver-recompensas.component.css']
})
export class VerRecompensasComponent implements OnInit {
  dataSource: Recompensa[];
  dataSourceDefault: Recompensa[];
  selectedProd: any;

  constructor(private cartaService: VerCartaService, private router: Router) { }

  ngOnInit() {
    this.cartaService.getRecompensas().subscribe(data => {
      this.dataSource = data;
      this.dataSourceDefault = data;
    });
  }

  onSearchChange(busqueda: string) {
    this.dataSource = this.dataSourceDefault;
    this.dataSource = this.dataSource.filter(data => data.nombre.includes(busqueda));
  }
  agregarRecompensa(){
    this.router.navigate(['/crearRecompensa']);

  }

}
