import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarCartaComponent } from './asignar-carta.component';

describe('AsignarCartaComponent', () => {
  let component: AsignarCartaComponent;
  let fixture: ComponentFixture<AsignarCartaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarCartaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarCartaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
