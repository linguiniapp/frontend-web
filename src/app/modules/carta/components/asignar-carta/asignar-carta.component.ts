

import { Component, OnInit, OnDestroy } from '@angular/core';
import { VerCartaService, DetalleCartaProductoInput } from '../../services/ver-carta.service';
import { ProductosService, categoria, SelectedCarta, SelectedProducto } from '../../../productos/services/productos.service';
import { NgForm, Validators, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ClrDatagridStringFilterInterface } from "@clr/angular";
import { PokemonFilter } from '../../utils/pokemon-filter';
import { ProductoCarta } from '../../../pedidos/services/pedidos.service';
import { SucursalService, ListaElement } from '../../../sucursales/services/sucursal.service';
import { Observable, Unsubscribable } from 'rxjs';

@Component({
  selector: 'app-asignar-carta',
  templateUrl: './asignar-carta.component.html',
  styleUrls: ['./asignar-carta.component.css'],
  exportAs: 'ngModel'

})
export class AsignarCartaComponent implements OnInit {



  sucursales: ListaElement[] = [];
  sucursal: number = 1;

  producto: ProductoCarta;
  imagen: String = "";
  descripcion: string = "";
  basic: boolean = false;
  listaCategoria: categoria[] = [];
  listaCategoriaDefault: categoria[] = [];
  selectedProd: SelectedCarta[] = [];
  selectedProdDefault: SelectedCarta[] = [];
  select: SelectedCarta[] = [];
  correcto: boolean = false;
  codCategoria: number = -1;
  complete: boolean = false;
  busqueda: string = "";
  error: boolean = false;
  detalleCartaProducto: DetalleCartaProductoInput[] = []
  completePrecio: boolean = false;
  numeroSucursal: number = null;
  observableCategoria: any = null;
  observableSeleccion: any = null;
  prodSucursales: prodSuc[] = [];
  selectedProd1: any;

  constructor(private serviceCarta: VerCartaService, private sucursalService: SucursalService, private routes: Router, private activateRoute: ActivatedRoute, private serviceProducto: ProductosService) { }

  ngOnInit() {

    this.sucursalService.getListaBasic().subscribe(data => {
      this.sucursales = data;
    });
    this.observableCategoria = this.serviceProducto.getCategoria().subscribe(data => {
      this.listaCategoria = data;
    });
    if (this.selectedProd == [] || this.selectedProd.length == 0) {
      setTimeout(() => {
        this.selectedProd = [];
        this.listaCategoria;
        if (this.codCategoria == -1) {
          if (this.listaCategoria == undefined || this.listaCategoria.length == 0) {
          } else {



            //variar por cada sucursal
            this.sucursales.forEach(suc => {
              //reincio los productos
              this.selectedProd = [];

              //por cada categoria vario
              this.listaCategoria.forEach(elem => {
                this.selectedProd = [];



                this.serviceCarta.getProductosCartaQuery(elem.codigo, suc.numero).subscribe(data => {
                    // if (this.selectedProd.find(p => p.codigo == data.codigo)) {
                    //   existe = true;

                    // } else {
                    //   this.selectedProd = [...this.selectedProd, data];
                    //   this.selectedProdDefault = [...this.selectedProdDefault, d];
                    // }

                  this.selectedProd = data
                });

                setTimeout(() => {

                  console.log("time")
                  let sucProd: prodSuc = {
                    numeroSuc: suc.numero,
                    prod: this.selectedProd
                  }
                  if (this.prodSucursales.find(a => a.numeroSuc == sucProd.numeroSuc)) {
                  } else {
                    this.prodSucursales = [...this.prodSucursales, sucProd];
                    console.log(this.prodSucursales);
                  }


                }, 1000)

                // this.selectedProd = [];

                // //por cada producto de esa categoria en esa sucursal
                // data.forEach(d => {
                //   let existe = false;
                //   //si ya existe ese producto en la lista no lo meto
                //   this.prodSucursales.forEach(prod1 => {
                //     if (prod1.prod.find(p => p.codigo == d.codigo)) {
                //       existe = true;

                //     } else {
                //       this.selectedProd = [...this.selectedProd, d];
                //       this.selectedProdDefault = [...this.selectedProdDefault, d];
                //     }
                //   })

                //   if (!existe) {

                //     if (this.selectedProd.find(p => p.codigo == d.codigo)) {
                //       existe = true;

                //     } else {
                //       this.selectedProd = [...this.selectedProd, d];
                //       this.selectedProdDefault = [...this.selectedProdDefault, d];
                //     }

                //   }
                //   console.log(suc.numero)
                //   let sucProd: prodSuc = {
                //     numeroSuc: suc.numero,
                //     prod: this.selectedProd
                //   }
                //   if (this.prodSucursales.find(a => a.numeroSuc == sucProd.numeroSuc)) {
                //   } else {
                //     this.prodSucursales = [...this.prodSucursales, sucProd];
                //     console.log(this.prodSucursales);
                //   }

                // });

                // console.log(this.selectedProd)
              })


            })
          // })



    }
  }

}, 1000)
    }
  }

volver() {
  this.routes.navigate(["/listaCarta"]);
}


categoriaChange() {

  this.selectedProd = [];
  this.selectedProd = this.selectedProdDefault;
  this.selectedProd = this.selectedProd.filter(data => data.codigoCat == this.codCategoria);


}

sucursalChange() {



  this.selectedProd = [];
  this.selectedProd = this.selectedProdDefault;

  this.selectedProd = this.prodSucursales.find(s => s.numeroSuc == this.sucursal).prod;
  console.log(this.sucursal);
  console.log(this.selectedProd);
  console.log(this.prodSucursales);


  //   // this.serviceProducto.getCategoriaBis().unsubscribe();
  //   this.listaCategoriaDefault = this.listaCategoria;

  //   console.log("ver carta sucursal nro " + this.sucursal)
  //   this.selectedProd = [];
  //   console.log("ver prod sucursal nro " + this.selectedProd)

  //   if (this.codCategoria == -1) {
  //     this.listaCategoriaDefault.forEach(elem => {
  //       console.log(elem.codigo);
  //       let indx = this.listaCategoriaDefault.findIndex(data => data.codigo == elem.codigo);
  //       // this.serviceProducto.getProductoCategoria(elem.codigo)
  //      this.observableSeleccion = this.serviceCarta.getProductosCartaQuery(elem.codigo, this.sucursal)

  //     })



  //   }
  //   else {

  //     // this.serviceProducto.getProductoCategoria(elem.codigo)
  //     this.serviceCarta.getProductosCartaQuery(this.codCategoria, this.sucursal).subscribe(data => {
  //       data.forEach(d => {
  //         if (this.selectedProd.find(p => p.codigo == d.codigo)) {

  //         } else {
  //           this.selectedProd = [...this.selectedProd, d];
  //           this.selectedProdDefault = [...this.selectedProdDefault, d];
  //           // console.log(this.dataSource)
  //         }


  //       })


  //     })

  //   }
}
onSearchChange(busqueda: string) {

  this.selectedProd = this.selectedProdDefault;


  this.selectedProd = this.selectedProd.filter(data => data.nombre.includes(busqueda));



}

onSearchCodChange(busqueda) {

  this.selectedProd = this.selectedProdDefault;

  if (busqueda != undefined && busqueda != "") {

    this.selectedProd = this.selectedProd.filter(data => data.precio == busqueda);
  }

}
verProducto(id: number) {
  let prodse = this.selectedProd.find(p => p.codigo == id);

  let prod: ProductoCarta = {
    idProducto: prodse.codigo,
    nombre: prodse.nombre,
    montoPesos: prodse.precio,
    cantidad: 0,

  }
  this.producto = prod;
  this.serviceCarta.getProducto(id).subscribe(p => {
    // console.log(p.imagen)
    this.imagen = p.imagen;
    this.descripcion = p.descripcion
  });
  this.basic = true;
}
asignarNuevo() {
  this.routes.navigate(['/crearCarta', this.sucursal])
}


}
export interface prodSuc {
  numeroSuc: number;
  prod: SelectedCarta[]
}