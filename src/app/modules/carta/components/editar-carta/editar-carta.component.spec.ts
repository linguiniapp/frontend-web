import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarCartaComponent } from './editar-carta.component';

describe('EditarCartaComponent', () => {
  let component: EditarCartaComponent;
  let fixture: ComponentFixture<EditarCartaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarCartaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarCartaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
