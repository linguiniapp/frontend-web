
import { Component, OnInit, OnDestroy } from '@angular/core';
import { VerCartaService, DetalleCartaProductoInput } from '../../services/ver-carta.service';
import { ProductosService, categoria, ListaElement, SelectedCarta } from '../../../productos/services/productos.service';
import { NgForm, Validators, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ClrDatagridStringFilterInterface } from "@clr/angular";
import { PokemonFilter } from '../../utils/pokemon-filter';
import { ProductoCarta } from '../../../pedidos/services/pedidos.service';

import { AuthService } from '../../../../security/login/auth.service';
import { acciones } from '../../../../modules/roles/services/roles.service';

@Component({
  selector: 'app-editar-carta',
  templateUrl: './editar-carta.component.html',
  styleUrls: ['./editar-carta.component.css'],
  exportAs: 'ngModel'
})
export class EditarCartaComponent implements OnInit {
  dataSource: ListaElement[] = [];
  producto: ProductoCarta;
  imagen: String = "";
  descripcion: string = "";
  basic: boolean = false;
  listaCategoria: categoria[] = [];
  listaCategoriaDefault: categoria[] = [];
  selectedProd: SelectedCarta[] = [];
  selectedProdDefault: SelectedCarta[] = [];
  select: SelectedCarta[] = [];
  correcto: boolean = false;
  codCategoria: number = -1;
  complete: boolean = false;
  busqueda: string = "";
  error: boolean = false;
  detalleCartaProducto: DetalleCartaProductoInput[] = []
  completePrecio: boolean = false;
  numeroSucursal: number = null;
  observableCategoria: any = null;
  puedoCrear: boolean = false;
  selectedProd1: any;

  constructor(private serviceCarta: VerCartaService,private authService: AuthService,  private routes: Router, private activateRoute: ActivatedRoute, private serviceProducto: ProductosService) { }

  ngOnInit() {

    this.authService.getAccionesAsinc().subscribe(data => {
    if (this.authService.verificarAccion("crear_carta", data)){
      this.puedoCrear = true;
    }
  });
    this.activateRoute.params.subscribe(params => {
      this.numeroSucursal = params['id'];
    });
    // console.log("antes de reiniciar el selectedProd")
    this.selectedProd = [];
    this.observableCategoria = this.serviceProducto.getCategoriaBis();
    this.observableCategoria.subscribe(data => {
      this.listaCategoria = data;
      // console.log(this.listaCategoria)
      this.listaCategoriaDefault = data;
      // console.log(this.listaCategoriaDefault)
      if (this.codCategoria == -1) {
        this.listaCategoriaDefault.forEach(elem => {

          let indx = this.listaCategoriaDefault.findIndex(data => data.codigo == elem.codigo);
          // this.serviceProducto.getProductoCategoria(elem.codigo)
          this.serviceCarta.getProductosCartaQuery(elem.codigo, this.numeroSucursal).subscribe(data => {
            data.forEach(d => {
              if (this.selectedProd.find(p => p.codigo == d.codigo)) {

              } else {
                this.selectedProd = [...this.selectedProd, d];
                this.selectedProdDefault = [...this.selectedProdDefault, d];
                // console.log(this.dataSource)
              }

            });
          })

          this.listaCategoriaDefault.splice(indx, 1);
        })


      }
    });

  }
  ngOnDestroy() {
    console.log("ondestroy")
  }



  volver() {
    this.routes.navigate(["/listaCarta"]);
  }


  categoriaChange() {

    this.selectedProd = this.selectedProdDefault;
    this.selectedProd = this.selectedProd.filter(data => data.codigoCat == this.codCategoria);


  }


  onSearchChange(busqueda: string) {

    this.selectedProd = this.selectedProdDefault;


    this.selectedProd = this.selectedProd.filter(data => data.nombre.includes(busqueda));



  }

  onSearchCodChange(busqueda) {

    this.selectedProd = this.selectedProdDefault;

    if (busqueda != undefined && busqueda != "") {

      this.selectedProd = this.selectedProd.filter(data => data.precio == busqueda);
    }

  }
  verProducto(id: number) {
    let prodse = this.selectedProd.find(p => p.codigo == id);

    let prod: ProductoCarta = {
      idProducto: prodse.codigo,
      nombre: prodse.nombre,
      montoPesos: prodse.precio,
      cantidad: 0,

    }
    this.producto = prod;
    this.serviceCarta.getProducto(id).subscribe(p => {
      // console.log(p.imagen)
      this.imagen = p.imagen;
      this.descripcion = p.descripcion
    });
    this.basic = true;
  }
  asignarNuevo(){
    this.routes.navigate(['/crearCarta', this.numeroSucursal])
  }


}
