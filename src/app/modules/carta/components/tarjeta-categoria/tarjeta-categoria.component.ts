import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjeta-categoria',
  templateUrl: './tarjeta-categoria.component.html',
  styleUrls: ['./tarjeta-categoria.component.css']
})
export class TarjetaCategoriaComponent implements OnInit {
  @Input() categoria:any = {};
  @Input() numeroSucursal:number = null;
  @Input() numeroReserva:number = null;
  

  constructor(private router: Router) { }

  ngOnInit() {
  }
  verProductos(categoriaId){
    this.router.navigate(['/listaCarta/categoria',categoriaId, this.numeroSucursal, this.numeroReserva]);
    
  }

}
