import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarjetaRecompensaComponent } from './tarjeta-recompensa.component';

describe('TarjetaRecompensaComponent', () => {
  let component: TarjetaRecompensaComponent;
  let fixture: ComponentFixture<TarjetaRecompensaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarjetaRecompensaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarjetaRecompensaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
