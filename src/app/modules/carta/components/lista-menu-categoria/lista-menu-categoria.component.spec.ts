import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMenuCategoriaComponent } from './lista-menu-categoria.component';

describe('ListaMenuCategoriaComponent', () => {
  let component: ListaMenuCategoriaComponent;
  let fixture: ComponentFixture<ListaMenuCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaMenuCategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMenuCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
