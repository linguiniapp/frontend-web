import { Component, OnInit } from '@angular/core';
import { VerCartaService, Producto } from '../../services/ver-carta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-lista-menu-categoria',
  templateUrl: './lista-menu-categoria.component.html',
  styleUrls: ['./lista-menu-categoria.component.css']
})
export class ListaMenuCategoriaComponent implements OnInit {
  cant: number = 0;
  total: number = 0;
  productos: Producto[];
  numeroSucursal: number = null;
  numeroReserva: number = null;
  ok: boolean = false;
  suscripcion: any;

  constructor(private _cartaService: VerCartaService, private actvateRoute?: ActivatedRoute, private route?: Router) { }

  ngOnInit() {
    this.actvateRoute.params.subscribe(params => {

      this.numeroSucursal = params['idSucursal'];
      this.numeroReserva = params['idReserva'];
      this.suscripcion = this._cartaService.getProductos(params['id'], this.numeroSucursal).subscribe(data => {
        this.productos = data;
        if (data[0]) {
          this.ok = false;
        } else {
          this.ok = true;
        }
      });
    });
    this.cant = this._cartaService.cant;
    this.total = this._cartaService.total;
  }
  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();
  }


  updateCant($event, ) {
    this.cant = $event[0];
    this.total = $event[1];
  }
  verProducto(idx) {
    this._cartaService.getProductos(idx, this.numeroSucursal);
  }
  verPedido() {
    this.route.navigate(['/verCarrito', this.numeroReserva]);
  }
}
