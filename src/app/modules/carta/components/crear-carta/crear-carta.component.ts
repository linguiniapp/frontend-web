import {Component, OnInit} from '@angular/core';
import {DetalleCartaProductoInput, VerCartaService} from '../../services/ver-carta.service';
import {categoria, ListaElement, ProductosService, SelectedCarta} from '../../../productos/services/productos.service';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
    selector: 'app-crear-carta',
    templateUrl: './crear-carta.component.html',
    styleUrls: ['./crear-carta.component.css'],
    exportAs: 'ngModel'

})
export class CrearCartaComponent implements OnInit {
    dataSource: ListaElement[] = [];
    listaCategoria: categoria[] = [];
    selectedProd: SelectedCarta[] = [];
    select: SelectedCarta[] = [];
    correcto: boolean = false;
    codCategoria: number = 1;
    complete: boolean = false;
    busqueda: string = '';
    error: boolean = false;
    detalleCartaProducto: DetalleCartaProductoInput[] = [];
    completePrecio: boolean = false;
    numeroSucursal: number = null;
    observableCategoria: any = null;

    constructor(private serviceCarta: VerCartaService, private routes: Router, private activateRoute: ActivatedRoute, private serviceProducto: ProductosService) {
    }

    ngOnInit() {
        this.activateRoute.params.subscribe(params => {
            this.numeroSucursal = params['id'];
        });
        this.selectedProd = [];
        this.observableCategoria = this.serviceProducto.getCategoria();
        this.observableCategoria.subscribe(data => {
            this.listaCategoria = data;

        });
        // })
        // if (!this.dataSource) {
        this.serviceProducto.getProductoCategoria(this.codCategoria).subscribe(data => this.dataSource = data);

        // }

    }

    onSubmit(f: NgForm) {
        this.complete = false;
        this.completePrecio = false;
        this.error = false;

        if (f.valid) {

            if (this.selectedProd.length == 0) {
                this.complete = true;
            } else {

                this.selectedProd.forEach(data => {
                    if (data.precio === 0 || data.precio === undefined) {
                        this.completePrecio = true;
                        return;
                    } else {
                        let detalleProd: DetalleCartaProductoInput = {
                            numero: data.codigo,
                            precio: data.precio

                        };

                        this.detalleCartaProducto.push(detalleProd);

                        this.serviceCarta.crearCarta(this.detalleCartaProducto).subscribe(data => {

                            if (data) {

                                this.codCategoria = 0;


                                this.serviceCarta.asignarCartaSucursal(this.numeroSucursal).subscribe(res => {

                                    if (res) {
                                        this.correcto = true;
                                        // reinica las tablas de productos
                                        this.selectedProd = [];

                                    } else {
                                        this.error = true;

                                    }


                                });


                            } else {
                                this.error = true;
                            }
                        });

                    }

                });
            }
        } else {

            this.complete = !this.complete;
        }

    }

    buscar(busqueda) {
        this.dataSource = this.dataSource.filter(data => data.nombre === busqueda.value);
    }

    cerrarCorrecto() {
        this.correcto = false;
    }

    volver() {
        this.routes.navigate(['/verCarta', this.numeroSucursal]);
    }


    categoriaChange() {

        this.dataSource = [];
        this.serviceProducto.getProductoCategoria(this.codCategoria).subscribe(data => {
            this.dataSource = data;
        });

        this.serviceCarta.getProductosCarta(this.codCategoria, this.numeroSucursal).subscribe(data => this.select = data);


    }

    selected(codigo) {
        let prod = this.select.find(d => d.codigo === codigo);
        return !!prod;

    }

}
