import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';
import { VerCartaService } from '../../services/ver-carta.service';
import { ClrLoadingState } from '@clr/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tarjeta-producto',
  templateUrl: './tarjeta-producto.component.html',
  styleUrls: ['./tarjeta-producto.component.css']
})
export class TarjetaProductoComponent implements OnInit {
  @Output() evento = new EventEmitter<Number[]>();
  @Input() producto:any = {};
  @Input() numeroReserva:number = null;
  validateBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;
  cant: number = 0;
  constructor(private service: VerCartaService) { }
  
  ngOnInit(){  
  }

addProductCant(){
this.cant = this.cant +1;
}

substracProductCant(){
  if (this.cant == 0) {
    return;
  }
  else
  this.cant = this.cant -1;
}

addCart(prod){

  this.validateBtnState = ClrLoadingState.DEFAULT;

  this.validateBtnState = ClrLoadingState.LOADING;

  var stat = this.service.insertProducto(prod, this.cant, this.numeroReserva);

  if(stat){
    this.validateBtnState = ClrLoadingState.SUCCESS;
    this.evento.emit([this.service.getCantProduct(), this.service.getTotal()]);

  }
}

}
