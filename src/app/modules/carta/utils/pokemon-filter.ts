/*
 * Copyright (c) 2016-2018 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */

import { ClrDatagridStringFilterInterface } from '@clr/angular';
import { ListaElement } from '../../productos/services/productos.service';

export class PokemonFilter implements ClrDatagridStringFilterInterface<ListaElement> {
  accepts(user: ListaElement, search: string): boolean {
    debugger
    return  user.nombre.toLowerCase().indexOf(search) >= 0;
  }
}
