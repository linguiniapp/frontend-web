import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular-boost';
import { Subject, Observable } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';
import { SelectedCarta } from '../../productos/services/productos.service';
import { concatSeries } from 'async';
import { NumericDictionaryIterator } from 'lodash';
import { ListaElement as Promocion } from '../../promociones/services/promociones.service';



@Injectable({
  providedIn: 'root'
})
export class VerCartaService {
  public cant: number = 0;
  public total: number = 0;
  private respuestaAgregar = new Subject<boolean>();
  public reservaId: number = 0;
  private listaProductos = new Subject<Producto[]>();
  private listaCategorias = new Subject<Categoria[]>();
  private listaProductos1: SelectedCarta[] = [];
  private product = new Subject<Producto>();
  private ELEMENT_DATA = new Subject<SelectedCarta[]>();
  private ELEMENT_RECOMPENSA = new Subject<Recompensa[]>();
  private resultado = new Subject<boolean>();
  private resultadoModificar = new Subject<boolean>();
  private resultadoAsignar = new Subject<boolean>();
  private numeroCarta: number = null;
  private categorias: Categoria[] = [];
  private productos: Producto[] = [];
  private opciones: Opcion[] = [{ nombre: "Productos", imagen: "../../../../../assets/images/arugua-and-squash-salad-XL-RECIPE1217.jpg", id: 1 }, { nombre: "Promociones", imagen: "../../../../../assets/images/o.jpg", id: 2 }, { nombre: "Recompensas", imagen: "../../../../../assets/images/Organic_Walnuts_Dessert_4.jpg", id: 3 }]

  constructor(private apollo: Apollo) { }

  getCategorias(sucursalId: number): Observable<Categoria[]> {
    this.apollo.watchQuery<any>({
      query: gql
        `{
          verCategorias{
            edges{
              node{
                nombre
                foto
                codigo
              }
            }
          }
        }`, fetchPolicy: 'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        let ELEMENT_DATA: Categoria[] = [];
        data.verCategorias.edges.forEach(r => {
          let lelem: Categoria = {
            nombre: r.node.nombre,
            url: r.node.foto,
            id: r.node.codigo,
          }
          ELEMENT_DATA.push(lelem);
          this.listaCategorias.next(ELEMENT_DATA);
        });
      });
    return this.listaCategorias.asObservable();
  }
  getProductos(idx, idSucursal): Observable<Producto[]> {
    this.apollo.watchQuery<any>({
      query: gql
        `{
          verCarta(numeroSucursal:${idSucursal}) {
            cartaProductos(codCategoria:${idx}) {
              edges {
                node {
                  precio
                  producto {
                    nombre
                    foto
                    descripcion
                    codigo
                  }
                }
              }
            }
          }
        }`, fetchPolicy: 'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        let ELEMENT_DATA: Producto[] = [];
        data.verCarta.cartaProductos.edges.forEach(r => {
          let lelem: Producto = {
            nombre: r.node.producto.nombre,
            imagen: r.node.producto.foto,
            id: r.node.producto.codigo,
            precio: r.node.precio,
            descripcion: r.node.producto.descripcion
          }
          ELEMENT_DATA.push(lelem);
          this.listaProductos.next(ELEMENT_DATA);
        });
      });
    return this.listaProductos.asObservable();
  }

  getProductosCartaQuery(idx, idSucursal): Observable<SelectedCarta[]> {
    this.apollo.watchQuery<any>({
      query: gql
        `{
          verCarta(numeroSucursal:${idSucursal}) {
            cartaProductos(codCategoria:${idx}) {
              edges {
                node {
                  precio
                  producto {
                    nombre
                    codigo
                    categorias{
                      edges{
                        node{
                          nombre
                          codigo
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }` , fetchPolicy: 'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        data.verCarta.cartaProductos.edges.forEach(p => {
          let elemento: SelectedCarta = {
            codigo: p.node.producto.codigo,
            nombre: p.node.producto.nombre,
            categoria: p.node.producto.categorias.edges[0].node.nombre,
            codigoCat: p.node.producto.categorias.edges[0].node.codigo,
            precio: p.node.precio
          };

          this.listaProductos1.push(elemento)
          if (this.listaProductos1.includes(elemento)) {

          } else {
            this.listaProductos1 = [...this.listaProductos1, elemento];
          }
        });

        this.ELEMENT_DATA.next(this.listaProductos1);
      });
    return this.ELEMENT_DATA.asObservable();
  }


  getProductosCarta(idx, idSucursal): Observable<SelectedCarta[]> {
    this.apollo.watchQuery<any>({
      query: gql
        `{
          verCarta(numeroSucursal:${idSucursal}) {
            cartaProductos(codCategoria:${idx}) {
              edges {
                node {
                  precio
                  producto {
                    nombre
                    codigo
                  }
                }
              }
            }
          }
        }`
    }).valueChanges
      .subscribe(({ data }) => {
        this.listaProductos1 = [];
        data.verCarta.cartaProductos.edges.forEach(p => {
          let elemento: SelectedCarta = {
            codigo: p.node.producto.codigo,
            nombre: p.node.producto.nombre,
            categoria: "",
            codigoCat: null,
            precio: p.node.precio
          };

          if (this.listaProductos1.includes(elemento)) {

          } else {
            this.listaProductos1 = [...this.listaProductos1, elemento];
          }
        });

        this.ELEMENT_DATA.next(this.listaProductos1);
      });
    return this.ELEMENT_DATA.asObservable();
  }


  getProducto(idx): Observable<Producto> {
    this.apollo.watchQuery<any>({
      query: gql
        `{
          verProducto(codigoProducto: ${idx} ){
            nombre
            foto
            descripcion
          }
        }`
    }).valueChanges
      .subscribe(({ data }) => {
        console.log(data.verProducto.nombre)
        let pro: Producto = {
          nombre: data.verProducto.nombre,
          imagen: data.verProducto.foto,
          id: null,
          precio: null,
          descripcion: data.verProducto.descripcion
        }

        this.product.next(pro);
      });
    return this.product.asObservable();
  }

  modificarProducto(prod: number, cant: number, reserva): Observable<boolean> {
    // this.cant = this.cant + cant;
    // this.total = prod.precio * cant + this.total;
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            agregarPedidoRemoto(numeroReserva:${reserva},listaProductos:[{numero:${prod},cantidad:${cant}}], comentarios: ""){
              ok
              pedido{
                numero
                reserva{
                  mesas{
                    edges{
                      node{
                        numero
                        sucursal{
                          numero
                        }
                      }
                    }
                  }
                }
             }
            }
          }`
        ,
      })
      .subscribe(({ data }) => {

        if (data.agregarPedidoRemoto.ok == true) {
          localStorage.setItem("numeroPedido-reserva", data.agregarPedidoRemoto.pedido.numero + "-" + reserva);
          localStorage.setItem("Pedidomesa", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.numero);
          localStorage.setItem("Pedidosucursal", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.sucursal.numero);
          this.resultadoModificar.next(true);
        } else {
          this.resultadoModificar.next(false);

        }



        // alert(data.agregarPedidoRemoto.pedido.numero)
      }, (error) => {
        this.resultadoModificar.next(false);

        console.log('error', error);
      });
    return this.resultadoModificar.asObservable();
  }


  modificarProductoComentario(reserva, comentarios): Observable<boolean> {
    // this.cant = this.cant + cant;
    // this.total = prod.precio * cant + this.total;
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            agregarPedidoRemoto(numeroReserva:${reserva},comentarios: "${comentarios}"){
              ok
              pedido{
                numero
                reserva{
                  mesas{
                    edges{
                      node{
                        numero
                        sucursal{
                          numero
                        }
                      }
                    }
                  }
                }
             }
            }
          }`,fetchPolicy:"no-cache"
      })
      .subscribe(({ data }) => {

        if (data.agregarPedidoRemoto.ok == true) {
          localStorage.setItem("numeroPedido-reserva", data.agregarPedidoRemoto.pedido.numero + "-" + reserva);
          localStorage.setItem("Pedidomesa", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.numero);
          localStorage.setItem("Pedidosucursal", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.sucursal.numero);
          this.resultadoModificar.next(true);
        } else {
          this.resultadoModificar.next(false);

        }



        // alert(data.agregarPedidoRemoto.pedido.numero)
      }, (error) => {
        this.resultadoModificar.next(false);

        console.log('error', error);
      });
    return this.resultadoModificar.asObservable();
  }
  insertProducto(prod: Producto, cant: number, reserva): boolean {  
    this.cant = this.cant + cant;
    this.total = prod.precio * cant + this.total;
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            agregarPedidoRemoto(numeroReserva:${reserva},listaProductos:[{numero:${prod.id},cantidad:${cant}}], comentarios: ""){
              ok
              pedido {
                numero
                reserva {
                  mesas {
                    edges {
                      node {
                        numero
                        sucursal {
                          numero
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          `
        ,
      })
      .subscribe(({ data }) => {
        localStorage.setItem("numeroPedido-reserva", data.agregarPedidoRemoto.pedido.numero + "-" + reserva);
        localStorage.setItem("Pedidomesa", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.numero);
        localStorage.setItem("Pedidosucursal", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.sucursal.numero);


        // alert(data.agregarPedidoRemoto.pedido.numero)
      }, (error) => {
        console.log('error', error);
      });
    return true;
  }
  insertRecompensa(recom: Recompensa, cant: number, reserva): boolean {  
    this.cant = this.cant + cant;
    this.total = 0 + this.total;
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            agregarPedidoRemoto(numeroReserva:${reserva},
              listaRecompensas:[{numero:${recom.id},cantidad:${cant}}],
              comentarios: ""){
              ok
              pedido {
                numero
                reserva {
                  mesas {
                    edges {
                      node {
                        numero
                        sucursal {
                          numero
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          `
        ,
      })
      .subscribe(({ data }) => {
        localStorage.setItem("numeroPedido-reserva", data.agregarPedidoRemoto.pedido.numero + "-" + reserva);
        localStorage.setItem("Pedidomesa", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.numero);
        localStorage.setItem("Pedidosucursal", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.sucursal.numero);


        // alert(data.agregarPedidoRemoto.pedido.numero)
      }, (error) => {
        console.log('error', error);
      });
    return true;
  }
  insertPromocion(promo: Promocion, cant: number, reserva): boolean {  
    this.cant = this.cant + cant;
    this.total = promo.precio * cant  + this.total;
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            agregarPedidoRemoto(numeroReserva:${reserva},
              listaPromociones:[{numero:${promo.numero},cantidad:${cant}}],
              comentarios: ""){
              ok
              pedido {
                numero
                reserva {
                  mesas {
                    edges {
                      node {
                        numero
                        sucursal {
                          numero
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          `
        ,
      })
      .subscribe(({ data }) => {
        localStorage.setItem("numeroPedido-reserva", data.agregarPedidoRemoto.pedido.numero + "-" + reserva);
        localStorage.setItem("Pedidomesa", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.numero);
        localStorage.setItem("Pedidosucursal", data.agregarPedidoRemoto.pedido.reserva.mesas.edges[0].node.sucursal.numero);


        // alert(data.agregarPedidoRemoto.pedido.numero)
      }, (error) => {
        console.log('error', error);
      });
    return true;
  }
  agregarPedidoPresencial(prod: ProductoPedido[], mesa: number, sucursal: number): Observable<boolean> {
    let lista: String = '';
    prod.forEach(data => {
      lista = lista + '{numero:' + data.numero + ',cantidad:' + data.cantidad + '},'
    })
    this.apollo
      .mutate({
        mutation: gql
          `mutation{
            agregarPedidoPresencial(numeroSucursal:${sucursal},
              numeroMesa:${mesa},
              listaProductos:${lista}){
              ok
            }
          }`
        ,
      })
      .subscribe(({ data }) => {
        if (data.agregarPedidoPresencial.ok == true) {
          this.respuestaAgregar.next(true);
        } else {
          this.respuestaAgregar.next(false);
          
        }
      }, (error) => {
        console.log(error)
        this.respuestaAgregar.next(false);

      });
    return this.respuestaAgregar.asObservable();
  }

  asignarCartaSucursal(idSucursal): Observable<boolean> {
    this.apollo
      .mutate({
        mutation: gql
          `mutation{
        agregarCartaSucursal(numeroCarta: ${this.numeroCarta}, numeroSucursal: ${idSucursal}){
         ok
  }
}
`
      })
      .subscribe(({ data }) => {
        if (data.agregarCartaSucursal.ok) {
          this.resultadoAsignar.next(true);
        }
      }, (error) => {
        console.log('error', error);
        this.resultadoAsignar.next(false);

      });
    return this.resultadoAsignar.asObservable();
  }

  crearCarta(listaProductos1: DetalleCartaProductoInput[]): Observable<boolean> {
    this.apollo
      .mutate({
        mutation: gql
          `mutation crearCarta($listaProductos: [DetalleCartaProductoInput]!) {
            crearCarta(listaProductos: $listaProductos){
             ok
             carta{
               numero
             }
      }
  }`, fetchPolicy: 'no-cache',
        variables: { listaProductos: listaProductos1 }
      })
      .subscribe(({ data }) => {
        if (data.crearCarta.ok) {
          this.numeroCarta = data.crearCarta.carta.numero;
          this.resultado.next(true);
        }
      }, (error) => {
        console.log('error', error);
        this.resultado.next(false);

      });

    return this.resultado.asObservable();
  }
  crearRecompensa(listaProductos1: DetalleCartaRecompensa[], nombre: string, puntos: number): Observable<boolean> {
    let lista: String = '';

    listaProductos1.forEach(data => {
      lista = lista + '{codigo:' + data.numero + ',cantidad:' + data.cantidad + '},'
    })
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            crearRecompensa(
              detalles: [${lista}]
              nombre:"${nombre}"
              puntos: ${puntos}
            ) {
              ok
              recompensa{
                numero
              }
            }
          }`
      })
      .subscribe(({ data }) => {
        if (data.crearRecompensa.ok == 0) {
          this.resultado.next(true);
        } else {
          this.resultado.next(false);
          
        }

      }, (error) => {
        this.resultado.next(false);

      });

    return this.resultado.asObservable();
  }
  getRecompensas(): Observable<Recompensa[]> {
    this.apollo.watchQuery<any>({
      query: gql
        `{
          verRecompensas {
            edges {
              node {
                nombre
                numero
                puntos
                detalles {
                  edges {
                    node {
                      producto {
                        nombre
                        codigo
                        foto
                      }
                      cantidad
                    }
                  }
                }
              }
            }
          }
        }`, fetchPolicy: 'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        
        let ELEMENT_DATA$: Recompensa[] = [];
      
        data.verRecompensas.edges.forEach(r => {
        let recompensa: Recompensa = { id: null, nombre: null, puntos: null, productos: [] }
         
          recompensa.id = r.node.numero;
          recompensa.puntos = r.node.puntos;
          recompensa.nombre = r.node.nombre;
          r.node.detalles.edges.forEach(p => {
            let producto: ProductoRecompensa = {nombre: null, id: 0, imagen: null, cantidad: 0 }
            producto.nombre = p.node.producto.nombre;
            producto.id = p.node.producto.codigo;
            producto.imagen = p.node.producto.foto;
            producto.cantidad = p.node.cantidad;
            recompensa.productos.push(producto);
          });
          ELEMENT_DATA$.push(recompensa);
        });
        this.ELEMENT_RECOMPENSA.next(ELEMENT_DATA$);
      });
    return this.ELEMENT_RECOMPENSA.asObservable();
  }

  getOpciones() {
    return this.opciones;
  }

  refresh() {
    this.listaProductos.next(this.productos);
  }


  refresh1() {
    this.ELEMENT_DATA.next(this.listaProductos1);
  }


  refreshCategoria() {
    this.listaCategorias.next(this.categorias);
  }

  getCantProduct(): number {
    return this.cant;
  }


  getTotal(): number {
    return this.total;
  }
}

export interface SelectedCarta {
  codigo: number;
  nombre: string;
  categoria: string;
  codigoCat: number;
  precio: number;
}

export interface ListaElement {
  codigo: number;
  nombre: string;
  categoria: string;
  codigoCat: number;
  foto: "";
  descripcion: string;

}
export interface Categoria {
  nombre: String;
  url: String;
  id: number;
}
export interface Opcion {
  nombre: String;
  imagen: String;
  id: number;
}
export interface Producto {
  nombre: String;
  imagen: String;
  id: number;
  precio: number;
  descripcion: string;
}
export interface ProductoRecompensa {
  nombre: String;
  imagen: String;
  id: number;
  cantidad: number;
}
export interface Recompensa {
  nombre: String;
  id: number;
  puntos: number;
  productos: ProductoRecompensa[];
}

export interface DetalleCartaProductoInput {
  numero: number;
  precio: number;
}
export interface DetalleCartaRecompensa {
  numero: number;
  cantidad: number;
}
export interface ProductoPedido {
  numero: number;
  cantidad: number;
}
export interface NuevoPedido {
  mesa: number;
  numeroSucursal: number;
  productos: ProductoPedido[];
}
