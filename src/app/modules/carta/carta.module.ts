import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerCartaComponent } from './components/ver-carta/ver-carta.component';

import { CartaRoutingModule } from './carta-routing.module';
import { ListaMenuCategoriaComponent } from './components/lista-menu-categoria/lista-menu-categoria.component';
import { ClrModalModule,ClrSelectModule,ClrFormsModule, ClrInputModule} from "@clr/angular";
import { TarjetaCategoriaComponent } from './components/tarjeta-categoria/tarjeta-categoria.component';
import { TarjetaProductoComponent } from './components/tarjeta-producto/tarjeta-producto.component';
import { ClarityModule,  } from '@clr/angular';
import { CrearCartaComponent } from './components/crear-carta/crear-carta.component';
import { AsignarCartaComponent } from './components/asignar-carta/asignar-carta.component';
import { EditarCartaComponent } from './components/editar-carta/editar-carta.component';
import {  ClrFormsNextModule } from '@clr/angular';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VerCartaGeneralComponent } from './components/ver-carta-general/ver-carta-general.component';
import { VerCartaPromocionesComponent } from './components/ver-carta-promociones/ver-carta-promociones.component';
import { VerCartaRecompensasComponent } from './components/ver-carta-recompensas/ver-carta-recompensas.component';
import { CrearRecompensaComponent } from './components/crear-recompensa/crear-recompensa.component';
import { VerRecompensasComponent } from './components/ver-recompensas/ver-recompensas.component';
import { TarjetaRecompensaComponent } from './components/tarjeta-recompensa/tarjeta-recompensa.component';
import { TarjetaPromocionesComponent } from './components/tarjeta-promociones/tarjeta-promociones.component';



@NgModule({
  imports: [
    CommonModule,
    CartaRoutingModule,
    ClarityModule,
    ClrModalModule,
    ClrSelectModule,
    ClrFormsModule,
    FormsModule,
    ClrFormsNextModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,
    ClrInputModule    
       

  ],
  declarations: [VerCartaComponent, ListaMenuCategoriaComponent, TarjetaCategoriaComponent, TarjetaProductoComponent, CrearCartaComponent, AsignarCartaComponent, EditarCartaComponent, VerCartaGeneralComponent, VerCartaPromocionesComponent, VerCartaRecompensasComponent, CrearRecompensaComponent, VerRecompensasComponent, TarjetaRecompensaComponent, TarjetaPromocionesComponent]
})
export class CartaModule { }
