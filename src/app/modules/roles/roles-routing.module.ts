import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { VerRolesComponent } from './components/ver-roles/ver-roles.component';
import { AltaRolComponent } from './components/alta-rol/alta-rol.component';
import { BajaRolComponent } from './components/baja-rol/baja-rol.component';
import { EditarRolComponent } from './components/editar-rol/editar-rol.component';
import { AuthService } from '../../security/login/auth.service';

 
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';


const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'verRoles',
                component: VerRolesComponent, canActivate: [AuthService],
                data: {acciones: ["ver_roles"]}
            },
            {
                path: 'bajaRol',
                component: BajaRolComponent, canActivate: [AuthService],
                data: {acciones: ["eliminar_rol"]}
            },
            {
                path: 'altaRol',
                component: AltaRolComponent, canActivate: [AuthService],
                data: {acciones: ["crear_rol"],
                    breadcrumb: 'Crear Rol'}
            },
            {
                path: 'editarRol',
                component: EditarRolComponent, canActivate: [AuthService],
                data: {acciones: ["modificar_rol"]}
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RolesRoutingModule { }
