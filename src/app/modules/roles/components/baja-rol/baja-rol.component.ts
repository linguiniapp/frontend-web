import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-baja-rol',
  templateUrl: './baja-rol.component.html',
  styleUrls: ['./baja-rol.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BajaRolComponent {
  
  complete : boolean = false;

  constructor(private routes: Router) { }

  volver(){
    this.routes.navigate(['/verRoles']);
  }
  volverMenu(){
    this.routes.navigate(['/']);
  }
  cerrar(){
    this.routes.navigate(['/verRoles']);
  }
}
