import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RolesService, ListaElement } from '../../services/roles.service';

@Component({
  selector: 'app-ver-roles',
  templateUrl: './ver-roles.component.html',
  styleUrls: ['./ver-roles.component.css']
})
export class VerRolesComponent implements OnInit {

  dataSource: ListaElement[]=[];
  selectedRol: boolean;

  constructor(private routes:Router, private rol: RolesService) { }

  ngOnInit() {
  }
  agregarRol(){
    this.routes.navigate(['/altaRol']);
  }
  eliminarRol(codigo: number){
    //this.rol.eliminarRol(codigo);
    this.routes.navigate(['/eliminarRol']);
  }
  editarRol(codigo: number){    
    this.routes.navigate(['/editarRol',codigo]);
  }

}
