import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesService, roles, acciones, rolesAcciones } from '../../services/roles.service';

@Component({
  selector: 'app-editar-rol',
  templateUrl: './editar-rol.component.html',
  styleUrls: ['./editar-rol.component.css'],
  exportAs: 'ngModel'
})
export class EditarRolComponent implements OnInit {

  categoria = [];
  roles: roles[] = [];
  rol: number = -1;
  eliminar: boolean = false;
  eliminado: boolean = false;
  nombreRol: string = "";
  complete: boolean = false;
  error: boolean = false;
  correcto: boolean = false;
  dataSource: acciones[] = [];
  dataSourceDefault: acciones[] = [];
  rolesAcciones: rolesAcciones[] = [];
  selectedAcciones: acciones[] = [];
  selectedAccionesHidden: acciones[] = [];
  selectedAccionesInit: acciones[] = [];


  constructor(private routes: Router, private service: RolesService) { }


  ngOnInit() {

    this.service.getRol().subscribe(data => {

      this.roles = data;




    });


    this.service.getRolAcciones().subscribe(data => {



      this.rolesAcciones = data;

      // this.rolChange();


    })

    this.service.getAcciones().subscribe(data => {
      // this.dataSource = data;
      this.dataSourceDefault = data;
    });


  }
  rolChange() {


    this.nombreRol = this.rolesAcciones.find(r => r.codigo == this.rol).nombre;
    // console.log(this.rol)
    this.selectedAccionesInit = this.rolesAcciones.find(r => r.codigo == this.rol).listaAcciones;

    this.selectedAccionesHidden = this.rolesAcciones.find(r => r.codigo == this.rol).listaAcciones;
    this.dataSource = this.dataSourceDefault;


    // this.serviceProducto.getProductoCategoria(this.codCategoria).subscribe(data => {
    //   this.dataSource = data;
    // });

    // this.serviceCarta.getProductosCarta(this.codCategoria, this.numeroSucursal).subscribe(data => this.select = data);


  }

  selected(codigo) {

    let accion = this.selectedAccionesHidden.find(a => a.codigo == codigo);

    if (accion) {

      return true;

    } else {
      return false;
    }
    // let prod = this.select.find(d => d.codigo == codigo);
    // if (prod) {
    //   return true
    // } else {
    //   return false
    // }

  }
  onSubmit(f: NgForm) {

    if (f.valid) {

      let agregarAcciones: number[] = [];
      let quitarAcciones: number[] = [];
      let codigo: number = this.rol;

      this.selectedAccionesInit.forEach(data => {

        let accion = this.selectedAcciones.find(d => d.codigo == data.codigo);
        if (accion) {

        } else {


          quitarAcciones.push(data.codigo);
        }

      });


      this.selectedAcciones.forEach(data => {

        let accion = this.selectedAccionesInit.find(d => d.codigo == data.codigo);
        if (accion) {

        } else {


          agregarAcciones.push(data.codigo);
        }

      });


      this.service.editarRol(agregarAcciones, quitarAcciones, codigo).subscribe(ok => {
        if (ok) {
          this.correcto = true;
          this.complete = false;
          this.error = false;
          this.rol = -1;
          this.cerrarModal();
        } else {
          this.complete = false;
          this.correcto = false;

          this.error = true;
        }
      });


    } else {

    }

  }
  volver() {
    this.routes.navigate(['/verCategorias']);
  }
  cerrar() {
    this.routes.navigate(['/verCategorias']);
  }



  onSearchChange(busqueda: string) {

    this.dataSource = this.dataSourceDefault;

    this.dataSource = this.dataSource.filter(data => data.nombre.includes(busqueda));



  }

  onSearchCodChange(busqueda) {

    this.dataSource = this.dataSourceDefault;

    if (busqueda != undefined && busqueda != "") {

      this.dataSource = this.dataSource.filter(data => data.codigo == busqueda);
    }


  }

  eliminarRol() {
    this.eliminar = true;

  }

  cerrarModal() {
    this.eliminar = false;
  }
  borrar() {
    this.service.eliminarRol(this.rol).subscribe(ok => {
      if (ok) {
        this.eliminado = true;
        this.cerrarModal();

      } else {
        this.error = true;
      }

    });
    this.eliminar = true;

  }
}
