import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RolesService, acciones } from '../../services/roles.service';
import { element } from 'protractor';
@Component({
  selector: 'app-alta-rol',
  templateUrl: './alta-rol.component.html',
  styleUrls: ['./alta-rol.component.css'],
  exportAs: 'ngModel'
})
export class AltaRolComponent {
  complete: boolean = false;
  error: boolean = false;
  correcto: boolean = false;
  dataSource: acciones[] = [];
  dataSourceDefault: acciones[] = [];

  selectedAcciones: acciones[] = [];

  constructor(private routes: Router, private promo: RolesService) { }

  ngOnInit() {

    this.promo.getAcciones().subscribe(data => {
      this.dataSource = data;
      this.dataSourceDefault = data;
    });

  }

  onSubmit(f: NgForm) {

    if (f.valid) {
      if (this.selectedAcciones.length != 0) {


let codigos = [];
this.selectedAcciones.forEach( data => {
  codigos.push(data.codigo);
})

        this.promo.agregarRol(codigos, f.value.nombre).subscribe(ok => {

          console.log("ok: " + ok)
          if (ok) {
            this.correcto = true;
            this.error = false;
            this.selectedAcciones = [];
            this.dataSource = this.dataSourceDefault;

          } else {
            this.correcto = false;
            this.error = true;
          }
        });

      }

      else {

      }

    }
    else {


    }

  }

  volver() {
    this.routes.navigate(['/verRoles']);
  }
  cerrar() {
    this.routes.navigate(['/verRoles']);
  }

  onSearchChange(busqueda : string ){

    this.dataSource = this.dataSourceDefault;

    this.dataSource = this.dataSource.filter( data => data.nombre.includes( busqueda));
        
    

  }

  onSearchCodChange(busqueda){

    this.dataSource = this.dataSourceDefault;

    if(busqueda != undefined && busqueda != ""){

    this.dataSource = this.dataSource.filter( data => data.codigo == busqueda);
    }
    
  }


}
