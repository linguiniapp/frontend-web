import { Injectable } from '@angular/core';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Action } from 'rxjs/internal/scheduler/Action';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  private lelem = new Subject<ListaElement>();
  private ELEMENT_DATA = new Subject<roles[]>();
  private rolesAcciones = new Subject<rolesAcciones[]>();
  private ELEMENT_DATA_ACCIONES = new Subject<acciones[]>();
  private response = new Subject<boolean>();

  // te encotrare un mañana
  //
  // acciones: acciones[] = [{ nombre: "editarSucursal", codigo: 1 }, { nombre: "eliminarSucursal", codigo: 2 }];

  constructor(private apollo: Apollo, private router: Router) {
    console.log("Ready to use");
  }

  getRol(): Observable<roles[]> {
    this.apollo.watchQuery<any>({
      query: gql

        `{
          verRoles{
            edges{
              node{
                codigo
                nombre
              }
            }
          }
        }`,
        fetchPolicy: 'no-cache'

    })
      .valueChanges
      .subscribe(({ data }) => {
        let ELEMENT_DATA$: roles[] = [];
        data.verRoles.edges.forEach(p => {
          let lelem: roles = {
            codigo: p.node.codigo,
            nombre: p.node.nombre
          };

          ELEMENT_DATA$.push(lelem);
          this.ELEMENT_DATA.next(ELEMENT_DATA$);

        });
      });
    return this.ELEMENT_DATA.asObservable();
  }



  getRolAcciones(): Observable<rolesAcciones[]> {
    this.apollo.watchQuery<any>({
      query: gql

        `{
          verRoles{
            edges{
              node{
                codigo
                nombre
                acciones{
                  edges{
                    node{
                      codigo
                      nombre
                    }
                  }
                }
              }
            }
          }
        }`

    })
      .valueChanges
      .subscribe(({ data }) => {
        let rolesAccione: rolesAcciones[] = [];
        data.verRoles.edges.forEach(p => {

          let acciones1: acciones[] = [];
          p.node.acciones.edges.forEach(element => {
            let accion: acciones = {
              codigo: element.node.codigo,
              nombre: element.node.nombre
            }

            acciones1.push(accion);
          });

          let lelem: rolesAcciones = {
            codigo: p.node.codigo,
            listaAcciones: acciones1,
            nombre: p.node.nombre

          };

          rolesAccione.push(lelem);
          this.rolesAcciones.next(rolesAccione);

        });
      });
    return this.rolesAcciones.asObservable();
  }


  getUnRol(codigo: number) {
    this.apollo.watchQuery<any>({
      query: gql

    })
      .valueChanges
      .subscribe(({ data }) => {

        let lelem: ListaElement = {
          codigo: data.verProducto.codigo,
          nombre: data.verProducto.nombre,


        };

        this.lelem.next(lelem);
      });
    return this.lelem.asObservable();

  }

  agregarRol(listaAcciones, nombreRol): Observable<boolean> {

    this.apollo
      .mutate({
        mutation: gql
          `mutation crearRol($listaAcciones: [Int]!, $nombre : String!) {
            crearRol(acciones: $listaAcciones, nombre: $nombre){
              ok
            }
          }
        `,

        fetchPolicy: 'no-cache',
        variables: { listaAcciones: listaAcciones, nombre: nombreRol }
      })
      .subscribe(({ data }) => {
        if (data.crearRol.ok == "0") {
          this.response.next(true)
        }

      }, (error) => {
        console.log('error', error);
        this.response.next(false);



      });

    return this.response.asObservable();

  }



  editarRol(agregarAcciones, quitarAcciones, codigo): Observable<boolean> {
   
    this.apollo
      .mutate({
        mutation: gql
          `mutation modificarRol($agregarAcciones: [Int], $quitarAcciones :[Int]) {
            modificarRol(agregarAcciones: $agregarAcciones, quitarAcciones: $quitarAcciones, codigo: ${codigo}){
              ok
            }
          }
        `,

        fetchPolicy: 'no-cache',
        variables: { agregarAcciones: agregarAcciones, quitarAcciones: quitarAcciones }
      })
      .subscribe(({ data }) => {
        if (data.modificarRol.ok == "0") {
          this.response.next(true)
        }

      }, (error) => {
        console.log('error', error);
        this.response.next(false);



      });

    return this.response.asObservable();

  }



  eliminarRol(codigo): Observable<boolean> {
   
    this.apollo
      .mutate({
        mutation: gql
          `mutation{
          eliminarRol(codigo: ${codigo}){
              ok
            }
          }
        `,

        fetchPolicy: 'no-cache',
      })
      .subscribe(({ data }) => {
        if (data.eliminarRol.ok == "0") {
          this.response.next(true)
        }

      }, (error) => {
        console.log('error', error);
        this.response.next(false);



      });

    return this.response.asObservable();

  }


  // ACCIONES!



  getAcciones(): Observable<acciones[]> {
    this.apollo.watchQuery<any>({
      query: gql

        `{
          verAcciones{
            edges{
              node{
                nombre
                codigo
              }
            }
          }
        }`, fetchPolicy: 'no-cache'

    })
      .valueChanges
      .subscribe(({ data }) => {
        let acciones: acciones[] = [];
        data.verAcciones.edges.forEach(p => {
          let lelem: acciones = {
            nombre: p.node.nombre,
            codigo: p.node.codigo
          };

          acciones.push(lelem);
          // ELEMENT_DATA_ACCIONES$.push(lelem);

        });

        this.ELEMENT_DATA_ACCIONES.next(acciones);

      });
    return this.ELEMENT_DATA_ACCIONES.asObservable();

  }

}

export interface ListaElement {
  codigo: number;
  nombre: string;
}

export interface rolesAcciones {
  codigo: number;
  nombre: string;
  listaAcciones: acciones[];
}

export interface acciones {
  nombre: string;
  codigo: number;
}

export interface roles {
  nombre: string;
  codigo: number;
}

