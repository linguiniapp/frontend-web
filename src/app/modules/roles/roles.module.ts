import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AltaRolComponent } from './components/alta-rol/alta-rol.component';
import { BajaRolComponent } from './components/baja-rol/baja-rol.component';
import { EditarRolComponent } from './components/editar-rol/editar-rol.component';
import { VerRolesComponent } from './components/ver-roles/ver-roles.component';

import { RolesRoutingModule } from './roles-routing.module';

import { ClarityModule, ClrFormsNextModule } from '@clr/angular';

@NgModule({
  imports: [
    CommonModule,
    ClrFormsNextModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,
    RolesRoutingModule
  ],
  declarations: [AltaRolComponent, BajaRolComponent, EditarRolComponent, VerRolesComponent]
})
export class RolesModule { }
