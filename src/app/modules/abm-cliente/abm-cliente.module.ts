import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AltaClienteComponent } from './components/alta-cliente/alta-cliente.component';
import { AbmClienteRoutingModule } from './abm-cliente-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule} from '@angular/forms';
import { ClarityModule, ClrFormsNextModule, ClrDatagridModule, /*ClrDatagridPagination, ClrDatagridActionOverflow*/} from '@clr/angular';
import { ListaClientesComponent } from './components/lista-clientes/lista-clientes.component';
import { VerClienteComponent } from './components/ver-cliente/ver-cliente.component';
import { EditarClienteComponent } from './components/editar-cliente/editar-cliente.component';


@NgModule({
  imports: [
    CommonModule,
     FormsModule,
     ReactiveFormsModule,
     AbmClienteRoutingModule,
     ClarityModule,
     ClrFormsNextModule,
     ClrDatagridModule,
  ],
     
  declarations: [ AltaClienteComponent, ListaClientesComponent, VerClienteComponent, EditarClienteComponent]
})
export class AbmClienteModule { }
