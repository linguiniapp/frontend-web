import { Component, OnInit } from '@angular/core';
import { AbmClienteService } from '../../services/abm-cliente.service';
import { Cliente, QueryClientes } from '../../types';
import { Router, ActivatedRoute, RouterModule } from '@angular/router';
import { Subject, Observable } from 'rxjs';

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent implements OnInit {
  queryClientes = new Subject<QueryClientes>();
  clientes = new Subject<Array<Cliente>>();
  // cliente$ = new QueryClientes();
  cliente$ = new Observable<Cliente[]>();

  constructor(private _abmClienteService: AbmClienteService, private router: Router) { }

  ngOnInit() {
    
this.cliente$ = this._abmClienteService.getAllClientes();
    
  }
  onVerPerfil(id:number){    
    this.router.navigate(['/ver-cliente',id]);   
  }
  onEdit(id:number){    
    this.router.navigate(['/editar-cliente',id]);   
  }

  registrar(){
    this.router.navigate(['/altaCliente']);   

  }

}
