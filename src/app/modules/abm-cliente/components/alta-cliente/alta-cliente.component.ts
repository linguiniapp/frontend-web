import { Component, OnInit } from '@angular/core';
import { AbmClienteService, DatosCliente } from '../../services/abm-cliente.service';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-alta-cliente',
  templateUrl: './alta-cliente.component.html',
  styleUrls: ['./alta-cliente.component.css'],
  exportAs: 'ngModel'

})

export class AltaClienteComponent {

  datosCliente: DatosCliente;
  password: string;
  username: string;
  isInvalid: boolean = false;
  complete: boolean = false;
  altaCliente = new FormGroup({
    nombre: new FormControl('', [
      Validators.required,
    ]),
    apellido: new FormControl('', [
      Validators.required,
    ]),
    direccion: new FormControl('', [
      Validators.required,
    ]),
    telefono: new FormControl('', [
      Validators.required,
    ]),
    sexo: new FormControl('', [
      Validators.required,
    ]),
    usuario: new FormControl('', [
      Validators.required,
    ]),
    dni: new FormControl('', [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.minLength(8)

    ]),
    emailFormControl: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    date: new FormControl('', [
      Validators.required,
    ]),
  }
  );
  constructor(private routes: Router, private _AbmClienteService: AbmClienteService) {
  }



  sexos: Sexo[] = [
    { value: "Masculino", },
    { value: "Femenino", },
    { value: "No quiero decirlo" }

  ];

  onSubmit(f: NgForm) {
    this.complete = false;
    if (f.valid && this.altaCliente.valid) {
      this.datosCliente = new DatosCliente();
      this.datosCliente.nombre = f.value.nombre;
      this.datosCliente.apellido = f.value.apellido;
      this.datosCliente.direccion = f.value.direccion;
      this.datosCliente.dni = f.value.dni;
      this.datosCliente.email = this.altaCliente.value.emailFormControl;
      this.datosCliente.fechaNacimiento = formatDate(f.value.date, "yyyy-MM-dd", "en-US");//.getFullYear()+ "-" + f.value.fechaNacimiento.getMonth(). + "-" +f.value.fechaNacimiento.getDate();
      this.datosCliente.sexo = f.value.sexo;
      this.datosCliente.telefono = f.value.telefono;
      this.password = f.value.password;
      this.username = f.value.usuario;
      this._AbmClienteService.guardarCliente(this.datosCliente, this.password, this.username).subscribe( ok => {
        if (ok) {


          this.routes.navigate(['/login']);
          
        } else {
          
        }
        
 
      });
    }
    else {
      this.complete = true;
      this.validateAllFormFields(this.altaCliente);
    }
  }
/**
 * Muestra cartel para completar los campos.
 *
 * @memberof AltaClienteComponent
 */
cerrar() {
    this.complete = !this.complete;
  }

  login() {
    this.routes.navigate(['/login']);

  }
  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
        control.updateValueAndValidity();
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
}
export interface Sexo {
  value: string;
}
