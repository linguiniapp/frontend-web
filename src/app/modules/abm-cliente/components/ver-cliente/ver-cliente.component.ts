import { Component, OnInit } from '@angular/core';
import { AbmClienteService, DatosCliente } from '../../services/abm-cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-ver-cliente',
  templateUrl: './ver-cliente.component.html',
  styleUrls: ['./ver-cliente.component.css']
})
export class VerClienteComponent implements OnInit {
  cliente: DatosCliente;
  constructor(private _abmClienteService: AbmClienteService, private router: Router, private activatedRoute: ActivatedRoute ) { }


  ngOnInit() {
    this.activatedRoute.params.subscribe( params =>{
      this._abmClienteService.getCliente(params['id']).subscribe(cliente => { 
        this.cliente=cliente;
        this.cliente.fechaNacimiento = formatDate(this.cliente.fechaNacimiento, 'd/M/yy', "en-US" );
      });
    });
  }
}
