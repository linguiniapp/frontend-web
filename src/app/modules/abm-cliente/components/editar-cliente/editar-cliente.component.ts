import { Component, OnInit, AfterContentInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { DatosCliente, AbmClienteService } from '../../services/abm-cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Sexo } from '../alta-cliente/alta-cliente.component';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css'],
  exportAs: 'ngModel'
})
export class EditarClienteComponent implements OnInit {
  idCliente: number;
  cliente = new DatosCliente();
  clienteFNac: Date;
  constructor(private _abmClienteService: AbmClienteService, private router: Router, private activatedRoute: ActivatedRoute) { }
  password: string;
  username: string;
  isInvalid: boolean = false;
  numero: number = 0;
  complete: boolean = false;
  sexos: Sexo[] = [
    { value: "Masculino", },
    { value: "Femenino", },
    { value: "No quiero decirlo" }
  ];
  editarClienteGroup = new FormGroup({
    nombre: new FormControl('', [Validators.required]),
    apellido: new FormControl('', [Validators.required]),
    direccion: new FormControl('', [Validators.required]),
    telefono: new FormControl('', [Validators.required]),
    sexo: new FormControl('', [Validators.required]),
    dni: new FormControl('', [Validators.required, Validators.maxLength(8)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    fecha: new FormControl('', [Validators.required])
  });

  // dateForm = new FormGroup({date: new FormControl()});

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      this.idCliente = params['id'];
      this._abmClienteService.getCliente(this.idCliente).subscribe(cliente => {
        this.cliente = cliente;
        this.clienteFNac = new Date(cliente.fechaNacimiento);
        this.editarClienteGroup.setValue({
          nombre: this.cliente.nombre,
          apellido: this.cliente.apellido,
          direccion: this.cliente.direccion,
          telefono: this.cliente.telefono,
          sexo: this.cliente.sexo,
          dni: this.cliente.dni,
          email: this.cliente.email,
          fecha: this.cliente.fechaNacimiento
        });
      });
    });
  }
  onSubmit(f: NgForm) {

    this.complete = false;
    if ( this.editarClienteGroup.valid) {
      let datos: DatosCliente = {
        nombre: this.editarClienteGroup.value.nombre,
        apellido: this.editarClienteGroup.value.apellido,
        sexo: this.editarClienteGroup.value.sexo,
        dni: this.editarClienteGroup.value.dni,
        telefono: this.editarClienteGroup.value.telefono,
        fechaNacimiento: formatDate(this.editarClienteGroup.value.fecha, "yyyy-MM-dd", "en-US"),
        email: this.editarClienteGroup.value.email,
        direccion: this.editarClienteGroup.value.direccion


      }

      alert( parseInt(this.editarClienteGroup.value.dni) + " " + this.editarClienteGroup.value.dni)

      this._abmClienteService.modificarCliente(datos, parseInt(this.editarClienteGroup.value.dni));
    }
    else {
      this.complete = true;

    }


  }
}
