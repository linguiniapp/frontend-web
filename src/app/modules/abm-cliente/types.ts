import { Observable } from "apollo-client/util/Observable";

export class QueryClientes {
    infoaux: InfoAuxCliente;
    clientes: Cliente[];
}
export class InfoAuxCliente {
    endCursor: string;
    hasNextPage: boolean;
}
export class Cliente {
    id: number;
    apellido: string;
    nombre: string;
    telefono: string;
    email: string;
}
