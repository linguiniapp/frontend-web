import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AltaClienteComponent } from './components/alta-cliente/alta-cliente.component';
import { ListaClientesComponent } from './components/lista-clientes/lista-clientes.component';
import { VerClienteComponent } from './components/ver-cliente/ver-cliente.component';
import { EditarClienteComponent } from './components/editar-cliente/editar-cliente.component';
import { AuthService } from '../../security/login/auth.service';

import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';

const routes: Routes = [
  {
      path: '',
      component: AuxviewComponent,
      children: [

          {
              path: 'lista-clientes',
              component: ListaClientesComponent, canActivate: [AuthService],
              data: {acciones: ["ver_clientes"]}
          },
          {
            path: 'ver-cliente/:id',
            component: VerClienteComponent, canActivate: [AuthService],
            data: {acciones: ["ver_clientes"]}
          },
          {
            path: 'editar-cliente/:id',
            component: EditarClienteComponent, canActivate: [AuthService],
            data: {acciones: ["modificar_cliente"]}
          }
      ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AbmClienteRoutingModule { }
