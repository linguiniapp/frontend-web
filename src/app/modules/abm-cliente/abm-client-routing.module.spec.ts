import { AbmClienteRoutingModule } from './abm-cliente-routing.module';

describe('RoutingModule', () => {
  let routingModule: AbmClienteRoutingModule;

  beforeEach(() => {
    routingModule = new AbmClienteRoutingModule();
  });

  it('should create an instance', () => {
    expect(routingModule).toBeTruthy();
  });
});
