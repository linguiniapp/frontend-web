import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular-boost';
import { QueryClientes, Cliente, InfoAuxCliente } from '../types';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AbmClienteService {
  private cliente = new Subject<DatosCliente>();
  private clientesArray = new Array<Cliente>();
  private ClientesResponse = new Subject<Cliente[]>();
  private respuesta = new Subject<boolean>();
  private responseRol = new Subject<boolean>();
  private nombreUsuario = new Subject<verifCliente>();
  private verifUsuarioSub = new Subject<verifUsuario>();

  constructor(private apollo: Apollo) {

  }
  private refresh() {
    // Emitir los nuevos valores para que todos los que dependan se actualicen.
    this.ClientesResponse.next(this.clientesArray);
  }



  
  asignarRol(codigoRol, empleado): Observable<boolean>{
    // , password: "${password}", username: "${username}"
    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            asignarRol(codigoRol:${codigoRol}, numeroOCuilEmpleado: "${empleado}") {
                ok
                    }
           }`
        ,
      })
      .subscribe(({ data }) => {

        if (data.asignarRol.ok == "0") {
          this.responseRol.next(true);
        }
        else{

          this.responseRol.next(false);
        }

      }, (error) => {

        this.responseRol.next(false);

        console.log('there was an error sending the query', error);
      });

      return this.responseRol.asObservable();


  }

  guardarCliente(datosCliente, password, username): Observable<boolean> {

    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            crearUsuarioCliente(datosCliente: {
                nombre: "${datosCliente.nombre}",
                apellido: "${datosCliente.apellido}" ,
                sexo: "${datosCliente.sexo}",
                email: "${datosCliente.email}",
                telefono: "${datosCliente.telefono}",
                fechaNacimiento: "${datosCliente.fechaNacimiento}",
                dni: "${datosCliente.dni}",
                direccion: "${datosCliente.direccion}"
              }, 
              password: "${password}",
              username: "${username}") {
                                          ok
                                          email
                                        }
            }`
        ,
      })
      .subscribe(({ data }) => {
        if (data.crearUsuarioCliente.ok == true) {
          // this.getAllClientes();
          this.respuesta.next(true);

        }
        else {
          this.respuesta.next(false);
        }

      }, (error) => {
        console.log('there was an error sending the query', error);
        this.respuesta.next(false);

      });

    return this.respuesta.asObservable();

  }


  modificarCliente(datosCliente, dni): Observable<boolean> {

    this.apollo
      .mutate({
        mutation: gql
          `mutation {
            modificarCliente(infoCliente: {
                nombre: "${datosCliente.nombre}",
                apellido: "${datosCliente.apellido}" ,
                sexo: "${datosCliente.sexo}",
                email: "${datosCliente.email}",
                telefono: "${datosCliente.telefono}",
                fechaNacimiento: "${datosCliente.fechaNacimiento}",
                dni: "${datosCliente.dni}",
                direccion: "${datosCliente.direccion}"
              }, numeroODni: ${dni} )
              {
                ok
              
              }
            }`
        ,
      })
      .subscribe(({ data }) => {
        if (data.modificarCliente.ok == "true") {
          this.getAllClientes();
          this.respuesta.next(true);

        }
        else {
          this.respuesta.next(false);
        }

      }, (error) => {
        console.log('there was an error sending the query', error);
        this.respuesta.next(false);

      });

    return this.respuesta.asObservable();

  }


  getAllClientes(): Observable<Cliente[]> {
    this.apollo
      .watchQuery<any>({
        query: gql
          `{
          verClientes (sort:[apellido_asc,nombre_asc], first:10){
            pageInfo{
               endCursor,
               hasNextPage
            }
            edges{
              node{
                numero
                apellido,
                nombre,
                telefono,
                email
              }
            }
          }
        }`
      }).valueChanges
      .subscribe(({ data }) => {

        let InfoAuxCliente: InfoAuxCliente;
        data.verClientes.edges.forEach(element => {
          let cliente: Cliente = {
            id: element.node.numero,
            apellido: element.node.apellido,
            nombre: element.node.nombre,
            email: element.node.email,
            telefono: element.node.telefono
          };
          this.clientesArray.push(cliente);
        });
        /*let aux: InfoAuxCliente = {
          endCursor: data.verClientes.pageInfo.endCursor,
          hasNextPage: data.verClientes.pageInfo.hasNextPage
        };
        clientesData.infoaux = aux;*/
        this.ClientesResponse.next(this.clientesArray);
      });
    return this.ClientesResponse.asObservable();
  }

  getCliente(nroCliente: number): Observable<DatosCliente> {
    this.apollo.watchQuery<any>({
      query: gql`{
        verCliente(numeroCliente:${nroCliente}){
          oid
          nombre
          apellido
          sexo
          email
          telefono
          fechaNacimiento
          tipo
          dni
          direccion
          numero
          estadoOid
          usuario{
            nombre
          }
        }}`
    }).valueChanges
      .subscribe(({ data }) => {
        let elcliente: DatosCliente = {
          apellido: data.verCliente.apellido,
          nombre: data.verCliente.nombre,
          dni: data.verCliente.dni,
          email: data.verCliente.email,
          direccion: data.verCliente.direccion,
          // fechaNacimiento: (data.verCliente.fechaNacimiento, "M/d/yyyy", "en-US"), // Para el DatePicker de Clarity
          fechaNacimiento: data.verCliente.fechaNacimiento, // Para el DatePicker de Clarity
          sexo: data.verCliente.sexo,
          telefono: data.verCliente.telefono
        };
        this.cliente.next(elcliente)
      });
    return this.cliente.asObservable();

  }



  verifCliente(): Observable<verifCliente> {
    let nombre: string = "";
    this.apollo.watchQuery<any>({
      query: gql`{
        verificarUsuario{
          nombre
          persona{
            nombre
            id
          }
        }
      }`,fetchPolicy: 'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        let cliente: verifCliente = {
          nombre: data.verificarUsuario.persona.nombre,

          id: data.verificarUsuario.persona.id
        }
        this.nombreUsuario.next(cliente);
      }, (error) => {
        let cliente: verifCliente = {
          nombre: "admin",

          id:12
        }
        this.nombreUsuario.next(cliente);
      });
    return this.nombreUsuario.asObservable();
  }

  verifUsuario(): Observable<verifUsuario> {
    let nombre: string = "";
    this.apollo.watchQuery<any>({
      query: gql`
      {
        verificarUsuario {
          tipoUsuario {
            nombre
          }
          datosPersona {
            ... on EmpleadoNode{
              sucursal{
                numero
              }
            }
          }
        }
      }
      
`,fetchPolicy: 'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        let cliente: verifUsuario = {
          nombreTipo: data.verificarUsuario.tipoUsuario.nombre,

          sucursal: data.verificarUsuario.datosPersona.sucursal.numero
        }
        this.verifUsuarioSub.next(cliente);
      }, (error) => {
        // let cliente: verifCliente = {
        //   nombre: "admin",

        //   id:12
        // }
        // this.verifUsuarioSub.next(cliente);
      });
    return this.verifUsuarioSub.asObservable();
  }
}

export class DatosCliente {
  nombre: string;
  apellido: string;
  sexo: string;
  email: string;
  telefono: string;
  fechaNacimiento: string;
  dni: string;
  direccion: string;
}


export interface verifCliente {
  nombre: string;
  id: number
}


export interface verifUsuario {
  nombreTipo: string;
  sucursal: number
}