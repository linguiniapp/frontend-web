import { TestBed, inject } from '@angular/core/testing';

import { AbmClienteService } from './abm-cliente.service';

describe('AbmClienteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AbmClienteService]
    });
  });

  it('should be created', inject([AbmClienteService], (service: AbmClienteService) => {
    expect(service).toBeTruthy();
  }));
});
