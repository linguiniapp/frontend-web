import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AdminPanelComponent } from "./components/admin-panel/admin-panel.component";
import { AuthService } from '../../security/login/auth.service';



import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { ClientePanelComponent } from './components/admin-panel/cliente-panel/cliente-panel.component';
import { acciones } from '../roles/services/roles.service';

const routes: Routes = [
  {
      path: '',
      component: AuxviewComponent,
      children: [
          {
              path: 'admin-panel',
              component: AdminPanelComponent, canActivate: [AuthService],
              data: {acciones: ["ver_panel_admin"]}
          },
          {
            path: 'cliente-panel',
            component: ClientePanelComponent
          }
        ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminPanelRoutingModule { }
