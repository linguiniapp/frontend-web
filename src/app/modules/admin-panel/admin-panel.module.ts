import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AdminPanelRoutingModule } from "./admin-panel-routing-module";
import {ClarityModule} from '@clr/angular';
import { ClientePanelComponent } from './components/admin-panel/cliente-panel/cliente-panel.component';
@NgModule({
  imports: [
    CommonModule,
    AdminPanelRoutingModule,
    ClarityModule
  ],
  declarations: [AdminPanelComponent, ClientePanelComponent]
})
export class AdminPanelModule { }
