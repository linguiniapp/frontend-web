import { AdminPanelRoutingModule } from './admin-panel-routing-module';

describe('RoutingModule', () => {
  let routingModule: AdminPanelRoutingModule;

  beforeEach(() => {
    routingModule = new AdminPanelRoutingModule();
  });

  it('should create an instance', () => {
    expect(routingModule).toBeTruthy();
  });
});
