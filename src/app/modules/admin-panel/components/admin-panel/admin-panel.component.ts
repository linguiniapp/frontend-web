import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';
import { SucursalService, ListaElement } from 'src/app/modules/sucursales/services/sucursal.service';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';


@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements AfterViewInit, OnInit {
    subscription;
    sucursales: ListaElement[] =[];
    graficoBarra: Chart;
    graficoLineaReviews: Chart;
    cantReservasSemanal: number = 25;
    ingresoMensual:number = 32000;
    porcentajeIncrementoIngresos: number = 15.1;
    valoracionPromedio = 4.2;
    tpoEsperaPromedio = 16;
    porcentajeIncrementoTiempoEspera = 8.2;
    mejorMesResenas = 'Septiembre';
    peorMesResenas = 'Mayo';
    
  constructor(private router: Router, private activatedRoute: ActivatedRoute , private _sucursalService: SucursalService ) { }
  ngOnInit(){
    this.subscription =this._sucursalService.getLista().subscribe(data => {
        data.forEach(sucursal => {
            let Sucursal: ListaElement = {
                direccion: sucursal.direccion,
                foto: sucursal.foto,
                horaDesde: sucursal.horaDesde,
                horaHasta: sucursal.horaHasta,
                latitud: sucursal.latitud,
                longitud: sucursal.longitud,
                nombre: sucursal.nombre,
                numero: sucursal.numero,
                telefono: sucursal.telefono
            };
            this.sucursales.push(Sucursal);
        });
    });
}
ngOnDestroy(): void {
    this.subscription.unsubscribe();
    
}
  ngAfterViewInit() {
    this.graficoBarra = new Chart('barChart', {
      type: 'bar',
      data: {
          labels: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
          datasets: [{
              label: 'Cant. Reservas',
              data: [12, 19, 3, 5, 2, 3, 7],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)',
                  'rgba(255, 168, 46, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(255, 168, 46, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });
  this.graficoLineaReviews = new Chart('lineChart', {
    type: 'line',
    data: {
            labels: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre', 'Noviembre', 'Diciembre'],
            datasets: [{ 
                data: [4, 4.2, 4.7, 4, 3.8, 4.1, 4.6, 4.3, 4.7, 4.7, 4.1, 4.4],
                label: "Sucursal 1",
                borderColor: "#3e95cd",
                fill: false
              }, 
            ]
          },
          options: {
            title: {
              display: true,
              text: 'Evolución de las valoraciones de los clientes (anual)'
            }
          }
        });
    }
    redirigir( funcionalidad: string ){
        switch (funcionalidad){
            case 'reservas':
                this.router.navigate(['/reservas']);
                break;
            case 'pedidos':
                this.router.navigate(['/pedidos']);
                break;
        }
    }
}
