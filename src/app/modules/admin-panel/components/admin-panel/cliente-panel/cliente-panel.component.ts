import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SucursalService, ListaElement } from 'src/app/modules/sucursales/services/sucursal.service';
@Component({
  selector: 'app-cliente-panel',
  templateUrl: './cliente-panel.component.html',
  styleUrls: ['./cliente-panel.component.css']
})
export class ClientePanelComponent implements OnInit {
  tienePedidos = true;
  reservasPendientes = true;
  subscription;
  constructor(private router: Router, private activatedRoute: ActivatedRoute , private _sucursalService: SucursalService ){}
  sucursales: ListaElement[] =[];
  ngOnInit() {
    this.subscription =this._sucursalService.getLista().subscribe(data => {
      data.forEach(sucursal => {
          let Sucursal: ListaElement = {
              direccion: sucursal.direccion,
              foto: sucursal.foto,
              horaDesde: sucursal.horaDesde,
              horaHasta: sucursal.horaHasta,
              latitud: sucursal.latitud,
              longitud: sucursal.longitud,
              nombre: sucursal.nombre,
              numero: sucursal.numero,
              telefono: sucursal.telefono
          };
          this.sucursales.push(Sucursal);
        });
      });
      console.log(this.sucursales);
  }

}
