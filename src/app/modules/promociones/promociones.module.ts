import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { VerPromocionesComponent } from './components/ver-promociones/ver-promociones.component';
import { CrearPromocionesComponent } from './components/crear-promociones/crear-promociones.component';
import { EliminarPromocionesComponent } from './components/eliminar-promociones/eliminar-promociones.component';
import { EditarPromocionesComponent } from './components/editar-promociones/editar-promociones.component';

import { ClarityModule, ClrFormsNextModule, ClrCheckboxModule , ClrDatagridColumnToggle, ClrDatagridModule } from '@clr/angular';

import { PromocionesRoutingModule } from './promociones-routing.module';
import { VistaComponent } from './components/vista/vista.component';

@NgModule({
  imports: [
    CommonModule,
    PromocionesRoutingModule,
    ClarityModule,
    ClrFormsNextModule,
    ClrCheckboxModule,
    FormsModule,
    
    ClrDatagridModule
  ],
  declarations: [VerPromocionesComponent, CrearPromocionesComponent, EliminarPromocionesComponent, EditarPromocionesComponent, VistaComponent]
})
export class PromocionesModule { }
