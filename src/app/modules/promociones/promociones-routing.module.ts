import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CrearPromocionesComponent } from './components/crear-promociones/crear-promociones.component';
import { EditarPromocionesComponent } from './components/editar-promociones/editar-promociones.component';
import { EliminarPromocionesComponent } from './components/eliminar-promociones/eliminar-promociones.component';
import { VerPromocionesComponent } from './components/ver-promociones/ver-promociones.component';

import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';


const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'verPromo',
                component: VerPromocionesComponent
            },
            {
                path: 'editarPromo',
                component: EditarPromocionesComponent
            },
            {
                path: 'eliminarPromo',
                component: EliminarPromocionesComponent
            },
            {
                path: 'crearPromo',
                component: CrearPromocionesComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PromocionesRoutingModule { }
