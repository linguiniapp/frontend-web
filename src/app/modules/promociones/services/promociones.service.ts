import { Injectable } from '@angular/core';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Time } from '@angular/common';
import { DirectiveAst } from '@angular/compiler';
import { disableDebugTools } from '@angular/platform-browser';

@Injectable()
export class PromocionesService {

    promo: verPromo[] = [];
    private ELEMENT_DATA = new Subject<ListaElement[]>();
    private lelem = new Subject<ListaElement>();
    private ELEMENT_DATAPER = new Subject<periodicidad[]>();

    constructor(private apollo: Apollo, private router: Router) {
        console.log("Ready to use");
    }

    getPromo(): Observable<ListaElement[]> {
        this.apollo.watchQuery<any>({
            query: gql
                `{ verPromociones(numeroSucursal:3){
                      edges{
                        node{
                          numero
                          descripcion
                          nombre
                          descuento
                          precio
                          diaDesde
                          diaHasta
                          periodicidad{
                              nombre
                              codigo
                          }
                          weekDay{
                            lunes
                            martes
                            miercoles
                            jueves
                            viernes
                            sabado
                            domingo
                          }
                          horaDesde
                          horaHasta
                          mesDesde
                          mesHasta
                          detalles{
                            edges{
                              node{
                                cantidad
                                producto{
                                  codigo
                                  nombre
                                  foto
                                  categorias{
                                    edges{
                                      node{
                                        codigo
                                        nombre
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  
                }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({ data }) => {
                let ELEMENT_DATA$: ListaElement[] = [];
                data.verPromociones.edges.forEach(p => {
                    let detalleProductos: DetalleProductoInput[] = [];
                    let diasActiva: string = ""
                    let wd = p.node.weekDay
                    if (wd) {
                        if (wd.lunes)
                            diasActiva += "Lunes, "
                        if (wd.martes)
                            diasActiva += "Martes, "
                        if (wd.miercoles)
                            diasActiva += "Miércoles, "
                        if (wd.jueves)
                            diasActiva += "Jueves, "
                        if (wd.viernes)
                            diasActiva += "Viernes, "
                        if (wd.diasActiva)
                            diasActiva += "Sábado, "
                        if (wd.diasActiva)
                            diasActiva += "Domingo, "
                        if (diasActiva.length > 0)
                            diasActiva = diasActiva.substring(0, diasActiva.length - 2)
                    }
                    let lelem: ListaElement = {
                        numero: p.node.numero,
                        descripcion: p.node.descripcion,
                        nombre: p.node.nombre,
                        descuento: p.node.descuento,
                        precio: p.node.precio,
                        diaDesde: p.node.diaDesde,
                        diaHasta: p.node.diaHasta,
                        periodicidad: p.node.periodicidad.nombre,
                        diasActivaString: diasActiva,
                        diasActiva: wd,
                        horaDesde: p.node.horaDesde,
                        horaHasta: p.node.horaHasta,
                        mesDesde: p.node.mesDesde,
                        mesHasta: p.node.mesHasta,
                        detallesProductos: []
                    }
                    p.node.detalles.edges.forEach(detalle => {
                        let detalleProducto: DetalleProductoInput = { nombre: "", foto: "", cantidad: 0, numero: 0 }
                        detalleProducto.cantidad = detalle.node.cantidad;
                        detalleProducto.numero = detalle.node.producto.codigo;
                        detalleProducto.nombre = detalle.node.producto.nombre;
                        detalleProducto.foto = detalle.node.producto.foto;
                        detalleProductos.push(detalleProducto)
                    });
                    lelem.detallesProductos = detalleProductos;
                    ELEMENT_DATA$.push(lelem);
                    this.ELEMENT_DATA.next(ELEMENT_DATA$);
                });
            });
        return this.ELEMENT_DATA.asObservable();
    }


    getElemento(idx: string) {
        return this.ELEMENT_DATA[idx];
    }
    agregarPromo(elemento: ListaElement, detallesProductos: DetalleProductoInput[]) {
        let lista: String = '';
        detallesProductos.forEach(data => {
            lista = lista + '{numero:' + data.numero + ',cantidad:' + data.cantidad + '},'
        })
        this.apollo
            .mutate({
                mutation: gql
                    `mutation crearPromocion{
                crearPromocion(detalleProductos: [${lista}],
                descuento: ${elemento.descuento}, 
                descripcion: "${elemento.descripcion}", 
                nombre: "${elemento.nombre}", 
                diaDesde: ${elemento.diaDesde}, 
                diaHasta: ${elemento.diaHasta}, 
                diasActiva: {
                    lunes: ${elemento.diasActiva.lunes},
                    martes: ${elemento.diasActiva.martes},
                    miercoles: ${elemento.diasActiva.miercoles},
                    jueves: ${elemento.diasActiva.jueves},
                    viernes: ${elemento.diasActiva.viernes},
                    sabado: ${elemento.diasActiva.sabado},
                    domingo: ${elemento.diasActiva.domingo},
                }
                horaDesde: "${elemento.horaDesde}", 
                horaHasta: "${elemento.horaHasta}",
                mesDesde: ${elemento.mesDesde}, 
                mesHasta: ${elemento.mesHasta},
                precio: ${elemento.precio}){
                  ok
                }
              }`
                ,
            })
            .subscribe(({ data }) => {
            }, (error) => {
            });
        return true;
    }
    quitarPromo(numero) {
        this.apollo
            .mutate({
                mutation: gql
                    `mutation{
            deshabilitarPromocion(numero: ${numero} ){
              ok
            }
          }`,
            }).subscribe(({ data }) => {

            },
                (error) => {
                    alert("error: " + error);

                }

            );
    }
    // editarPromo(producto:ListaElement){
    //     this.apollo
    //     .mutate({
    //         mutation: gql
    //         `mutation{
    //             crearProducto(nombre: "${producto.nombre}"){
    //                 ok
    //             }
    //         }
    //         `,
    //     })
    //     .subscribe();
    // }


    getPeriodicidad(): Observable<periodicidad[]> {
        this.apollo.watchQuery<any>({
            query: gql
                `{ 
                    verPeriodicidad {
                      edges {
                        node {
                          nombre
                          codigo
                        }
                      }
                    }
                  
                      
                    }`
        })
            .valueChanges
            .subscribe(({ data }) => {
                let ELEMENT_DATAPER$: periodicidad[] = [];

                data.verPeriodicidad.edges.forEach(p => {
                    let lelem: periodicidad = {
                        codigo: p.node.codigo,
                        nombre: p.node.nombre
                    };
                    ELEMENT_DATAPER$.push(lelem);
                    this.ELEMENT_DATAPER.next(ELEMENT_DATAPER$);


                });
            });
        return this.ELEMENT_DATAPER.asObservable();
    }

}



export interface ListaElement {
    numero: number;
    descripcion: string;
    descuento: number;
    diaDesde: number;
    diaHasta: number;
    diasActiva: WeekMaaskInput;
    horaDesde: string;
    horaHasta: string;
    nombre: string;
    periodicidad: string;
    precio: number;
    mesDesde: number;
    mesHasta: number;
    diasActivaString: string,
    detallesProductos: DetalleProductoInput[];

}
export interface WeekMaaskInput {
    lunes: boolean;
    martes: boolean;
    miercoles: boolean;
    jueves: boolean;
    viernes: boolean;
    sabado: boolean;
    domingo: boolean;
}
export interface DetalleProductoInput {
    cantidad: number;
    nombre: string;
    numero: number;
    foto: string;
}

export interface verPromo {
    descripcion: string;
    descuento: number;
    diaDesde: number;
    diaHasta: number;
    diasActiva: string;
    horaDesde: string;
    horaHasta: string;
    nombre: string;
    periodicidad: string;
    precio: number;
}

export interface periodicidad {
    codigo: number;
    nombre: string;
}
// export interface diasActiva{
//     lunes: boolean,
//     martes: boolean,
//     miercoles: boolean,
//     jueves: boolean,
//     viernes: boolean,
//     sabado: boolean,
//     domingo: boolean,
// }