import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarPromocionesComponent } from './editar-promociones.component';

describe('EditarPromocionesComponent', () => {
  let component: EditarPromocionesComponent;
  let fixture: ComponentFixture<EditarPromocionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarPromocionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarPromocionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
