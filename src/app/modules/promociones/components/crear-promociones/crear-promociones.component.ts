import { Component } from '@angular/core';
import { NgForm, Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { PromocionesService, ListaElement, periodicidad, WeekMaaskInput, DetalleProductoInput } from '../../services/promociones.service';
import { ProductosService, ListaElement as Productos, SelectedProd } from '../../../productos/services/productos.service';


@Component({
  selector: 'app-crear-promociones',
  templateUrl: './crear-promociones.component.html',
  styleUrls: ['./crear-promociones.component.css'],
  exportAs: 'ngModel'
})
export class CrearPromocionesComponent {

  complete: boolean = false;
  dataSourcePer: periodicidad[] = [];
  termsAgreement: boolean;
  codPeriodicidad: number;
  dataSource: Productos[] = [];
  selectedProd: SelectedProd[] = [];

  weekMask: WeekMaaskInput = {
    lunes: false,
    martes: false,
    miercoles: false,
    jueves: false,
    viernes: false,
    sabado: false,
    domingo: false,
  }
  detallesProd: DetalleProductoInput[] = [];


  nuevaPromo: ListaElement = {
    numero: 0,
    descripcion: "",
    descuento: 0,
    diaDesde: 0,
    diaHasta: 0,
    nombre: "",
    precio: 0,
    horaDesde: "",
    horaHasta: "",
    periodicidad: "",
    diasActivaString: "",
    diasActiva: this.weekMask,
    mesDesde: 0,
    mesHasta: 0,
    detallesProductos: this.detallesProd  

  }

  constructor(private routes: Router, private promo: PromocionesService, private serviceProd: ProductosService) { }


  ngOnInit() {

    this.promo.getPeriodicidad().subscribe(data => this.dataSourcePer = data);
    this.serviceProd.getProducto().subscribe(data => this.dataSource = data);

  }
  onSubmit(f: NgForm) {

    // alert( formatDate(f.value.dateDesde, "MM-dd", "en-US"));
    // alert( formatDate(f.value.date, "MM-dd", "en-US"));
    this.selectedProd.forEach(element => {
      if (element.cantidad == undefined || element.cantidad == 0) {
        alert("Ingrese la cantidad del producto " + element.nombre);
        this.complete = true;
        return;


      }

      let detalleProdutos: DetalleProductoInput = {
        numero: element.codigo,
        cantidad: element.cantidad,
        nombre: element.nombre,
        foto: ""
      }
      this.detallesProd.push(detalleProdutos);

    });

    if (f.valid) {
      this.nuevaPromo.descripcion = f.value.desc;
      this.nuevaPromo.descuento = 0;
      this.nuevaPromo.diaDesde = f.value.vigdesde;
      this.nuevaPromo.diaHasta = f.value.vighasta;
      this.nuevaPromo.nombre = f.value.nombre;
      this.nuevaPromo.precio = f.value.precio;
      this.nuevaPromo.horaDesde = f.value.horadesde;
      this.nuevaPromo.horaHasta = f.value.horahasta;
      this.nuevaPromo.mesDesde = 0;
      this.nuevaPromo.mesHasta = 0;
      this.nuevaPromo.detallesProductos = this.detallesProd;
      this.nuevaPromo.periodicidad = f.value.per;
      this.nuevaPromo.diasActiva = this.weekMask;



      this.promo.agregarPromo(this.nuevaPromo, this.nuevaPromo.detallesProductos);
      this.routes.navigate(['/verPromo'])
    } else {
      this.complete = !this.complete;
    }

  }
  volver() {
    this.routes.navigate(['/verPromo']);
  }
  cerrar() {
    this.routes.navigate(['/verPromo']);
  }

}
