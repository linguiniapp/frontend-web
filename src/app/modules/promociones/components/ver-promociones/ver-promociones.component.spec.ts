import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerPromocionesComponent } from './ver-promociones.component';

describe('VerPromocionesComponent', () => {
  let component: VerPromocionesComponent;
  let fixture: ComponentFixture<VerPromocionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerPromocionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerPromocionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
