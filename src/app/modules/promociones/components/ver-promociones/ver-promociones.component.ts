import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PromocionesService, ListaElement } from '../../services/promociones.service';


@Component({
  selector: 'app-ver-promociones',
  templateUrl: './ver-promociones.component.html',
  styleUrls: ['./ver-promociones.component.css']
})
export class VerPromocionesComponent implements OnInit {

  dataSource: ListaElement[]=[];
  selectedPromo: boolean;

  constructor(private ar:ActivatedRoute, private routes: Router, private promo: PromocionesService) { }

  ngOnInit() {
    this.ar.params.subscribe(params => this.promo.getPromo().subscribe(data =>this.dataSource=data));

    //this.periodicidad.getPeriodicidad().subscribe( data => this.dataSourcePer = data);
  }

  agregarPromo(){
    this.routes.navigate(['/crearPromo']);
  }
  quitarPromo(codigo: number){
    this.promo.quitarPromo(codigo);
    this.routes.navigate(['/eliminarPromo']);
  }
  editarPromo(){
    this.routes.navigate(['/editarPromo']);
  }

}
