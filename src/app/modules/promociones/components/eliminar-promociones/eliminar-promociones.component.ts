import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-eliminar-promociones',
  templateUrl: './eliminar-promociones.component.html',
  styleUrls: ['./eliminar-promociones.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class EliminarPromocionesComponent {
  
  complete : boolean = false;

  
  constructor(private routes: Router) { }
  volver(){
    this.routes.navigate(['/verPromo']);
  }
  volverMenu(){
    this.routes.navigate(['/']);
  }

  cerrar(){
    this.routes.navigate(['/verPromo']);
  }

}
