import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarPromocionesComponent } from './eliminar-promociones.component';

describe('EliminarPromocionesComponent', () => {
  let component: EliminarPromocionesComponent;
  let fixture: ComponentFixture<EliminarPromocionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarPromocionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarPromocionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
