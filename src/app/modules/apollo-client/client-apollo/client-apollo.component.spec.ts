import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientApolloComponent } from './client-apollo.component';

describe('ClientApolloComponent', () => {
  let component: ClientApolloComponent;
  let fixture: ComponentFixture<ClientApolloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientApolloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientApolloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
