import {Component, OnInit} from '@angular/core';
import {Apollo, gql} from 'apollo-angular-boost';

@Component({
  selector: 'exchange-rates',
  template: `
    <div *ngIf="loading">
      Loading...
    </div>
    <div *ngIf="error">
      Error :(
    </div>
    <div *ngIf="rates">
      <div *ngFor="let rate of rates">
      <p>{{rate.currency}}: {{rate.rate}}</p>
      </div>
    </div>
  `,
})

// <p>{{rate.currency}}: {{rate.rate}}</p>
export class ClientApolloComponent implements OnInit {
  rates: any;
  loading: boolean;
  error: any;

  constructor(private apollo: Apollo) {}

  ngOnInit() {
    this.apollo
      .mutate({
        mutation: gql
        `mutation {
          crearUsuarioCliente(username: "usuario1", password: "passuser1", datosCliente: {
            nombre: "NombreCliente1",
            apellido:"ApellidoCliente1",
            sexo: "Hombre",
            email: "cliente1@linguiniapp.com",
            telefono: "(0000) 0000000",
            fechaNacimiento: "1990-02-07",
            dni: "00000000",
            direccion: "Calle falsa 123, Ciudad, Mendoza",
          }) {
            ok
            email
          }
        }`
        ,
      })
      .subscribe();
  }
}