import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientApolloComponent } from './client-apollo/client-apollo.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ClientApolloComponent]
})
export class ApolloClientModule { }
