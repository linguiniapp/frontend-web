import { NgForm, Validators, FormControl, FormGroup } from '@angular/forms';
import { Component, ViewChild, NgZone, OnInit, ElementRef } from '@angular/core';
import { ListaElement } from '../../services/sucursal.service';
import { SucursalService } from '../../services/sucursal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { ChangeDetectorRef } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { formatDate } from '@angular/common';



@Component({
  selector: 'app-editar-sucursal',
  templateUrl: './editar-sucursal.component.html',
  styleUrls: ['./editar-sucursal.component.css'],

})
export class EditarSucursalComponent implements OnInit {


  errors: string[] = [];
  public file_srcs: string[] = [];
  public debug_size_before: string[] = [];
  public file_srcs1: string[] = [];

  public debug_size_after: string[] = [];
  sucursal: ListaElement = {
    numero: null,
    nombre: '',
    latitud: null,
    longitud: null,
    telefono: '',
    direccion: '',
    foto: '',
    horaDesde: '',
    horaHasta: ''
  }
  // foto: string = '';
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  @ViewChild("search")
  public searchElementRef: ElementRef;


  complete: boolean = false;
  nombre: string;
  direccion: string;
  imagen: string;
  telefono: string;
  horaDesde: string;
  horaHasta: string;


  altaSucursal = new FormGroup({
    nombre: new FormControl('', [
      Validators.required,
    ]),
    // numero: new FormControl('', [
    //   Validators.required,
    // ]),

    telefono: new FormControl('', [
      Validators.required,
    ]),

    foto: new FormControl('', [
      // Validators.required,
    ]),
    searchControl: new FormControl('', [
      // Validators.required,
    ]),
  }
  );

  constructor(private routes: Router, private changeDetectorRef: ChangeDetectorRef, private sanitizer: DomSanitizer, private sucursalServicio: SucursalService, private ar: ActivatedRoute, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {

    // this.foto = '';
    this.ar.params.subscribe(params => {
      this.sucursalServicio.getLista().subscribe(s => {
      this.sucursal = s.find(f => f.numero == params['id']);

        // this.sucursal.foto = this.getBackground(this.sucursal)
      });

    });


    //create search FormControl
  }
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.sucursal.latitud = place.geometry.location.lat();
          this.sucursal.longitud = place.geometry.location.lng();

          // this.zoom = 12;
        });
      });
    });

  }


  fileChange(input) {
    this.readFiles(input.files);
  }


  onSubmit(f: NgForm) {


    this.complete = false;
    if (f.valid && this.altaSucursal.valid) {


      // this.sucursalServicio.guardarImagen(this.imagen).then( a => 
      //   {
      // alert(a);
      let horaD = formatDate("2018-12-12" + " " + this.sucursal.horaDesde, 'HHmmss', 'en-US').toString();
      let horaH = formatDate("2018-12-12" + " " + this.sucursal.horaHasta, 'HHmmss', 'en-US').toString();

      this.sucursalServicio.modificarSucursal(this.sucursal.nombre, this.sucursal.direccion, this.sucursal.latitud, this.sucursal.longitud, this.sucursal.telefono, this.sucursal.numero, this.sucursal.foto, horaD, horaH).subscribe(ok => {
        if (ok) {
          this.routes.navigate(["/verSucursal"]);
        } else {
          this.routes.navigate(["/verSucursal"]);

          console.log("error");
        }

      });

      this.routes.navigate(["/verSucursal"]);

      // });      // this.foto = f.value.foto;

      // this.routes.navigate(['/verSucursal'])
    }
    else {
      this.complete = true;
      this.sucursalServicio.validateAllFormFields(this.altaSucursal);

    }

  }


  volver() {
    this.routes.navigate(['/verSucursal'])
  }


  getImage(image) {
    return this.sanitizer.bypassSecurityTrustUrl(image);//_sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
  }

  /* 
  
  Metodos para acomodar la imagen
  
  */




  resize(img, MAX_WIDTH: number, MAX_HEIGHT: number, callback) {
    // This will wait until the img is loaded before calling this function
    return img.onload = () => {

      // Get the images current width and height
      // tslint:disable-next-line:no-var-keyword
      var width = img.width;
      // tslint:disable-next-line:no-var-keyword
      var height = img.height;
      // Set the WxH to fit the Max values (but maintain proportions)
      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      // create a canvas object
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line:no-var-keyword
      // tslint:disable-next-line:prefer-const
      let canvas = document.createElement("canvas");
      // Set the canvas to the new calculated dimensions
      canvas.width = width;
      canvas.height = height;
      const ctx = canvas.getContext("2d");

      ctx.drawImage(img, 0, 0, width, height);

      // Get this encoded as a jpeg
      // IMPORTANT: 'jpeg' NOT 'jpg'
      // tslint:disable-next-line:prefer-const
      let dataUrl = canvas.toDataURL("image/jpeg");

      // callback with the results
      callback(dataUrl, img.src.length, dataUrl.length);
    };
  }


  readFile(file, reader, callback) {
    reader.onload = () => {
      callback(reader.result);
      this.sucursal.foto = reader.result;
      // alert(this.sucursal.foto);
      // alert("fotooooo: " +this.imagen);

      console.log(reader.result);
    };
    reader.readAsDataURL(file);
  }




  readFiles(files, index = 0) {
    // Create the file reader
    const reader = new FileReader();

    // If there is a file
    if (index in files) {
      // Start reading this file
      this.readFile(files[index], reader, (result) => {
        // Create an img element and add the image file data to it
        const img = document.createElement("img");
        img.src = result;

        // Send this img to the resize function (and wait for callback)
        this.resize(img, 50, 50, (resized_jpeg, before, after) => {
          // For debugging (size in bytes before and after)
          this.debug_size_before.push(before);
          this.debug_size_after.push(after);

          // Add the resized jpeg img source to a list for preview
          // This is also the file you want to upload. (either as a
          // base64 string or img.src = resized_jpeg if you prefer a file).
          this.file_srcs1.push(resized_jpeg);

          // Read the next file;
          //  this.readFiles(files, index+1);
        });
      });
    } else {
      // When all files are done This forces a change detection
      this.changeDetectorRef.detectChanges();
    }
  }




}
