import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SucursalService, ListaElement } from '../../services/sucursal.service';


@Component({
  selector: 'app-eliminar-sucursal',
  templateUrl: './eliminar-sucursal.component.html',
  styleUrls: ['./eliminar-sucursal.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class EliminarSucursalComponent {
  sucursal: ListaElement = {
    numero: null,
    nombre: '',
    latitud: null,
    longitud: null,
    telefono: '',
    direccion: '',
    foto:'',
    horaDesde: "",
    horaHasta: ""
  }
  acepta: boolean = false;
  constructor(private router: Router, private service: SucursalService, private ar: ActivatedRoute) {
    this.ar.params.subscribe(params => {
      this.service.getLista().subscribe(s => this.sucursal = s.find(f => f.numero == params['id']));

    });

  }

  volver() {
    this.router.navigate(['/verSucursal']);
  }
  volverMenu() {
    this.router.navigate(['/']);
  }


  cerrar() {
    this.router.navigate(['/verSucursal']);
  }


  eliminar() {
    this.service.quitarSucursal(this.sucursal.numero);
    this.acepta = true;
    // this.router.navigate(['/verProductos']);
  }


}
