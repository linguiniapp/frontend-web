import { Component, OnInit } from '@angular/core';
import { SucursalService, Mesa } from '../../services/sucursal.service';


@Component({
  selector: 'app-ver-mesas-reservables',
  templateUrl: './ver-mesas-reservables.component.html',
  styleUrls: ['./ver-mesas-reservables.component.css']
})
export class VerMesasReservablesComponent implements OnInit {


  items: Mesa[] = [];
  urlMesa: string = "../../../../../assets/silla.png";
  urlMesaRed: string = "../../../../../assets/sillaRed.png";
  urlMesaGreen: string = "../../../../../assets/sillaGreen.png";


  constructor(private service: SucursalService) { }

  ngOnInit() {
    // this.items = this.service.getMesas();

  }

  reservar(mesa: Mesa) {
    if (mesa.stat != 1) {
      if (mesa.stat == 0) {
        // alert("marcada");
        mesa.stat = 2;
      } else {
        // alert("desmarcada");
        mesa.stat = 0;
      }
    }
    else {
      // alert("Mesa Reserveda");
      return;
    }



  }

  getAlt(stat): string {
    return stat == 0 ? "Reservar" : stat == 1 ? "Reservada" : "Cencelar";
  }
}
