import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerMesasReservablesComponent } from './ver-mesas-reservables.component';

describe('VerMesasReservablesComponent', () => {
  let component: VerMesasReservablesComponent;
  let fixture: ComponentFixture<VerMesasReservablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerMesasReservablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerMesasReservablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
