import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargarSucursalComponent } from './cargar-sucursal.component';

describe('CargarSucursalComponent', () => {
  let component: CargarSucursalComponent;
  let fixture: ComponentFixture<CargarSucursalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargarSucursalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargarSucursalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
