import { Component, ViewChild, NgZone, OnInit, ElementRef } from '@angular/core';
import { SucursalService } from '../../services/sucursal.service';
import { NgForm, Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { ChangeDetectorRef } from '@angular/core';

// import { } from '..googlemaps';
import { MapsAPILoader, AgmMap } from '@agm/core';
declare var google: any;


@Component({
  selector: 'app-cargar-sucursal',
  templateUrl: './cargar-sucursal.component.html',
  styleUrls: ['./cargar-sucursal.component.css'],
  exportAs: 'ngModel'
})
export class CargarSucursalComponent implements OnInit {



  //para las imagenes
  errors: string[] = [];
  public file_srcs: string[] = [];
  public debug_size_before: string[] = [];
  public file_srcs1: string[] = [];
  public debug_size_after: string[] = [];




  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  @ViewChild("search")
  public searchElementRef: ElementRef;


  complete: boolean = false;
  nombre: string;
  direccion: string;
  direcciongoogle: string = undefined;
  telefono: string;
  foto: string;

  altaSucursal = new FormGroup({
    nombre: new FormControl('', [
      Validators.required,
    ]),
    telefono: new FormControl('', [
      Validators.required,
    ]),

    imagen: new FormControl('',  [
      Validators.required,
    ]),
    searchControl: new FormControl('', [
      Validators.required,
    ]),
  }
  );


  //sucursal = new ListaElement();

  constructor(private routes: Router, private sucursalServicio: SucursalService, private changeDetectorRef: ChangeDetectorRef, private sanitizer: DomSanitizer, private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) {
    this.foto = '';
  }
  ngOnInit() {
    //set google maps defaults

    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.direcciongoogle = place.formatted_address;
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            this.direcciongoogle = undefined;
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();

          this.zoom = 12;
        });
      });
    });
  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;

        this.zoom = 12;
      });
    }
  }
  onSubmit(f: NgForm) {
    // alert(this.direccion);
    this.direccion = this.direcciongoogle != undefined ? this.direcciongoogle : this.direccion;
    // alert("dsadas")
    this.complete = false;
    if (f.valid && this.altaSucursal.valid) {
      this.nombre = f.value.nombre;
      // this.direccion = f.value.searchControl;
      this.telefono = f.value.telefono;

      this.sucursalServicio.agregarSucursal(this.nombre, this.telefono, this.direccion, this.longitude, this.latitude, this.foto);
      this.routes.navigate(['/verSucursal']);
    }
    else {
      this.complete = true;
      this.sucursalServicio.validateAllFormFields(this.altaSucursal);

    }

  }
  volver() {
    this.routes.navigate(['/verSucursal'])
  }

  cerrar() {
    this.complete = !this.complete;
  }


  getBackground(image) {
    return this.sanitizer.bypassSecurityTrustUrl(image);//_sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
  }

  /* 
  
  Metodos para acomodar la imagen
  
  */



 fileChange(input) {
  this.readFiles(input.files);
}


  resize(img, MAX_WIDTH: number, MAX_HEIGHT: number, callback) {
    // This will wait until the img is loaded before calling this function
    return img.onload = () => {

      // Get the images current width and height
      // tslint:disable-next-line:no-var-keyword
      var width = img.width;
      // tslint:disable-next-line:no-var-keyword
      var height = img.height;
      // Set the WxH to fit the Max values (but maintain proportions)
      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      // create a canvas object
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line:no-var-keyword
      // tslint:disable-next-line:prefer-const
      let canvas = document.createElement("canvas");
      // Set the canvas to the new calculated dimensions
      canvas.width = width;
      canvas.height = height;
      const ctx = canvas.getContext("2d");

      ctx.drawImage(img, 0, 0, width, height);

      // Get this encoded as a jpeg
      // IMPORTANT: 'jpeg' NOT 'jpg'
      // tslint:disable-next-line:prefer-const
      let dataUrl = canvas.toDataURL("image/jpeg");

      // callback with the results
      callback(dataUrl, img.src.length, dataUrl.length);
    };
  }



  readFile(file, reader, callback) {
    reader.onload = () => {
      callback(reader.result);

      //ACA ES DONDE SE GUARDA LA IMAGEN
      this.foto= reader.result;
      // alert("fotooooo: " +this.imagen);
  
      console.log(reader.result);
    };
    reader.readAsDataURL(file);
  }



  readFiles(files, index = 0) {
    // Create the file reader
    const reader = new FileReader();

    // If there is a file
    if (index in files) {
      // Start reading this file
      this.readFile(files[index], reader, (result) => {
        // Create an img element and add the image file data to it
        const img = document.createElement("img");
        img.src = result;

        // Send this img to the resize function (and wait for callback)
        this.resize(img, 50, 50, (resized_jpeg, before, after) => {
          // For debugging (size in bytes before and after)
          this.debug_size_before.push(before);
          this.debug_size_after.push(after);

          // Add the resized jpeg img source to a list for preview
          // This is also the file you want to upload. (either as a
          // base64 string or img.src = resized_jpeg if you prefer a file).
          this.file_srcs1.push(resized_jpeg);

          // Read the next file;
          //  this.readFiles(files, index+1);
        });
      });
    } else {
      // When all files are done This forces a change detection
      this.changeDetectorRef.detectChanges();
    }
  }





}

