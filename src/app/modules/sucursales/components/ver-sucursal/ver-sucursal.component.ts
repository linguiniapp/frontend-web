import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SucursalService, ListaElement } from '../../services/sucursal.service';
import { Observable } from 'rxjs'


@Component({
  selector: 'app-ver-sucursal',
  templateUrl: './ver-sucursal.component.html',
  styleUrls: ['./ver-sucursal.component.css']
})
export class VerSucursalComponent implements OnInit {


  // displayedColumns: string[] = ['numero', 'nombre', 'direccion', 'actions','agregar'];
  sucursal$ = new Observable<ListaElement[]>();

  // dataSource: ListaElement[]=[];
  constructor(private routes: Router, private service: SucursalService) { }

  ngOnInit() {
    console.log("ngonit versucursal");

    this.sucursal$ = this.service.getListaBasic();

    // this.service.getLista().subscribe(data => this.dataSource=data);
  }
  agregarSucursal() {
    this.routes.navigate(['/cargarSucursal']);
  }
  eliminarSucursal(idx: number) {
    this.routes.navigate(['/eliminarSucursal', idx]);
  }
  onEdit(idx: number) {
    this.routes.navigate(['/editarSucursal', idx]);
  }

  onCargarMesas(idx: number){
    this.routes.navigate(['/cargarMesaSector', idx]);
  }
  onAsignarCarta(idx: number){
    this.routes.navigate(['/crearCarta', idx]);
  }
  onVerCarta(idx: number){
    this.routes.navigate(['/verCarta', idx]);
  }
}

