import { Component, OnInit } from '@angular/core';
import { SucursalService, Mesa } from '../../services/sucursal.service';
import { Router, ActivatedRoute } from '@angular/router';

import { Items } from '@clr/angular/data/datagrid/providers/items';

@Component({
  selector: 'app-sector',
  templateUrl: './sector.component.html',
  styleUrls: ['./sector.component.css']
})
export class SectorComponent implements OnInit {

  items: Mesa[] = [];
  numeroMesa: number = 1;
  suscripcion: any;
  cantidadPersonas: number = 4;
  numeroSucursal: number;
  mesa: Mesa;
  urlMesa: string = "../../../../../assets/silla.png";
  urlMesaRed: string = "../../../../../assets/sillaRed.png";
  urlMesaGreen: string = "../../../../../assets/sillaGreen.png";
  constructor(private service: SucursalService, private ar: ActivatedRoute, private route: Router) {

    this.ar.params.subscribe(params => {
      this.numeroSucursal = params['id'];
    });
  }
 
  ngOnInit() {
    // alert(this.service.verSucursalMesa(this.numeroSucursal));
   this.suscripcion=this.service.verSucursalMesa(this.numeroSucursal).subscribe( d => this.items = d);
  }
  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();
  }


  volver(){
    this.route.navigate(["/verSucursal"]);
  }

  addOnClick(event) {


    let x = event.offsetX;
    let y = event.offsetY;
    let stat = 0;
    console.log("position");

    console.log(y);
    console.log(x);

    console.log("mod");

    console.log((y % 80));
    console.log((x % 90));
    x = -(x % 90) + x;
    y = -(y % 80) + y;

    if (x < 90) {
      x = 10;
      // stat = 1;
    }

    if (y < 80) {
      y = 12;
      // stat = 2;


    }

    console.log("despues");
    console.log(y);
    console.log(x);
    if (this.items.find(item => item.id == this.numeroMesa)) {
      alert("Ya existe una mesa con ese número");
      return;
    }
    // alert(this.cantidadPersonas);

    this.items.push({ x: x, y: y, stat: stat, id: this.numeroMesa, cantidad: this.cantidadPersonas, tieneReserva: null, noTienePedido: null });
    this.numeroMesa = this.numeroMesa + 1;

  }

  deshacer() {
    // this.items.pop();
    this.numeroMesa = 1;
    this.items = [];
  }

  guardar() {
    this.service.guardarMesas(this.items, this.numeroSucursal).subscribe(ok => {
      if (ok) {

        this.route.navigate(['/verSucursal']);
      } else {
        
      }
    });
  }

  addOnClickMesa(mesa: Mesa) {
    // this.items.s
    if (this.items.find(data => data.id == mesa.id)) {
      let index = this.items.findIndex(data => data.id == mesa.id);
      this.items.splice(index, 1);
      return;
    }
  }
}

