import { Component, ViewChild, NgZone, OnInit, ElementRef } from '@angular/core';
import { SucursalService, ListaElement } from "../../services/sucursal.service";
import { MapsAPILoader, AgmMap } from '@agm/core';
import { Point } from '@agm/core/services/google-maps-types';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';


declare var google: any;

@Component({
  selector: 'app-ver-sucursales',
  templateUrl: './ver-sucursales.component.html',
  styleUrls: ['./ver-sucursales.component.css']
})
export class VerSucursalesComponent implements OnInit {

  public latitude: number;
  public longitude: number;
  dataSource: ListaElement[] = [];
  public zoom: number = 18;
  public title: string = "Aquí estas";
   @ViewChild("search")
  public searchElementRef: ElementRef;

  constructor(private service: SucursalService, private routes: Router, private mapsAPILoader: MapsAPILoader, private sanitizer: DomSanitizer, private ngZone: NgZone) { }

  ngOnInit() {
    this.setCurrentPosition();

    this.service.getLista().subscribe(data => this.dataSource = data);

    //load Places Autocomplete


  }


  getImage(image) {
    return this.sanitizer.bypassSecurityTrustUrl(image);//_sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
  }


  reservar(idx) {
    this.routes.navigate(['/reservas', idx]);

  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }
  onVerCarta(idx: number){
    this.routes.navigate(['/verCarta', idx]);
  }
}

export interface Points {

  latitude: number, longitude: number, direccion: string, nombreSucursal: string, telefono: string

}