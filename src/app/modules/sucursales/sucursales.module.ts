import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerSucursalComponent } from './components/ver-sucursal/ver-sucursal.component';
import { CargarSucursalComponent } from '../sucursales/components/cargar-sucursal/cargar-sucursal.component';
import { EliminarSucursalComponent } from './components/eliminar-sucursal/eliminar-sucursal.component';
import { EditarSucursalComponent } from './components/editar-sucursal/editar-sucursal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule, ClrFormsNextModule } from '@clr/angular';
import { HttpModule } from '@angular/http';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';





import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';

import { SucursalesRoutingModule } from './sucursales-routing.module';
import { VerSucursalesComponent } from './components/ver-sucursales/ver-sucursales.component';
import { SectorComponent } from './components/sector/sector.component';
import { VerMesasReservablesComponent } from './components/ver-mesas-reservables/ver-mesas-reservables.component';



@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyDypB_lExsC4NArfG66OKiijD10U3fEQCY', libraries: ["places"]}),
    ReactiveFormsModule,
    ClarityModule,
    ClrFormsNextModule,
    SucursalesRoutingModule,
    NgxMaterialTimepickerModule,
    FormsModule,
    HttpModule

    
  ],
  declarations: [VerSucursalComponent, CargarSucursalComponent, EliminarSucursalComponent, EditarSucursalComponent, VerSucursalesComponent, SectorComponent, VerMesasReservablesComponent],
  providers: [
    GoogleMapsAPIWrapper,
     // <---
  ],
  
})
export class SucursalesModule { }
