import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { VerSucursalComponent } from './components/ver-sucursal/ver-sucursal.component';
import { VerSucursalesComponent } from './components/ver-sucursales/ver-sucursales.component';
import { CargarSucursalComponent } from './components/cargar-sucursal/cargar-sucursal.component';
import { EliminarSucursalComponent } from './components/eliminar-sucursal/eliminar-sucursal.component';
import { EditarSucursalComponent } from './components/editar-sucursal/editar-sucursal.component';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { SectorComponent } from './components/sector/sector.component';
import { VerMesasReservablesComponent } from './components/ver-mesas-reservables/ver-mesas-reservables.component';
import { AuthService } from './/../../security/login/auth.service';




const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            
            {
                path: 'cargarSucursal',
                component: CargarSucursalComponent, canActivate: [AuthService],
                data: {acciones: ["crear_sucursal"]}
            },
            {
                path: 'verSucursal',
                component: VerSucursalComponent
            },
            {
                path: 'verSucursales',
                component: VerSucursalesComponent, canActivate: [AuthService],
                data: {acciones: ["ver_sucursal"]}
            },
            {
                path: 'eliminarSucursal/:id',
                component: EliminarSucursalComponent
            },
            {
                path: 'editarSucursal/:id',
                component: EditarSucursalComponent
            },
            {
                path: 'cargarMesaSector/:id',
                component: SectorComponent
            },
            {
                path: 'verMesasSector',
                component: VerMesasReservablesComponent
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SucursalesRoutingModule { }
