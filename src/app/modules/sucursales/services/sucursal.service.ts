import {Injectable} from '@angular/core';
import {Apollo, gql,} from 'apollo-angular-boost';
import {Observable, Subject} from 'rxjs';
import {AuthService} from '../../../security/login/auth.service';

import {ImagenesService} from '../../imagenes/services/imagenes.service';
import {FormControl, FormGroup} from '@angular/forms';


@Injectable()
export class SucursalService {

    sucursal: verSucursal[] = [];
    sucursales: ListaElement[] = [];
    private ELEMENT_DATA = new Subject<ListaElement[]>();
    private respuesta = new Subject<boolean>();
    mesas: Mesa[] = [];
    private nombreSubject = new Subject<string>();

    private existe = new Subject<boolean>();


    private resultado = new Subject<boolean>();


    private ListaElement = new Subject<Mesa[]>();

    token: string = localStorage.getItem('token');


    constructor(private apollo: Apollo, private imagenesService: ImagenesService, private authService: AuthService
    ) {

        console.log('Ready to use');

    }


    getLista(): Observable<ListaElement[]> {
        this.apollo.watchQuery<any>({
            query: gql
                `{
          verSucursales(first:10){
            edges{
              node{
                numero
                direccion
                nombre
                latitud
                longitud
                telefono
                foto
                horaInicioReserva
                horaFinReserva
              }
            }
          }
        }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                this.sucursales = [];
                let ELEMENT_DATA$: ListaElement[] = [];
                data.verSucursales.edges.forEach(s => {
                    let lelem: ListaElement = {
                        latitud: s.node.latitud,
                        longitud: s.node.longitud,
                        numero: s.node.numero,
                        nombre: s.node.nombre,
                        direccion: s.node.direccion,
                        telefono: s.node.telefono,
                        foto: s.node.foto,
                        horaDesde: s.node.horaInicioReserva,
                        horaHasta: s.node.horaFinReserva
                    };
                    ELEMENT_DATA$.push(lelem);
                });
                this.ELEMENT_DATA.next(ELEMENT_DATA$);
            });
        return this.ELEMENT_DATA.asObservable();
    }

    getListaBasic(): Observable<ListaElement[]> {
        this.apollo.watchQuery<any>({
            query: gql
                `{
          verSucursales(first:10){
            edges{
              node{
                numero
                direccion
                nombre
                latitud
                longitud
                telefono
                horaInicioReserva
                horaFinReserva

              }
            }
          }
        }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                let ELEMENT_DATA$: ListaElement[] = [];
                data.verSucursales.edges.forEach(s => {
                    let lelem: ListaElement = {
                        latitud: s.node.latitud,
                        longitud: s.node.longitud,
                        numero: s.node.numero,
                        nombre: s.node.nombre,
                        direccion: s.node.direccion,
                        telefono: s.node.telefono,
                        foto: '',
                        horaDesde: s.node.horaInicioReserva,
                        horaHasta: s.node.horaFinReserva
                    };
                    ELEMENT_DATA$.push(lelem);
                });
                this.ELEMENT_DATA.next(ELEMENT_DATA$);
            });
        return this.ELEMENT_DATA.asObservable();
    }


    agregarSucursal(nombre, telefono, direccion, longitud, latitud, foto) {
        this.apollo
            .mutate({
                mutation: gql
                    `
          mutation {
            crearSucursal(direccion: "${direccion}", telefono: "${telefono}", nombre: "${nombre}", latitud: ${latitud}, longitud: ${longitud}, foto: "${foto}") {
              ok
              sucursal{
                numero
                nombre
              }
            }
          }
          
          `
                ,
            })
            .subscribe(({data}) => {
                if (data.crearSucursal.ok = 'true') {

                    let lelem: ListaElement = {
                        latitud: latitud,
                        longitud: longitud,
                        numero: data.crearSucursal.sucursal.numero,
                        nombre: nombre,
                        direccion: direccion,
                        telefono: telefono,
                        foto: foto,
                        horaDesde: '',
                        horaHasta: ''
                    };

                    this.sucursales = [...this.sucursales, lelem];
                    this.refresh();
                    // alert("Nueva sucursal creada con numero: " + data.numero);
                } else {
                    alert('error');
                }
            }, (error) => {
                console.log('error', error);
            });

        //this.ELEMENT_DATA.push(elemento);
    };


    modificarSucursal(nombre, direccion, longitud, latitud, telefono, numero, foto, horaDesde, horaHasta): Observable<boolean> {
        // alert(foto);

        this.apollo
            .mutate({
                mutation: gql
                    `
          mutation {
            modificarSucursal(direccion: "${direccion}", nombre: "${nombre}",  telefono: "${telefono}", numero: ${numero}, foto: "${foto}",horaInicioReserva: "${horaDesde}", horaFinReserva: "${horaHasta}" ) {
              ok
              
            }
          }
          
          `
                ,
            })
            .subscribe(({data}) => {
                    if (data.modificarSucursal.ok = 'true') {

                        this.sucursales.find(f => f.numero == numero).direccion = direccion;
                        this.sucursales.find(f => f.numero == numero).nombre = nombre;
                        this.sucursales.find(f => f.numero == numero).telefono = telefono;
                        this.respuesta.next(true);
                        this.refresh();

                    } else {
                        this.respuesta.next(false);
                    }
                }, (error) => {
                    this.respuesta.next(false);

                    console.log('error', error);
                }
            );

        return this.respuesta.asObservable();


        //this.ELEMENT_DATA.push(elemento);
    };

    quitarSucursal(x) {
        return this.ELEMENT_DATA;
    }


    guardarMesas(mesas: Mesa[], numeroSucursal: number): Observable<boolean> {
        mesas.forEach(item => {
            this.apollo
                .mutate({
                    mutation: gql
                        `
    mutation {
      crearMesa(capacidad: ${item.cantidad}, numeroMesa: ${item.id},  numeroSucursal: ${numeroSucursal}, posicionX: ${item.x}, posicionY: ${item.y},) {
        ok        
      }
    }`,
                })
                .subscribe(({data}) => {
                    if (data.ok = 'true') {
                        this.resultado.next(true);
                    } else {
                        this.resultado.next(true);
                        alert('error');
                    }
                }, (error) => {
                    console.log('error', error);
                });
        });
        return this.resultado.asObservable();
    }


    verSucursalNombre(id: number): Observable<string> {

        let nombre: string = '';
        this.apollo.watchQuery<any>({
            query: gql
                `{
          verSucursal(numeroSucursal: ${id}) {
            numero
            nombre          
          }
      }`, fetchPolicy: 'no-cache'
        }).valueChanges
            .subscribe(({data}) => {
                nombre = data.verSucursal.nombre;
                this.nombreSubject.next(nombre);
                // let ELEMENT_DATA$: ListaElement[] = [];
            });
        return this.nombreSubject.asObservable();
    }


    verSucursalMesa(id: number): Observable<Mesa[]> {

        this.apollo.watchQuery<any>({
            query: gql
                `{
          verSucursal(numeroSucursal: ${id}) {
            numero
            mesas {
              edges {
                node {
                  numero
                  pedidoPago
                  reservaExistente
                  capacidadPersonas
                  posicionX
                  posicionY
                  estado {
                    nombre
                    codigo
                  }
                }
              }
            }
          }
      }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                this.mesas = [];
                data.verSucursal.mesas.edges.forEach(s => {
                    let lelem: Mesa = {
                        id: s.node.numero,
                        cantidad: s.node.capacidadPersonas,
                        x: s.node.posicionX,
                        y: s.node.posicionY,
                        stat: s.node.estado.codigo,
                        tieneReserva: s.node.reservaExistente,
                        noTienePedido: s.node.pedidoPago
                    };
                    this.mesas.push(lelem);
                    this.ListaElement.next(this.mesas);
                });
            });

        return this.ListaElement.asObservable();


    }


    guardarImagen(imagen: String): Promise<any> {
        return this.imagenesService.saveImage(imagen);
    }


    // UTILS
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({onlySelf: true});
                control.updateValueAndValidity();
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }


    getMesas(fecha, sucursal): Observable<Mesa[]> {
        this.apollo.watchQuery<any>({
            query: gql
                `    {
          verSucursal(numeroSucursal:${sucursal}) {
            numero
            mesas {
              edges {
                node {
                  numero
                  pedidoPago
                  capacidadPersonas
                  posicionX
                  posicionY
                  reservaExistente(fechaHoraReserva: "${fecha}")
                  estado {
                    nombre
                    codigo
                  }
                }
              }
            }
          }
      }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                let ELEMENT_DATA$: Mesa[] = [];
                data.verSucursal.mesas.edges.forEach(s => {
                    let lelem: Mesa = {
                        id: s.node.numero,
                        cantidad: s.node.capacidadPersonas,
                        x: s.node.posicionX,
                        y: s.node.posicionY,
                        stat: s.node.estado.codigo,
                        tieneReserva: s.node.reservaExistente,
                        noTienePedido: s.node.pedidoPago
                    };
                    ELEMENT_DATA$.push(lelem);
                });
                this.ListaElement.next(ELEMENT_DATA$);
            });
        return this.ListaElement.asObservable();
    }

    getMesasActual(sucursal): Observable<Mesa[]> {
        this.apollo.watchQuery<any>({
            query: gql
                `{
          verSucursal(numeroSucursal: ${sucursal}) {
            mesas {
              edges {
                node {
                  numero
                  pedidoPago
                  reservaExistente
                  capacidadPersonas
                  posicionX
                  posicionY
                  estado {
                    nombre
                    codigo
                  }
                }
              }
            }
          }
        }
        `, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                let ELEMENT_DATA$: Mesa[] = [];
                data.verSucursal.mesas.edges.forEach(s => {
                    let lelem: Mesa = {
                        id: s.node.numero,
                        cantidad: s.node.capacidadPersonas,
                        x: s.node.posicionX,
                        y: s.node.posicionY,
                        stat: s.node.estado.codigo,
                        tieneReserva: s.node.reservaExistente,
                        noTienePedido: s.node.pedidoPago

                    };
                    ELEMENT_DATA$.push(lelem);
                });
                this.ListaElement.next(ELEMENT_DATA$);
            });
        return this.ListaElement.asObservable();
    }


    private refresh() {
        this.ELEMENT_DATA.next(this.sucursales);
    }


    private refreshMesa() {
        this.ListaElement.next(this.mesas);
    }

    tieneEstadia(numeroReserva): Observable<boolean>  {

        this.apollo.watchQuery<any>({
            query: gql
                `    {
       verSucursales{
       edges{
         node{
           estadias{
             edges{
               node{
                 reserva{
                  numero
                }
               }
             }
           }
         }
       }
     }
     }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                    data.verSucursales.edges.forEach(sucursal => {
                        if (sucursal.node.estadias) {
                            sucursal.node.estadias.edges.forEach(estadia => {
                                if (estadia.node.reserva) {
                                    if (estadia.node.reserva.numero == numeroReserva) {
                                        this.existe.next(true);
                                    }
                                }
                            });
                        }
                    });
                }
            );
        return this.existe;
    }
}

export interface ListaElement {
    numero: number;
    nombre: string;
    latitud: number;
    longitud: number;
    direccion: string;
    telefono: string;
    foto: string;
    horaDesde: string;
    horaHasta: string;
}

export interface verSucursal {
    numero: number;
    nombre: string;
    direccion: string;
}

export interface Mesa {
    x: number;
    y: number;
    stat: number;
    id: number;
    cantidad: number;
    tieneReserva: boolean;
    noTienePedido: boolean;
}
