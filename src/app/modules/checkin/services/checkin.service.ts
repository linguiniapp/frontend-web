import {Injectable} from '@angular/core';
import {Apollo, gql} from 'apollo-angular-boost';
import {Observable, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CheckinService {


    private resp = new Subject<boolean>();

    constructor(private apollo: Apollo) {
    }

    IngresarClienteReserva(numeroReserva): Observable<boolean> {
        this.apollo
            .mutate({
                mutation: gql
                    `mutation{
          checkIn(numeroReserva:${numeroReserva} ){
            ok
            estadia{
              numero
            }
          }
        }`
                ,
            })
            .subscribe(({data}) => {
                if (data.checkIn.ok === 0) {
                    let check: checkIn = {
                        numero: data.checkIn.estadia.numero
                    };
                    localStorage.setItem('estadia', check.numero.toString());
                    this.resp.next(true);
                } else {
                    this.resp.next(false);
                }
            }, (error) => {
                this.resp.next(false);

            });

        return this.resp.asObservable();
    }

    IngresarCliente(numeroMesa, numeroSucursal): Observable<boolean> {
        this.apollo
            .mutate({
                mutation: gql
                    `mutation{
          checkIn(numerosMesas:[${numeroMesa}] , numeroSucursal: ${numeroSucursal}){
            ok
            estadia{
              numero
            }
          }
        }`
                ,
            })
            .subscribe(({data}) => {
                if (data.checkIn.ok == 0) {
                    let check: checkIn = {
                        numero: data.checkIn.estadia.numero
                    };
                    localStorage.setItem('estadia', check.numero.toString());
                    this.resp.next(true);
                } else {
                    this.resp.next(false);
                }
            }, (error) => {
                this.resp.next(false);

            });

        return this.resp.asObservable();
    }
}

export interface checkIn {
    numero: number;
}
