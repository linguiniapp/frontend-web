import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { CheckInComponent } from './components/check-in/check-in.component';


const routes: Routes = [
  {
      path: '',
      component: AuxviewComponent,
      children: [
          {
              path: 'check-in',
              component: CheckInComponent,
              data: {
                breadcrumb: 'Check-in'
            }
          },
      ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckinRoutingModule { }
