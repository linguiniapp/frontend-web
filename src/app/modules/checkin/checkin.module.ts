import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckinRoutingModule } from './checkin-routing.module';
import { CheckInComponent } from './components/check-in/check-in.component';
import { ClarityModule, ClrFormsNextModule } from '@clr/angular';
import { ClrRadioModule } from '@clr/angular';
import {BreadcrumbsModule} from "ng6-breadcrumbs";

@NgModule({
  imports: [
    CommonModule,ClarityModule,ClrFormsNextModule, FormsModule, ReactiveFormsModule,ClrRadioModule,BreadcrumbsModule,

    CheckinRoutingModule
  ],
  declarations: [CheckInComponent]
})
export class CheckinModule { }
