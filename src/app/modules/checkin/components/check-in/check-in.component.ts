import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ListaElement, Mesa, SucursalService} from '../../../sucursales/services/sucursal.service';
import {CheckinService} from '../../services/checkin.service';
import swal from 'sweetalert2';
import {Router} from '@angular/router';


@Component({
    selector: 'app-check-in',
    templateUrl: './check-in.component.html',
    styleUrls: ['./check-in.component.css'],
    exportAs: 'ngModel'
})
export class CheckInComponent implements OnInit {

    sucursales: ListaElement[] = [];
    sucursal: ListaElement = null;
    correcto: boolean = false;
    error: boolean = false;
    complete: boolean = false;
    sucursalNumero: number;
    options: string;
    seleccionarSucursal: boolean = false;
    numeroMesa: number;
    creado: any;
    checkin: boolean = false;
    items: Mesa[] = [];
    urlMesa: string = '../../../../../assets/silla.png';
    urlMesaRed: string = '../../../../../assets/sillaRed.png';
    urlMesaGreen: string = '../../../../../assets/sillaGreen.png';
    urlMesaYellow: string = '../../../../../assets/sillaYellow.png';

    constructor(private sucursalService: SucursalService, private service: CheckinService,
                private router: Router) {
    }

    ngOnInit() {

        this.sucursalService.getListaBasic().subscribe(data => {

            this.sucursales = data;

        });
    }

    checkinOn() {
        this.checkin = true;
    }

    checkinOf() {
        this.checkin = false;
    }

    sucursalOn() {
        this.seleccionarSucursal = true;
        if (this.options === 'no') {
            this.traerMesas();
        }
    }

    onSubmit(f: NgForm) {

        this.complete = false;
        this.correcto = false;
        this.error = false;

        if (f.valid && this.options === 'si') {
            this.complete = false;
            this.service.IngresarClienteReserva(f.value.reserva).subscribe(ok => {
                if (ok) {
                    this.correcto = true;

                } else {
                    this.error = true;

                }
            });
            this.correcto = true;
            swal("Bien hecho!", "Check-in realizado correctamente!", "success");
            this.router.navigate(['/verCarrito', f.value.reserva]);
        }
        if (f.valid && this.options === 'no') {
            this.complete = false;
            this.service.IngresarCliente(this.numeroMesa, this.sucursalNumero).subscribe(ok => {
                if (ok) {
                    this.correcto = true;

                } else {
                    this.error = true;

                }
            });
        }
    }

    traerMesas() {
        this.sucursalService.getMesasActual(this.sucursalNumero).subscribe(data => {
            this.items = [];
            this.items = data;
            this.items.forEach(item => {

                if (item.tieneReserva) {
                    item.stat = 2;
                }
                if (item.noTienePedido === false) {
                    item.stat = 0;
                } else {
                    item.stat = 1;
                }
            });
        });
    }

    seleccionarMesa(mesa: Mesa) {
        if (mesa.stat === 1 || mesa.stat === 2) {
            swal({
                text: 'Mesa seleccionada: ' + mesa.id
            });
            this.checkin = true;
            this.seleccionarSucursal = false;
            this.numeroMesa = mesa.id;
        }
    }
}
