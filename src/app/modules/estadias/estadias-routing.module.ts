import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { VerEstadiasComponent } from './components/ver-estadias/ver-estadias.component';

const routes: Routes = [
  {
      path: '',
      component: AuxviewComponent,
      children: [
          {
              path: 'estadias/:idSucursal',
              component: VerEstadiasComponent
          }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstadiasRoutingModule { }
