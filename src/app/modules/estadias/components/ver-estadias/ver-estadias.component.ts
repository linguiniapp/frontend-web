import {Component, OnInit} from '@angular/core';
import {Estadia, EstadiasService} from '../../services/estadias.service';
import {ActivatedRoute} from '@angular/router';
import {Mesa, SucursalService} from '../../../sucursales/services/sucursal.service';


@Component({
    selector: 'app-ver-estadias',
    templateUrl: './ver-estadias.component.html',
    styleUrls: ['./ver-estadias.component.css']
})
export class VerEstadiasComponent implements OnInit {


    numeroSucursal: any;

    items: Mesa[] = [];
    urlMesa: string = '../../../../../assets/silla.png';
    urlMesaRed: string = '../../../../../assets/sillaRed.png';
    urlMesaGreen: string = '../../../../../assets/sillaGreen.png';
    estadia: boolean = false;
    estadias: Estadia[] = [];
    estadiaSeleccionada: Estadia = null;

    constructor(private service: EstadiasService, private activateRoute: ActivatedRoute, private sucursalService: SucursalService) {
    }


    ngOnInit() {
        this.activateRoute.params.subscribe(params => {
            this.numeroSucursal = params['idSucursal'];
            this.sucursalService.verSucursalMesa(this.numeroSucursal).subscribe(data => {
                this.items = [];
                this.items = data;
                this.items.forEach(item => {
                    item.stat = 0;
                });
            });
            this.service.verEstadias(this.numeroSucursal).subscribe(data => {
                this.estadias = data;
                this.estadias.forEach(estadia => {
                    estadia.mesas.forEach(mesa => {
                        this.items.forEach(item => {
                            if (item.id === mesa) {
                                item.stat = 1;
                            }
                        });
                    });
                });
            });
        });
    }

    verEstadia(mesa: Mesa) {
        this.estadia = true;

        this.estadiaSeleccionada = this.estadias.find(e =>
            e.mesas.includes(mesa.id)
        );


    }

    cerrarModal() {
        this.estadia = false;
    }


}
