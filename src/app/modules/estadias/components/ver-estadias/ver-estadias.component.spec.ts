import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerEstadiasComponent } from './ver-estadias.component';

describe('VerEstadiasComponent', () => {
  let component: VerEstadiasComponent;
  let fixture: ComponentFixture<VerEstadiasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerEstadiasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerEstadiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
