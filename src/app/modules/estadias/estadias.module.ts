import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstadiasRoutingModule } from './estadias-routing.module';
import { VerEstadiasComponent } from './components/ver-estadias/ver-estadias.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule, ClrFormsNextModule } from '@clr/angular';


@NgModule({
  imports: [
    CommonModule,
    EstadiasRoutingModule,FormsModule, ReactiveFormsModule, ClarityModule, ClrFormsNextModule
  ],
  declarations: [VerEstadiasComponent]
})
export class EstadiasModule { }
