import { TestBed } from '@angular/core/testing';

import { EstadiasService } from './estadias.service';

describe('EstadiasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadiasService = TestBed.get(EstadiasService);
    expect(service).toBeTruthy();
  });
});
