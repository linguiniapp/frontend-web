import {Injectable} from '@angular/core';
import {Apollo, gql,} from 'apollo-angular-boost';
import {Observable, Subject} from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class EstadiasService {


    private EstadiasList = new Subject<Estadia[]>();
    private prefacturaDTO: Estadia;


    constructor(private apollo: Apollo) {
    }


    verEstadias(idSucursal): Observable<Estadia[]> {
        this.apollo.watchQuery<any>({
            query: gql
                `    {
          verSucursal(numeroSucursal: ${idSucursal}) {
            numero
            mesas {
              edges {
                node {
                  numero
                  capacidadPersonas
                  posicionX
                  posicionY
                }
              }
            }
            estadias {
              edges {
                node {
                  reserva{
                    numero
                  }
                  mesas{
                    edges{
                      node{
                        numero
                      }
                    }
                  }
                  cantidadPersonas
                  fechaHoraLlegada
                  numero
                  mozo {
                    nombre
                    apellido
                  }
                  clientes {
                    edges {
                      node {
                        nombre
                        apellido
                      }
                    }
                  }
                  pedidos {
                    edges {
                      node {
                        numero
                        detalles {
                          edges {
                            node {
                              cantidad
                              cartaProducto {
                                precio
                                producto {
                                  nombre
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        `, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({data}) => {
                let estadias$: Estadia[] = [];


                let pre = data.verSucursal.estadias;

                if (pre) {


                    pre.edges.forEach(element => {

                        let detalleList: EstadiaDetalleProducto[];
                        if (element.node.pedidos.edges.node) {
                            element.node.pedidos.edges.node.detalles.edges.forEach(ped => {

                                let dto: EstadiaDetalleProducto = {

                                    cantidad: ped.node.cantidad,
                                    nombre: ped.node.cartaProducto.producto.nombre,
                                    precio: ped.node.cartaProducto.precio,
                                    subtotal: (ped.node.cantidad * (ped.node.cartaProducto.precio as number)).toString()
                                };
                                detalleList.push(dto);

                            });
                        }

                        let total: number = 0;
                        if (element.node.pedidos.edges.node) {
                            detalleList.forEach(d => {

                                total += Number(d.subtotal);

                            });
                        }

                        let mesas: number[] = [];

                        element.node.mesas.edges.forEach(m => {
                            mesas.push(m.node.numero);

                        });

                        let estadia: Estadia = {
                            sucursal: idSucursal,
                            cliente: null,
                            total: total,
                            mozo: null,
                            reserva: null,
                            mesas: mesas,
                            detalles: detalleList,
                            cantidadPersonas: element.node.cantidadPersonas,
                            fechaHoraLlegada: element.node.fechaHoraLlegada
                        };
                        if (element.cliente) {
                            estadia.cliente = element.clientes.edges[0].node.nombre + ' ' + element.node.clientes.edges[0].node.apellido;
                        }
                        if (element.mozo) {
                            estadia.mozo = element.node.mozo.nombre + ' ' + element.node.mozo.apellido;
                        }
                        if (element.node.reserva) {
                            estadia.reserva = element.node.reserva.numero;
                        }

                        estadias$.push(estadia);


                    });

                    this.EstadiasList.next(estadias$);
                    // let estadias: Estadia = {
                    //   sucursal: pre.estadia.sucursal.nombre,
                    //   cliente: pre.estadia.reserva.cliente.nombre + " " + pre.estadia.reserva.cliente.apellido,
                    //   total: pre.total,
                    //   mozo: pre.estadia.mozo.nombre + " " + pre.estadia.mozo.apellido,
                    //   reserva: pre.estadia.reserva.numero,
                    //   mesas: pre.mesas,
                    //   detalles: detalleList
                    // }
                    // console.log(prefactura);
                    // this.prefacturaDTO = prefactura;
                } else {
                    console.log('no se encuentran estadias');
                }

            });

        return this.EstadiasList.asObservable();
        //     this.ListaElement.next(ELEMENT_DATA$);
        //   });
        // return this.ListaElement.asObservable();
    }


}


export interface Estadia {
    sucursal: string;
    cliente: string; //nombre y apellido de clientes edges node solo tomaremos uno solo
    cantidadPersonas: number;
    fechaHoraLlegada: string;
    mozo: string;
    reserva: number;
    total?: number;
    mesas: number[];
    detalles: EstadiaDetalleProducto[]
}

export interface EstadiaDetalleProducto {
    nombre: string;
    cantidad: number;
    precio: string;
    subtotal: string;
}