import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CajaService {
  montoEstimado = new Subject<number>();
  abrirCajaStatus = new Subject<boolean>();
  cerrarCajaStatus = new Subject<boolean>();
  constructor(private apollo: Apollo, private router: Router) { } 
  abrirCaja(nroSucursal: number, montoInicial: string, comentario: string):Observable<Boolean>{
    this.apollo
    .mutate({
        mutation: gql
        `mutation{
            abrirCaja(comentarios: "${comentario}", montoInicial: "${montoInicial}", numeroSucursal: ${nroSucursal}){
            ok
            sucursal{
                caja{
                  fechaHoraApertura
                  montoInicial
                }
            }
            }   
        }`
    })
    .subscribe(({ data }) => {
        console.log(data);
        if (data.abrirCaja.ok =true){
            this.abrirCajaStatus.next(true);
        }else{
            this.abrirCajaStatus.next(false);
        }
    });
  return this.abrirCajaStatus.asObservable();
}
cerrarCaja(montoIngresado: string, comentario: string = "Ninguno", nroSucursal: number):Observable<boolean> {
    this.apollo
    .mutate({
        mutation: gql
        `mutation{
          cerrarCaja(comentarios: "${ comentario }", montoFinalReal: "${ montoIngresado }" , numeroSucursal: ${ nroSucursal }){
            ok
          }
        }`
    })
    .subscribe(({ data }) => {
        console.log(data);
        if (data.cerrarCaja.ok = true){
            this.cerrarCajaStatus.next(true);
        }else{
            this.cerrarCajaStatus.next(false);
        }
    });
  return this.cerrarCajaStatus.asObservable();
}
getMontoEstimado(nroSucursal: number):Observable<number>{
    this.apollo
    .watchQuery<any>({
        query: gql
        `{
            verCajaActiva(numeroSucursal: ${ nroSucursal }){
              montoFinalEstimado,
            }
          }`
    })
    .valueChanges
    .subscribe(({data}) => {
        console.log(data);
        this.montoEstimado.next(data.verCajaActiva.montoFinalEstimado);
    })
    ;
    return this.montoEstimado.asObservable();
}
}
