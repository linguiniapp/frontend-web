import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';
import { CajaService } from '../../services/caja.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-cerrar-caja',
  templateUrl: './cerrar-caja.component.html',
  styleUrls: ['./cerrar-caja.component.css']
})
export class CerrarCajaComponent implements OnInit {
idSucursal;
cajaCerrada: boolean;
montoEstimado: number = 0; 
montoIngresado: number;
  constructor(private _cajaService: CajaService, private activatedRoute: ActivatedRoute) { }
  cerrarCajaGroup = new FormGroup({
    montoIngresado: new FormControl('', [Validators.required, Validators.pattern("^[+-]?([0-9]*[.])?[0-9]+$")]),
    comentario: new FormControl(''),
  });
  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      this.idSucursal = params['idsucursal'];});
    this._cajaService.getMontoEstimado(this.idSucursal).subscribe( monto => this.montoEstimado = monto );
  }
    onSubmit(){
        this._cajaService.cerrarCaja(this.cerrarCajaGroup.value.montoIngresado, this.cerrarCajaGroup.value.comentario, this.idSucursal).subscribe( ok=>{
          if(ok == true){
            this.cajaCerrada = true;
          }else{
            this.cajaCerrada = false;
          }
          console.log(this.cajaCerrada);
        });
    }
}
