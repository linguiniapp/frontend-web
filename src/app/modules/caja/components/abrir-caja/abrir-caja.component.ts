import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';
import { CajaService } from '../../services/caja.service';
import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-abrir-caja',
  templateUrl: './abrir-caja.component.html',
  styleUrls: ['./abrir-caja.component.css']
})
export class AbrirCajaComponent implements OnInit {
  idSucursal: number;
  cajaAbierta: boolean;
  constructor(private _cajaService: CajaService, private activatedRoute: ActivatedRoute, private route : Router) { }
  abrirCajaGroup = new FormGroup({
    monto: new FormControl('', [Validators.required, Validators.pattern("^[+-]?([0-9]*[.])?[0-9]+$")]),
    comentario: new FormControl('', [Validators.required]),
  });
  ngOnInit(
  ) {
  }
  onSubmit(){
    this.activatedRoute.params.subscribe( params => {
      this.idSucursal = params['idsucursal'];
      this._cajaService.abrirCaja(this.idSucursal, this.abrirCajaGroup.value.monto, this.abrirCajaGroup.value.comentario).subscribe( 
        ok => {
                if(ok == true){
                  this.cajaAbierta = true;
                }
                else{
                  this.cajaAbierta = false; 
                }
                console.log(this.cajaAbierta);
      });
    });
  }
  volver(){
    this.route.navigate(["/admin-panel"])
  }
}
