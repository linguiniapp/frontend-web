import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { AbrirCajaComponent } from './components/abrir-caja/abrir-caja.component';
import { CerrarCajaComponent } from './components/cerrar-caja/cerrar-caja.component';
import { AuthService } from '../../security/login/auth.service';


const routes: Routes = [
  {
    path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'abrir-caja/:idsucursal', 
                component: AbrirCajaComponent, canActivate: [AuthService],
                data: {acciones: ["abrir_caja"]}
            },
            {
                path: 'cerrar-caja/:idsucursal',
                component: CerrarCajaComponent, canActivate: [AuthService],
                data: {acciones: ["cerrar_caja"]}
            }
          ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CajaRoutingModule { }
