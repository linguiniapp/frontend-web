import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CajaRoutingModule } from './caja-routing.module';
import { AbrirCajaComponent } from './components/abrir-caja/abrir-caja.component';
import { ClarityModule, ClrFormsNextModule, ClrAlertModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CerrarCajaComponent } from './components/cerrar-caja/cerrar-caja.component';


@NgModule({
  imports: [
    CommonModule,
    CajaRoutingModule,
    ClarityModule,
    ClrFormsNextModule,
    ClrAlertModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AbrirCajaComponent, CerrarCajaComponent]
})
export class CajaModule { }
