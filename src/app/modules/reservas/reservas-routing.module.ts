import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ReservasComponent } from './components/reservas/reservas.component';
import { AgregarReservaComponent } from './components/agregar-reserva/agregar-reserva.component';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { EditarReservaComponent } from './components/editar-reserva/editar-reserva.component';




const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'reservas/:id', 
                component: ReservasComponent
            },
            {
                path: 'agregarReserva', 
                component: AgregarReservaComponent
            },
            {
                path: 'editarReserva/:id', 
                component: EditarReservaComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReservasRoutingModule { }
