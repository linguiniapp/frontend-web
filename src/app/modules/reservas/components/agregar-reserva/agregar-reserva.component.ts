import { Component, ViewChild } from '@angular/core';
import { ListaElement } from '../../services/reservas.service';
import { ReservasService } from '../../services/reservas.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ReservasComponent } from '../reservas/reservas.component';
import { ClrWizard } from '@clr/angular';

import {  } from '@angular/common';

import { formatDate } from '@angular/common';
import { TimeFormatterPipe } from 'ngx-material-timepicker/src/app/material-timepicker/pipes/time-formatter.pipe';


export interface Cantidad {
  value: number;
}
export interface Mesa {
  value: number;
}



@Component({
  selector: 'app-agregar-reserva',
  templateUrl: './agregar-reserva.component.html',
  styleUrls: ['./agregar-reserva.component.css'],
  exportAs: 'ngModel'
})
export class AgregarReservaComponent  {
  open:boolean=false;
  @ViewChild("wizard") wizard: ClrWizard;
  @ViewChild("number") numberFi: any;
  date: "";

  model = {
    nombre: "",
    apellido: "",
    fecha: "",
    hora: "",
    cantidad: null,
    mesa: null,
    dni:""
  }
  opend(){
   this.open=true;
  }
  

  complete : boolean = false;
 nuevaReserva:ListaElement={numero:0,nombre:"",cantidad:0,fechaHora:"",mesas:[]};

  constructor(private routes:Router, private _reservasServicio:ReservasService,){}


  cantidades: Cantidad[] = [
    {value: 1},{value: 2},{value: 3},{value: 4},{value: 5},{value: 6},{value: 7},{value: 8},
    {value: 9},{value: 10},{value: 11},{value: 12},{value: 13},{value: 14},{value: 15},{value: 16},
    {value: 17},{value: 18},{value: 19},{value: 20},{value: 21},{value: 22},{value: 12},{value: 24},
    {value: 25},{value: 26},{value: 27},{value: 28},{value: 29},{value: 30},{value: 31},{value: 32},
    {value: 33},{value: 34},
  
  ];
  mesas: Mesa[] = [
    {value: 1},{value: 2},{value: 3},{value: 4},{value: 5},{value: 6},{value: 7},{value: 8},
    {value: 9},{value: 10},{value: 11},{value: 12},{value: 13},{value: 14},{value: 15},{value: 16},
    {value: 17},{value: 18},{value: 19},{value: 20},{value: 21},{value: 22},{value: 12},{value: 24},
    {value: 25},{value: 26},{value: 27},{value: 28},{value: 29},{value: 30},{value: 31},{value: 32},
    {value: 33},{value: 34},
  
  ];

  
  onSubmit() {
    this.nuevaReserva.nombre=this.model.nombre+','+this.model.apellido+','+this.model.dni;
    this.nuevaReserva.cantidad=this.model.cantidad;
    this.nuevaReserva.mesas.push(this.model.mesa);
    let fecha=formatDate(this.model.fecha, "yyyy-MM-dd", 'en-US');
    let hora=formatDate(this.model.fecha + " " + this.model.hora,'HHmmss','en-US').toString();
    this.nuevaReserva.fechaHora=fecha+'T'+hora;
    


    // this._reservasServicio.agregarReserva(this.nuevaReserva);   
    // this.routes.navigate(['/reservas']);
    
  }
  regresarLista(){
    this.routes.navigate(['/reservas'])
  }
  
}
