
import { Component, OnInit } from '@angular/core';
import { ListaElementt,ListaElement } from '../../services/reservas.service';
import { ReservasService } from '../../services/reservas.service';
import { NgForm } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';

export interface Cantidad {
  value: number;
}
export interface Mesa {
  value: number;
}

@Component({
  selector: 'app-editar-reserva',
  templateUrl: './editar-reserva.component.html',
  styleUrls: ['./editar-reserva.component.css']
})

export class EditarReservaComponent implements OnInit  {
date: "";
 complete: boolean = false;
 nuevaReserva:ListaElement={numero:0,nombre:"",cantidad:0,fechaHora:"",mesas:[]};
 reserva:ListaElementt={numero:0,nombre:"",cantidad:0,fecha:"",hora:"",mesas:[]};

  cantidad: Cantidad[] = [
    {value: 1},{value: 2},{value: 3},{value: 4},{value: 5},{value: 6},{value: 7},{value: 8},
    {value: 9},{value: 10},{value: 11},{value: 12},{value: 13},{value: 14},{value: 15},{value: 16},
    {value: 17},{value: 18},{value: 19},{value: 20},{value: 21},{value: 22},{value: 12},{value: 24},
    {value: 25},{value: 26},{value: 27},{value: 28},{value: 29},{value: 30},{value: 31},{value: 32},
    {value: 33},{value: 34},
  
  ];
  mesa: Mesa[] = [
    {value: 1},{value: 2},{value: 3},{value: 4},{value: 5},{value: 6},{value: 7},{value: 8},
    {value: 9},{value: 10},{value: 11},{value: 12},{value: 13},{value: 14},{value: 15},{value: 16},
    {value: 17},{value: 18},{value: 19},{value: 20},{value: 21},{value: 22},{value: 12},{value: 24},
    {value: 25},{value: 26},{value: 27},{value: 28},{value: 29},{value: 30},{value: 31},{value: 32},
    {value: 33},{value: 34},{value: 67},
  
  ];

  
  constructor( private actvateRoute:ActivatedRoute, private _reservaService: ReservasService, private routes:Router ) {
    this.actvateRoute.params.subscribe( params =>{
      this._reservaService.getElementoLista( params['id']).subscribe(r=> this.reserva=r);        
     });  
  }
  ngOnInit(): void {    
    
  }
  
  onSubmit(f: NgForm) {       
    this.nuevaReserva.nombre=f.value.nombre;
    this.nuevaReserva.cantidad=f.value.cant;
    this.nuevaReserva.mesas=[f.value.mes];   
    this.nuevaReserva.numero=this.reserva.numero;   
    let fecha=formatDate(f.value.fecha, "yyyy-MM-dd", 'en-US');
    let hora=formatDate(f.value.fecha + " " + f.value.hora,'HHmmss','en-US').toString();
    this.nuevaReserva.fechaHora=fecha+'T'+hora;      
    this.actvateRoute.params.subscribe( params =>{
    this._reservaService.editarReserva(this.nuevaReserva) }); 
    this.routes.navigate(['/reservas']) 
  }

  regresarLista(){
    this.routes.navigate(['/reservas'])
  }

}
