import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReservasService, ListaElement, ListaElementt } from '../../services/reservas.service';
import { Observable } from 'rxjs';
import { ClrWizard } from '@clr/angular';
import { formatDate } from '@angular/common';
import { SucursalService } from '../../../sucursales/services/sucursal.service';
import { Mesa as Messa } from '../../../sucursales/services/sucursal.service';
import { Alert } from 'selenium-webdriver';
import { PedidosService } from '../../../pedidos/services/pedidos.service';


export interface Cantidad {
  value: number;
}
export interface Mesa {
  value: number;
}

@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.component.html',
  styleUrls: ['./reservas.component.css']
})
export class ReservasComponent implements OnInit {
  @ViewChild("wizard") wizard: ClrWizard;
  @ViewChild("number") numberFi: any;
  seleccionarMesa: boolean = false;
  displayedColumns: string[] = ['position', 'nombre', 'cantidad', 'hora', 'mesa', 'editar', 'eliminar', 'agregar'];
  dataSource: ListaElementt[] = [];
  suscripcion: any;
  selectedUser: boolean;
  date: "";
  open: boolean = false;
  coincideCantidad: boolean = false;
  sucursal: number;
  capacidad: number = 0;
  horaDesde: string = "";
  horaHasta: string = "";
  completeInput: boolean;
  cantidadDisponible: number = 0;
  capacidadInput: boolean = false;
  fechaValid: boolean = false;
  tienePedido: boolean = false;
  elementType: 'url' | 'canvas' | 'img' = 'url';
  value: string = 'Techiediaries';
  basic: boolean = false;

  model = {
    forceReset: true,
    nombre: "",
    apellido: "",
    fecha: "",
    hora: "",
    cantidad: null,
    dni: ""
  }
  items: Messa[] = [];
  urlMesa: string = "../../../../../assets/silla.png";
  urlMesaRed: string = "../../../../../assets/sillaRed.png";
  urlMesaGreen: string = "../../../../../assets/sillaGreen.png";

  opend() {
    this.open = true;
  }
  public doReset(): void {
    if (this.model.forceReset) {
      this.wizard.reset();
      this.model.forceReset = true;
      this.model.nombre = "",
        this.model.apellido = "",
        this.model.fecha = "";
      this.model.hora = "";
      this.model.cantidad = null;
      this.model.dni = "";
    }
  }
  complete: boolean = false;
  nuevaReserva: ListaElement = { numero: 0, nombre: "", cantidad: 0, fechaHora: "", mesas: [] };

  constructor(private actvateRoute: ActivatedRoute, private router: Router, private _reservasService: ReservasService,
    private service: SucursalService, private pedidoService: PedidosService) { }

  cantidades: Cantidad[] = [
    { value: 1 }, { value: 2 }, { value: 3 }, { value: 4 }, { value: 5 }, { value: 6 }, { value: 7 }, { value: 8 },
    { value: 9 }, { value: 10 }, { value: 11 }, { value: 12 }, { value: 13 }, { value: 14 }, { value: 15 }, { value: 16 },
    { value: 17 }, { value: 18 }, { value: 19 }, { value: 20 }, { value: 21 }, { value: 22 }, { value: 12 }, { value: 24 },
    { value: 25 }, { value: 26 }, { value: 27 }, { value: 28 }, { value: 29 }, { value: 30 }, { value: 31 }, { value: 32 },
    { value: 33 }, { value: 34 },

  ];
  ngOnInit() {
    this.actvateRoute.params.subscribe(params => {
      this.suscripcion = this._reservasService.getLista(params['id']).subscribe(data => this.dataSource = data);
      this.sucursal = params['id'];
      this.service.getLista().subscribe(s => {
        this.horaDesde = s.find(f => f.numero == params['id']).horaDesde;


        if (this.horaDesde == undefined || this.horaDesde == "") {
          this.horaDesde = "12:00 am"
        }

        this.horaHasta = s.find(f => f.numero == params['id']).horaHasta;

        if (this.horaHasta == undefined || this.horaHasta == "") {
          this.horaHasta = "11:59 pm"
        }
      });

    });

    // this.items = this.service.getMesas();


  }

  verPedido(idReserva: number) {
    this.router.navigate(['verCarrito', idReserva]);
  }

  reservar(mesa: Messa) {
    if (mesa.tieneReserva!==true) {
      if (mesa.stat == 0) {
        // alert("marcada");
        mesa.stat = 1;
        this.capacidad += mesa.cantidad;

      } else {
        // alert("desmarcada");
        mesa.stat = 0;
        this.capacidad -= mesa.cantidad;

      }
    }
    else {
      // alert("Mesa Reserveda");
      return;
    }
    //alert(this.capacidad);

  }

  agregarReserva() {
    this.router.navigate(['/agregarReserva']);
  }
  eliminarReserva(idx: number) {
    this._reservasService.eliminarReserva(idx);
  }

  editarReserva(idx: number) {
    this.router.navigate(['/editarReserva', idx]);
  }
  tienePedidos(reservaId) {
    this.tienePedido = false;
    this.pedidoService.getPedido(reservaId).subscribe(data => {
 
      if (data.producto) {
        this.tienePedido = true;
      }
      else {
        this.tienePedido = false;
      }
    })
  }

  verificar(hora) {
    let horaChange = formatDate("2018-12-12" + " " + hora.value, 'HHmmss', 'en-US');
    let horaDesdeC = formatDate("2018-12-12" + " " + this.horaDesde, 'HHmmss', 'en-US');
    let horaHastaC = formatDate("2018-12-12" + " " + this.horaHasta, 'HHmmss', 'en-US');

    this.cantidadDisponible = 0;
    this.seleccionarMesas();
    // if (this.horaDesde 1) {

    // } else {

    // }

    if (horaChange >= horaDesdeC && horaChange <= horaHastaC) {
      this.completeInput = false;
    }
    else {

      this.model.cantidad = null;
      this.completeInput = true;
      this.model.hora = "";
    }

  }

  resetHora() {
    this.model.hora = "";
  }
  cerrar() {
    this.completeInput = false;
    this.capacidadInput = false;
  }
  seleccionarMesas() {
    this.cantidadDisponible = 0;
    let fecha = formatDate(this.model.fecha, "yyyy-MM-dd", 'en-US');
    let hora = formatDate(this.model.fecha + " " + this.model.hora, 'HHmmss', 'en-US').toString();
    this.nuevaReserva.fechaHora = fecha + 'T' + hora;
    this.service.getMesas(this.nuevaReserva.fechaHora, this.sucursal).subscribe(data => {
      this.items = [];
      this.items = data;
      this.cantidadDisponible = 0;
      this.items.forEach(item => {
        if (item.tieneReserva===true) {
          item.stat = 2;
        } else {
          item.stat = 0;
          this.cantidadDisponible = item.cantidad + this.cantidadDisponible
        }
      });
      if (this.cantidadDisponible < this.model.cantidad) {
        this.model.hora = "";
        this.capacidadInput = true;
        this.model.cantidad = null;
      }
      else {
        this.capacidadInput = false;
      }
    });
  }
  realizarPedido(reservaId) {
    this.router.navigate(['/verCartaGeneral/', this.sucursal, reservaId]);
  }
  activarMesas() {
    this.seleccionarMesa = !this.seleccionarMesa;
    this.seleccionarMesas2();
  }

  seleccionarMesas2() {
    this.cantidadDisponible = 0;
    let fecha = formatDate(this.model.fecha, "yyyy-MM-dd", 'en-US');
    let hora = formatDate(this.model.fecha + " " + this.model.hora, 'HHmmss', 'en-US').toString();
    this.nuevaReserva.fechaHora = fecha + 'T' + hora;
    this.service.getMesas(this.nuevaReserva.fechaHora, this.sucursal).subscribe(data => {
      this.items = [];
      this.items = data;
      this.cantidadDisponible = 0;
      this.items.forEach(item => {
        if (item.tieneReserva===true) {
          item.stat = 2;
        } else {
          item.stat = 0;
          this.cantidadDisponible = item.cantidad + this.cantidadDisponible
        }
      });
    })
  }


  onSubmit() {
    this.items.forEach(element => {
      if (element.stat == 1) {
        this.nuevaReserva.mesas.push(element.id);
      }
    });
    console.log(this.nuevaReserva.mesas);
    this.nuevaReserva.nombre = this.model.nombre + ',' + this.model.apellido + ',' + this.model.dni;
    this.nuevaReserva.cantidad = this.model.cantidad;
    let fecha = formatDate(this.model.fecha, "yyyy-MM-dd", 'en-US');
    let hora = formatDate(this.model.fecha + " " + this.model.hora, 'HHmmss', 'en-US').toString();
    this.nuevaReserva.fechaHora = fecha + 'T' + hora;
    this.actvateRoute.params.subscribe(params => {
      this._reservasService.agregarReserva(this.nuevaReserva, params['id']);
      this.value = this.model.nombre + ' ' + this.model.apellido + ' Fecha: ' + fecha + ' Hora: ' + hora + ' Mesa: ' + this.nuevaReserva.mesas[0];

    })
    this.seleccionarMesa = !this.seleccionarMesa;
    this.doReset();
    this.suscripcion.unsubscribe();
    this.ngOnInit();
    this.basic = true;
  }
  regresarLista() {
    this.router.navigate(['/reservas'])
  }

  verQr(nombre, cantidad, fecha, hora, mesa) {
    this.value = nombre + ' Cantidad de personas: ' + cantidad + ' Fecha: ' + fecha + ' Hora: ' +
      hora + ' Mesa: ' + mesa;
    this.basic = true;
  }

  verificarFecha(fecha) {

    let fechaAyer = new Date();
    let fecahAyerStr = formatDate(fechaAyer, "yyyy-MM-dd", 'en-US');
    let fechahoy = formatDate(fecha.value, "yyyy-MM-dd", 'en-US');

    if (fechahoy <= fecahAyerStr) {

      this.fechaValid = true;
      this.model.forceReset;
    }
    else {

      this.fechaValid = false;

    }

  }
}
