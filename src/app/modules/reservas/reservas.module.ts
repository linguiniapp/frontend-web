import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { FormsModule } from '@angular/forms';
import { ReservasComponent } from './components/reservas/reservas.component';
import { EditarReservaComponent } from './components/editar-reserva/editar-reserva.component';
import { AgregarReservaComponent } from './components/agregar-reserva/agregar-reserva.component';
import { ReservasRoutingModule } from './reservas-routing.module';
import { ClrDatagridModule} from "@clr/angular";
import { ClrIconModule} from "@clr/angular";
import { ClrFormsModule} from "@clr/angular";
import { ClrInputModule} from "@clr/angular";
import { ClrSelectModule} from "@clr/angular";
import { ClrMainContainerModule} from "@clr/angular";
import { ClrWizardModule, ClrModalModule } from '@clr/angular';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CalendarModule} from 'primeng/calendar';
import { NgxQRCodeModule } from 'ngx-qrcode2';




@NgModule({
  imports: [
    CommonModule,
    ClrInputModule,
    NgxMaterialTimepickerModule.forRoot(),
    FormsModule,
    ReservasRoutingModule,
    ClrDatagridModule,
    ClrIconModule,
    ClrFormsModule,
    ClrSelectModule,
    ClrMainContainerModule,
    ClrWizardModule,
    ClrModalModule,
    BrowserModule, BrowserAnimationsModule,
    CalendarModule,
    NgxQRCodeModule

  ],
  declarations: [ReservasComponent, EditarReservaComponent, AgregarReservaComponent]
})
export class ReservasModule { }
