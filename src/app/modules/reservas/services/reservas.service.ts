import { Injectable } from '@angular/core';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';


@Injectable()
export class ReservasService {
  reserva: ListaElement[] = [];
  reservas: ListaElementt[] = [];
  private ELEMENT_DATA = new Subject<ListaElementt[]>();
  private lelem = new Subject<ListaElementt>();

  constructor(private apollo: Apollo, private router: Router) {

    console.log("Ready to use");
  }
  getLista(sucursalId: number): Observable<ListaElementt[]> {
    this.apollo.watchQuery<any>({
      query: gql
        `{verReservas(numeroSucursal:${sucursalId},
            fechaHoraDesde:"2012-12-11T230502",
            fechaHoraHasta:"2019-12-12T230502") {
            edges {
              node {
                mesas {
                  edges {
                    node {
                      capacidadPersonas
                      numero
                    }
                  }
                }
                numero
                cantidadPersonas
                fechaHora
                cliente {
                  nombre
                  apellido
                }
              }
            }
          }
        }`,fetchPolicy:'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        let ELEMENT_DATA$: ListaElementt[]=[];
        data.verReservas.edges.forEach(r => {
          let mesasArray = [];
          let lelem: ListaElementt = {
            numero: r.node.numero,
            nombre: r.node.cliente.nombre + " " + r.node.cliente.apellido,
            cantidad: r.node.cantidadPersonas,
            fecha: r.node.fechaHora,
            hora: r.node.fechaHora,
            mesas: r.node.mesas.edges.forEach(r => {
              mesasArray.push(r.node.numero);
            })
          }
          lelem.mesas = mesasArray;
          let fechaHora = lelem.fecha.split('T', 2);
          fechaHora[0] = formatDate(fechaHora[0], "M/d/yyyy", 'en-US');
          fechaHora[1] = formatDate(fechaHora[0] + " " + fechaHora[1], "h:mm a", 'en-US');
          lelem.fecha = fechaHora[0];
          lelem.hora = fechaHora[1];
          ELEMENT_DATA$.push(lelem);
          this.ELEMENT_DATA.next(ELEMENT_DATA$);
        });
      });
    return this.ELEMENT_DATA.asObservable();

  }



  getElementoLista(idx: number) {

    this.apollo.watchQuery<any>({
      query: gql
        `{verReserva(numeroReserva:${idx}){   
          numero       
          fechaHora  
          cliente{
            nombre
            apellido
          }
          cantidadPersonas
          mesas{
            edges{
              node{
                numero
              }
            }
          }}
        }
        `
    })
      .valueChanges
      .subscribe(({ data }) => {


        let mesasArray = [];
        let lelem: ListaElementt = {
          numero: data.verReserva.numero,
          nombre: data.verReserva.cliente.nombre + " " + data.verReserva.cliente.apellido,
          cantidad: data.verReserva.cantidadPersonas,
          fecha: data.verReserva.fechaHora,
          hora: data.verReserva.fechaHora,
          mesas: data.verReserva.mesas.edges.forEach(r => {
            mesasArray.push(r.node.numero);
          })
        };
        lelem.mesas = mesasArray;
        let fechaHora = lelem.fecha.split('T', 2);
        fechaHora[0] = formatDate(fechaHora[0], "M/d/yyyy", 'en-US');
        fechaHora[1] = formatDate(fechaHora[0] + " " + fechaHora[1], "h:mm a", 'en-US');
        lelem.fecha = fechaHora[0];
        lelem.hora = fechaHora[1];
        this.lelem.next(lelem);
      }
      );

    return this.lelem.asObservable();

  }
  agregarReserva(elemento: ListaElement, sucursalId: number) {
    console.log(elemento.mesas);
    this.apollo
      .mutate({
        mutation: gql
          `mutation{
          crearReserva(cantidadPersonas:${elemento.cantidad},
          datosCliente:"${elemento.nombre}",
          numeroSucursal:${sucursalId},
          numerosMesas:[${elemento.mesas}],
          fechaHora:"${elemento.fechaHora}"){
           ok
           reserva{
           numero
           cliente{
              nombre
              apellido
            }
           }
           }
         }`, fetchPolicy:'no-cache'
      })
      .subscribe(({ data }) => {
        if (data.crearReserva.ok = "true") {
          
        }
        else {
        }
      }, (error) => {
        console.log('error', error);
      });

    this.refresh();

  };
  eliminarReserva(x) {
    this.apollo
      .mutate({
        mutation: gql
          `mutation{
          anularReserva(numero:${x},comentario:"porque puedo"){
            ok   
          }
        }`
        ,
      })
      .subscribe(({ data }) => {
        if (data.ok = "true") {

          this.getLista(1);
        }
        else {
          alert("error")
        }
      }, (error) => {
        console.log('error', error);
      });

  }
  editarReserva(reserva: ListaElement) {
    this.apollo
      .mutate({
        mutation: gql
          `mutation{
        modificarReserva(cantidadPersonas:${reserva.cantidad},
        numeroSucursal:1,
        numero:${reserva.numero},
        fechaHora:"${reserva.fechaHora}"){
         ok
         reserva{
         numero
         }
         }
       }`
        ,
      })
      .subscribe();
  }

  refresh() {
    this.ELEMENT_DATA.next(this.reservas);
  }

}
export interface Cliente {
  nombre: string;
  apellido: string;
}
export interface Mesa {
  numero: number;
  capacidadPersonas: number;
}
export interface verReservas {
  mesas: Mesa[];
  fechaHora: string;
  cliente: Cliente;
}
export interface ListaElement {
  numero: number;
  nombre: string;
  cantidad: number;
  fechaHora: string;
  mesas: number[];
}
export interface ListaElementt {
  numero: number;
  nombre: string;
  cantidad: number;
  fecha: string;
  hora: string;
  mesas: number[];
}
