import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerPerfilComponent } from './components/ver-perfil/ver-perfil.component';
import { EditarPerfilComponent } from './components/editar-perfil/editar-perfil.component';
import { PerfilRoutingModule } from './perfil-restaurant-routing.module';
import { ClrWizardModule } from "@clr/angular";
import { ClrButtonModule } from "@clr/angular";
import { ClarityModule, ClrFormsNextModule, ClrAlertModule, ClrDatepickerModule, ClrSelectModule, ClrPasswordModule } from '@clr/angular';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    PerfilRoutingModule,
    ClrWizardModule,
    ClrButtonModule,
    ClarityModule,
    ClrFormsNextModule,
    ClrDatepickerModule,
    ClrSelectModule,
    ClrAlertModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [VerPerfilComponent, EditarPerfilComponent]
})
export class PerfilRestaurantModule { }
