import {Component, ViewChild, OnDestroy, OnInit} from '@angular/core';
import {ClrWizard} from '@clr/angular';
import { PerfilService, DatosCliente, DatosEmpleado, DatosAdmin } from '../../perfil.service';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { formatDate } from '@angular/common';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';
import { ValidadorPasswords } from '../../../../security/reestablecer-password/reestablecer-password.component';
import { Sexo } from '../../../abm-cliente/components/alta-cliente/alta-cliente.component';
import { SucursalService, ListaElement } from 'src/app/modules/sucursales/services/sucursal.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-ver-perfil',
  templateUrl: './ver-perfil.component.html',
  styleUrls: ['./ver-perfil.component.css']
})
export class VerPerfilComponent implements OnDestroy, OnInit {
  sucursales: ListaElement[] = [];
  tipoUsuario: string;
  datosEmpleado: DatosEmpleado;
  datosCliente: DatosCliente;
  datosAdmin: DatosAdmin;
  clienteGroup = new FormGroup({
    nombre: new FormControl('', [Validators.required]),
    apellido: new FormControl('', [Validators.required]),
    sexo: new FormControl('' , [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    telefono: new FormControl('', [Validators.required]),
    fechaNacimiento: new FormControl('', [Validators.required]),
    dni: new FormControl('', [Validators.required]),
    direccion: new FormControl('', [Validators.required]),
  });
  adminGroup: any;
  empleadoGroup = new FormGroup({
    nombre: new FormControl('', [Validators.required] ),
    apellido: new FormControl('', [Validators.required]),
    sexo: new FormControl('' , [Validators.required]),
    email: new FormControl('', [Validators.required]),
    fechaNacimiento: new FormControl('' , [Validators.required]),
    telefono: new FormControl('', [Validators.required]),
    cuil: new FormControl('', [Validators.required]),
    nroSucursal: new FormControl( '', [Validators.required]),
  });
  obs1: any;
  obs2: any;
  editIndex = false;
  passGroup = new FormGroup({
    passVieja: new FormControl('', [Validators.required, ]),
    pass1: new FormControl('', [Validators.minLength(8), Validators.required]),
    pass2: new FormControl('', [Validators.minLength(8), Validators.required])
  }, {  validators: ValidadorPasswords});
  sexos: Sexo[] = [
    { value: 'Masculino', },
    { value: 'Femenino', },
    { value: 'No quiero decirlo' }
  ];

  constructor(private _perfilService: PerfilService, private router: Router,  private _sucursalService: SucursalService ) {}
  ngOnInit(): void {
    this.obs1 = this._perfilService.getTipoUsuario().pipe(take(1)).subscribe( tipoUser => {
      this.tipoUsuario = tipoUser;
      switch (tipoUser) {
        case 'admin':
          console.log('Llamando al getAdminData');
          this.getDatosAdmin();
          break;
        case 'cliente':
          this.getDataCliente();
          break;
        case 'empleado':
          this.getDataEmpleado();
          console.log('Llamar a ver empleado');
          break;
      }
    });
    this.obs2 = this._sucursalService.getLista().subscribe(data => {
      data.forEach(sucursal => {
          const Sucursal: ListaElement = {
              direccion: sucursal.direccion,
              foto: sucursal.foto,
              horaDesde: sucursal.horaDesde,
              horaHasta: sucursal.horaHasta,
              latitud: sucursal.latitud,
              longitud: sucursal.longitud,
              nombre: sucursal.nombre,
              numero: sucursal.numero,
              telefono: sucursal.telefono
          };
          this.sucursales.push(Sucursal);
      });
  });
}
  ngOnDestroy() {
    this.obs1.unsubscribe();
    this.obs2.unsubscribe();
    this.tipoUsuario = null;
    this.clienteGroup = null;
    this.empleadoGroup = null;
    this.adminGroup = null;
  }
  getDataCliente() {
     this._perfilService.getDatosCliente().pipe(take(1)).subscribe( data => {
      this.datosCliente = data;
      this.clienteGroup.setValue({
        nombre: this.datosCliente.nombre,
        apellido: this.datosCliente.apellido,
        sexo: this.datosCliente.sexo,
        email: this.datosCliente.email,
        telefono: this.datosCliente.telefono,
        fechaNacimiento: formatDate(this.datosCliente.fechaNacimiento, 'MM/dd/yyyy', 'en-US'),
        dni: this.datosCliente.dni,
        direccion: this.datosCliente.direccion
        });
      });
  }
  getDatosAdmin() {
    this._perfilService.getDatosAdmin().pipe(take(1)).subscribe(data => {
      this.datosAdmin = data;
      this.adminGroup = new FormGroup({
        email: new FormControl(this.datosAdmin.mail, [Validators.required, Validators.email]),
        passOriginal: new FormControl('', [Validators.required]),
        pass1: new FormControl('', [Validators.minLength(8), Validators.required]),
        pass2: new FormControl('', [Validators.minLength(8), Validators.required])
      }, {  validators: ValidadorPasswords});
    });
  }
  getDataEmpleado() {
    this._perfilService.getDatosEmpleado().pipe(take(1)).subscribe(data => {
      this.datosEmpleado = data;
      this.empleadoGroup.setValue({
        nombre: this.datosEmpleado.nombre,
        apellido: this.datosEmpleado.apellido,
        sexo: this.datosEmpleado.sexo ,
        email: this.datosEmpleado.email,
        fechaNacimiento: formatDate(this.datosEmpleado.fechaNacimiento, 'MM/dd/yyyy', 'en-US'),
        telefono: this.datosEmpleado.telefono,
        cuil: this.datosEmpleado.cuil,
        nroSucursal:  this.datosEmpleado.nroSucursal,
      });
    });
  }
  habilitarEdicion() {
    this.editIndex = true;
  }
  deshabilitarEdicion() {
    this.editIndex = false;
  }
  updateDatos(rol: string) {
    switch (rol) {
      case 'admin':
        this._perfilService.updateMailAdmin( this.adminGroup.value.email).subscribe( data => {
          switch (data) {
            case 0:
              swal({
                type: 'success',
                title: 'Operación exitosa',
                text: 'Tus cambios han sido guardados con éxito .'
              });
              this.editIndex = false;
              break;
            case 400:
              swal({
                type: 'error',
                title: 'Error',
                text: 'Ha ocurrido un error: el mail ingresado es el mismo que el actual'
              });
              break
            case 500:
              swal({
                type: 'error',
                title: 'Error del backend',
                text: 'Explotó el backend y no hay remedio que lo salve.'
              });
              break;
            default:
              swal({
                type: 'warning',
                title: 'Ha ocurrido algo inesperado',
                text: 'Ha ocurrido una error en el servidor al procesar tu solicitud. Intenta nuevamente.'
              });
          }
        });
        break;
      case 'empleado':
        const datosemp: DatosEmpleado = {
          telefono: this.empleadoGroup.value.telefono,
          sexo: this.empleadoGroup.value.sexo,
          nroSucursal: this.empleadoGroup.value.nroSucursal,
          nombre: this.empleadoGroup.value.nombre,
          apellido: this.empleadoGroup.value.apellido,
          fechaNacimiento: formatDate(this.empleadoGroup.value.fechaNacimiento, 'yyyy-MM-dd', 'en-US'),
          email: this.empleadoGroup.value.email,
          cuil: this.empleadoGroup.value.cuil
      };
        this._perfilService.updatePerfilEmpleado(datosemp).subscribe( data => {
          switch (data) {
            case 0:
              swal({
                type: 'success',
                title: 'Operación exitosa',
                text: 'Tus cambios han sido guardados con éxito .'
              });
              this.editIndex = false;
              break;
            case 400:
              swal({
                type: 'error',
                title: 'Error',
                text: 'Ha ocurrido un error.'
              });
              break;
            default:
              swal({
                type: 'warning',
                title: 'Ha ocurrido algo inesperado',
                text: 'Ha ocurrido una error en el servidor al procesar tu solicitud. Intenta nuevamente.'
              });
          }
        });
        break;
      case 'cliente':
        const datos: DatosCliente = {
          apellido: this.clienteGroup.value.apellido ,
          direccion: this.clienteGroup.value.direccion,
          dni: this.clienteGroup.value.dni,
          email: this.clienteGroup.value.email,
          fechaNacimiento: formatDate(this.clienteGroup.value.fechaNacimiento, 'yyyy-MM-dd', 'en-US'),
          nombre: this.clienteGroup.value.nombre,
          sexo: this.clienteGroup.value.sexo,
          telefono: this.clienteGroup.value.telefono
        };
        this._perfilService.updatePerfilCliente(datos).subscribe( data => {
          switch (data) {
            case 0:
              swal({
                type: 'success',
                title: 'Operación exitosa',
                text: 'Tus cambios han sido guardados con éxito .'
              });
              this.editIndex = false;
              break;
            case 400:
              swal({
                type: 'error',
                title: 'Error',
                text: 'Ha ocurrido un error.'
              });
              break;
            default:
              swal({
                type: 'warning',
                title: 'Ha ocurrido algo inesperado',
                text: 'Ha ocurrido una error en el servidor al procesar tu solicitud. Intenta nuevamente.'
              });
          }
        });
        break;
    }
  }
  cambiarPass(rol: string) {
    console.log('Cambiando pass de ' + rol);
    if (rol === 'admin') {
      this._perfilService.cambiarPassword(this.adminGroup.value.passOriginal, this.adminGroup.value.pass1).subscribe( data => {
        switch (data) {
          case 0:
            swal({
              type: 'success',
              title: 'Operación exitosa',
              text: 'Tu contraseña fue cambiada exitosamente.'
            });
            this.editIndex = false;
            break;
          case 400:
            swal({
              type: 'error',
              title: 'Error de contraseña',
              text: 'La contraseña actual que ingresaste no es correcta. Revísala e intenta nuevamente.'
            });
            this.adminGroup.setValue({
              passOriginal: '',
              pass1: '',
              pass2: ''
            });
            break;
          default:
            swal({
              type: 'warning',
              title: 'Ha ocurrido algo inesperado',
              text: 'Ha ocurrido una error en el servidor al procesar tu solicitud. Intenta nuevamente.'
            });
            this.adminGroup.setValue({
              passOriginal: '',
              pass1: '',
              pass2: ''
            });
        }
      });
    } else {
      this._perfilService.cambiarPassword(this.passGroup.value.passVieja, this.passGroup.value.pass1).subscribe( data => {
        switch (data) {
          case 0:
            swal({
              type: 'success',
              title: 'Operación exitosa',
              text: 'Tu contraseña fue cambiada exitosamente.'
            });
            this.editIndex = false;
            break;
          case 400:
            swal({
              type: 'error',
              title: 'Error de contraseña',
              text: 'La contraseña actual que ingresaste no es correcta. Revísala e intenta nuevamente.'
            });
            this.passGroup.setValue({
              passVieja: '',
              pass1: '',
              pass2: ''
            });
            break;
          default:
            swal({
              type: 'warning',
              title: 'Ha ocurrido algo inesperado',
              text: 'Ha ocurrido una error en el servidor al procesar tu solicitud. Intenta nuevamente.'
            });
            this.passGroup.setValue({
              passVieja: '',
              pass1: '',
              pass2: ''
            });
        }
      });
    }

  }
}
