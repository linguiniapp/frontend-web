import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { VerPerfilComponent } from './components/ver-perfil/ver-perfil.component';

const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'verPerfil', 
                component: VerPerfilComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PerfilRoutingModule { }
