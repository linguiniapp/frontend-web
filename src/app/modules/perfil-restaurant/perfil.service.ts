import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators'
import swal from 'sweetalert2';
@Injectable({
  providedIn: 'root'
})
export class PerfilService {
  constructor(private apollo: Apollo, private router: Router) { }
  responseCambiarPSW = new Subject<number>();
  responseUpdateEmpleado = new Subject<number>();
  responseUpdateCliente = new Subject<number>();
  responseUpdateAdmin = new Subject<number>();
  tipoUsuario = new Subject<string>();
  datosEmpleado = new Subject<DatosEmpleado>();
  datosCliente = new Subject<DatosCliente>();
  datosAdmin = new Subject<DatosAdmin>();
  getTipoUsuario(): Observable<string> {
    this.apollo
      .watchQuery<any>({
        query: gql
          `{
        verificarUsuario {
          nombre
          tipoUsuario {
            nombre
          }
          persona{
            email
          }
        }
      }
      
      `
      }).valueChanges.subscribe(({ data }) => {
        this.tipoUsuario.next(data.verificarUsuario.tipoUsuario.nombre);
      });
    return this.tipoUsuario.asObservable();
  }
  getDatosEmpleado(): Observable<DatosEmpleado> {
    this.apollo.watchQuery<any>({
      query: gql
        `query{
        verEmpleado{
          nombre
          apellido
          sexo
          email
          telefono
          fechaNacimiento
          cuil
          sucursal{
            numero
          }
        }
      }`
      , fetchPolicy: 'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        const empleado: DatosEmpleado = {
          nombre: data.verEmpleado.nombre,
          apellido: data.verEmpleado.apellido,
          sexo: data.verEmpleado.sexo,
          email: data.verEmpleado.email,
          telefono: data.verEmpleado.telefono,
          fechaNacimiento: data.verEmpleado.fechaNacimiento,
          cuil: data.verEmpleado.cuil,
          nroSucursal: data.verEmpleado.sucursal.numero
        };
        this.datosEmpleado.next(empleado);
      });
    return this.datosEmpleado.asObservable();
  }
  getDatosAdmin(): Observable<DatosAdmin> {
    this.apollo.watchQuery<any>({
      query: gql
        `query {
        verificarUsuario {
            persona{
              email
            }
          }
        }`
      , fetchPolicy: 'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        const datosadmin: DatosAdmin = {
          mail: data.verificarUsuario.persona.email
        };
        this.datosAdmin.next(datosadmin);
      });
    return this.datosAdmin.asObservable();
  }
  getDatosCliente(): Observable<DatosCliente> {
    this.apollo.watchQuery<any>({
      query: gql
        `query{
        verCliente{
          nombre
          apellido
          sexo
          email
          telefono
          fechaNacimiento
          dni
          direccion
          numero
        }
      }`
      , fetchPolicy: 'no-cache'
    }).valueChanges
      .subscribe(({ data }) => {
        const cliente: DatosCliente = {
          nombre: data.verCliente.nombre,
          apellido: data.verCliente.apellido,
          sexo: data.verCliente.sexo,
          email: data.verCliente.email,
          telefono: data.verCliente.telefono,
          fechaNacimiento: data.verCliente.fechaNacimiento,
          dni: data.verCliente.dni,
          direccion: data.verCliente.direccion
        };
        this.datosCliente.next(cliente);
      });
    return this.datosCliente.asObservable();
  }
  updatePerfilEmpleado(datos: DatosEmpleado ) {
    this.apollo.mutate({
      mutation: gql`
      mutation{
        modificarEmpleado(
          apellido: "${ datos.apellido}",
          cuil: "${ datos.cuil }", 
          email: "${ datos.email }", 
          fechaNac: "${ datos.fechaNacimiento}", 
          nombre: "${ datos.nombre}", 
          numeroSucursal: ${ datos.nroSucursal }, 
          sexo: "${ datos.sexo }", 
          telefono: "${ datos.telefono }")
          {
            ok
          }
        }`
    }).subscribe( ({data}) => {
      this.responseUpdateEmpleado.next(parseInt(data.modificarEmpleado.ok));
    });
    return this.responseUpdateEmpleado.asObservable();
  }
  updatePerfilCliente( datosCliente: DatosCliente) {
    this.apollo.mutate({
      mutation: gql`
        mutation {
          modificarCliente(infoCliente: {
            nombre: "${datosCliente.nombre}",
            apellido: "${datosCliente.apellido}" ,
            sexo: "${datosCliente.sexo}",
            email: "${datosCliente.email}",
            telefono: "${datosCliente.telefono}",
            fechaNacimiento: "${datosCliente.fechaNacimiento}",
            dni: "${datosCliente.dni}",
            direccion: "${datosCliente.direccion}"
          }, )
          {
            ok 
          }
        }`
    })
        .subscribe( ({data}) => {
          if (data.modificarCliente.ok === true) {
            this.responseUpdateCliente.next(0);
          } else {
            this.responseUpdateCliente.next(409);
          }
        });
    return this.responseUpdateCliente.asObservable();
  }
  updateMailAdmin( nuevoMail: string) {
    this.apollo.mutate({
      mutation: gql`
      mutation{
        changeAdminEmail(newEmail: "${ nuevoMail }"){
          ok
        }
      }
      `
    }).subscribe( ({data}) => {
      if (data) {
      this.responseUpdateAdmin.next(parseInt(data.changeAdminEmail.ok));
      }
    }, errors => {
      if (errors) {
        this.responseUpdateAdmin.next(500);
      }
    });
    return this.responseUpdateAdmin.asObservable();
  }
  cambiarPassword(oldPass: string, newPass: string) {
    this.apollo.mutate({
      mutation: gql`
      mutation{
        changePassword(newPass: "${ newPass }", oldPass: "${ oldPass }"){
          ok
        }
      }`
    })
    .subscribe(({data}) => {
      const codigoRespuesta = parseInt(data.changePassword.ok);
      this.responseCambiarPSW.next(codigoRespuesta);
    });
    return this.responseCambiarPSW.asObservable();
  }
}

export class DatosAdmin {
  mail: string;
}
export class DatosCliente {
  nombre: string;
  apellido: string;
  sexo: string;
  email: string;
  telefono: string;
  fechaNacimiento: string;
  dni: string;
  direccion: string;
}
export class DatosEmpleado {
  nombre: string;
  apellido: string;
  sexo: string;
  email: string;
  telefono: string;
  fechaNacimiento: string;
  cuil: string;
  nroSucursal: number;
}
