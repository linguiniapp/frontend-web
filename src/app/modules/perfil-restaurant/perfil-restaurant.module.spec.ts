import { PerfilRestaurantModule } from './perfil-restaurant.module';

describe('PerfilRestaurantModule', () => {
  let perfilRestaurantModule: PerfilRestaurantModule;

  beforeEach(() => {
    perfilRestaurantModule = new PerfilRestaurantModule();
  });

  it('should create an instance', () => {
    expect(perfilRestaurantModule).toBeTruthy();
  });
});
