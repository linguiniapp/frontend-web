import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-eliminar-categoria',
  templateUrl: './eliminar-categoria.component.html',
  styleUrls: ['./eliminar-categoria.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EliminarCategoriaComponent{

  complete : boolean = false;

  constructor(private routes: Router) { }
  volver(){
    this.routes.navigate(['/verCategorias']);
  }
  volverMenu(){
    this.routes.navigate(['/']);
  }
  cerrar(){
    this.routes.navigate(['/verCategorias']);
  }

}
