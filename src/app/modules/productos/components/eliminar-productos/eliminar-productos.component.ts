import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-eliminar-productos',
  templateUrl: './eliminar-productos.component.html',
  styleUrls: ['./eliminar-productos.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class EliminarProductosComponent {

  complete : boolean = false;

  constructor(private routes: Router) { }
  volver(){
    this.routes.navigate(['/verProductos']);
  }
  volverMenu(){
    this.routes.navigate(['/']);
  }
  cerrar(){
    this.routes.navigate(['/verProductos']);
  }
 
}
