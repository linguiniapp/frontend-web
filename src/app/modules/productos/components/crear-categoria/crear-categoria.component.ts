import { Component, OnInit } from '@angular/core';
import { NgForm, Validators, FormControl, FormGroup  } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductosService, ListaElement, categoria } from '../../services/productos.service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-crear-categoria',
  templateUrl: './crear-categoria.component.html',
  styleUrls: ['./crear-categoria.component.css'], 
  exportAs: 'ngModel'

})
export class CrearCategoriaComponent implements OnInit {
  complete: boolean = false;
  dataSource: categoria[]=[];
  nombre: string;
  imagen: string;


  errors: string[] = [];
  public file_srcs: string[] = [];
  public debug_size_before: string[] = [];
  public file_srcs1: string[] = [];

  public debug_size_after: string[] = [];


  crearCategoria = new FormGroup({
    nombre: new FormControl('', [
      Validators.required,

    ]),
    foto: new FormControl('', [
      Validators.required,
    ]),
  });



  constructor(private ar: ActivatedRoute, private routes:Router, private cat: ProductosService, private changeDetectorRef: ChangeDetectorRef, private service: ProductosService, private prod: ProductosService) { this.imagen = ''; }
  

  ngOnInit() {
    //this.cat.getCategoria().subscribe( data => this.dataSource = data);

  }

  onSubmit(f: NgForm){
    if (f.valid) {
    this.nombre=f.value.nombre;
    this.cat.agregarCategoria(this.nombre, this.imagen);
    this.routes.navigate(['/verCategorias'])
  }
  else {

    this.complete = !this.complete;
  }

  }

  cerrar(){
    this.routes.navigate(['/verCategorias']);
  }

  volver(){
    this.routes.navigate(['/verCategorias']);
  }

  close(){
    this.complete = !this.complete;
  }


  //para foto

  fileChange(input) {
    this.readFiles(input.files);
  }

  resize(img, MAX_WIDTH: number, MAX_HEIGHT: number, callback) {
    // This will wait until the img is loaded before calling this function
    return img.onload = () => {

      // Get the images current width and height
      // tslint:disable-next-line:no-var-keyword
      var width = img.width;
      // tslint:disable-next-line:no-var-keyword
      var height = img.height;
      // Set the WxH to fit the Max values (but maintain proportions)
      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      // create a canvas object
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line:no-var-keyword
      // tslint:disable-next-line:prefer-const
      let canvas = document.createElement("canvas");
      // Set the canvas to the new calculated dimensions
      canvas.width = width;
      canvas.height = height;
      const ctx = canvas.getContext("2d");

      ctx.drawImage(img, 0, 0, width, height);

      // Get this encoded as a jpeg
      // IMPORTANT: 'jpeg' NOT 'jpg'
      // tslint:disable-next-line:prefer-const
      let dataUrl = canvas.toDataURL("image/jpeg");

      // callback with the results
      callback(dataUrl, img.src.length, dataUrl.length);
    };
  }



  readFile(file, reader, callback) {
    reader.onload = () => {
      callback(reader.result);
      this.imagen = reader.result;
      // alert("fotooooo: " +this.imagen);

      console.log(reader.result);
    };
    reader.readAsDataURL(file);
  }



  readFiles(files, index = 0) {
    // Create the file reader
    const reader = new FileReader();

    // If there is a file
    if (index in files) {
      // Start reading this file
      this.readFile(files[index], reader, (result) => {
        // Create an img element and add the image file data to it
        const img = document.createElement("img");
        img.src = result;

        // Send this img to the resize function (and wait for callback)
        this.resize(img, 50, 50, (resized_jpeg, before, after) => {
          // For debugging (size in bytes before and after)
          this.debug_size_before.push(before);
          this.debug_size_after.push(after);

          // Add the resized jpeg img source to a list for preview
          // This is also the file you want to upload. (either as a
          // base64 string or img.src = resized_jpeg if you prefer a file).
          this.file_srcs1.push(resized_jpeg);

          // Read the next file;
          //  this.readFiles(files, index+1);
        });
      });
    } else {
      // When all files are done This forces a change detection
      this.changeDetectorRef.detectChanges();
    }
  }

}
