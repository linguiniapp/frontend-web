import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductosService, ListaElement, categoria } from '../../services/productos.service';
import { ChangeDetectorRef } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';


@Component({
  selector: 'app-editar-productos',
  templateUrl: './editar-productos.component.html',
  styleUrls: ['./editar-productos.component.css'],
  exportAs: 'ngModel'
})
export class EditarProductosComponent  {
  nombre: string;
  errors: string[] = [];
  public file_srcs: string[] = [];
  public debug_size_before: string[] = [];
  public file_srcs1: string[] = [];

  public debug_size_after: string[] = [];


  producto: ListaElement={codigo: null,nombre: "",categoria:"", codigoCat: null, foto:"",descripcion: ""};
  categoria = [];
  dataSource: categoria[]=[];
  
  

  constructor(private ar:ActivatedRoute,private changeDetectorRef: ChangeDetectorRef, private sanitizer: DomSanitizer, private routes:Router, private prod: ProductosService) { 
    this.ar.params.subscribe( params => {
      this.prod.getElemento( params['id']).subscribe( p => this.producto=p);
    });
    this.prod.getCategoria().subscribe( data => this.dataSource = data);
  }


  onSubmit(f: NgForm){       
    this.producto.nombre=f.value.nombre;
    let codigoCat= this.dataSource.find( data => data.nombre === f.value.cat);
    this.producto.codigoCat=codigoCat.codigo;
    this.prod.editarProducto(this.producto);   
    this.routes.navigate(['/verProductos'])
  }

  volver(){
    this.routes.navigate(['/verProductos']);
  }
  cerrar(){
    this.routes.navigate(['/verProductos']);
  }





  fileChange(input) {
    this.readFiles(input.files);
  }

  resize(img, MAX_WIDTH: number, MAX_HEIGHT: number, callback) {
    // This will wait until the img is loaded before calling this function
    return img.onload = () => {

      // Get the images current width and height
      // tslint:disable-next-line:no-var-keyword
      var width = img.width;
      // tslint:disable-next-line:no-var-keyword
      var height = img.height;
      // Set the WxH to fit the Max values (but maintain proportions)
      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      // create a canvas object
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line:no-var-keyword
      // tslint:disable-next-line:prefer-const
      let canvas = document.createElement("canvas");
      // Set the canvas to the new calculated dimensions
      canvas.width = width;
      canvas.height = height;
      const ctx = canvas.getContext("2d");

      ctx.drawImage(img, 0, 0, width, height);

      // Get this encoded as a jpeg
      // IMPORTANT: 'jpeg' NOT 'jpg'
      // tslint:disable-next-line:prefer-const
      let dataUrl = canvas.toDataURL("image/jpeg");

      // callback with the results
      callback(dataUrl, img.src.length, dataUrl.length);
    };
  }

  getImage(image) {
    return this.sanitizer.bypassSecurityTrustUrl(image);//_sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
}


  readFile(file, reader, callback) {
    reader.onload = () => {
      callback(reader.result);
      this.producto.foto = reader.result;
      // alert("fotooooo: " +this.imagen);

      console.log(reader.result);
    };
    reader.readAsDataURL(file);
  }



  readFiles(files, index = 0) {
    // Create the file reader
    const reader = new FileReader();

    // If there is a file
    if (index in files) {
      // Start reading this file
      this.readFile(files[index], reader, (result) => {
        // Create an img element and add the image file data to it
        const img = document.createElement("img");
        img.src = result;

        // Send this img to the resize function (and wait for callback)
        this.resize(img, 50, 50, (resized_jpeg, before, after) => {
          // For debugging (size in bytes before and after)
          this.debug_size_before.push(before);
          this.debug_size_after.push(after);

          // Add the resized jpeg img source to a list for preview
          // This is also the file you want to upload. (either as a
          // base64 string or img.src = resized_jpeg if you prefer a file).
          this.file_srcs1.push(resized_jpeg);

          // Read the next file;
          //  this.readFiles(files, index+1);
        });
      });
    } else {
      // When all files are done This forces a change detection
      this.changeDetectorRef.detectChanges();
    }
  }
}
