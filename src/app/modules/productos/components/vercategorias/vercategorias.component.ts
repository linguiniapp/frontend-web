import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductosService, ListaElement, categoria } from '../../services/productos.service';


@Component({
  selector: 'app-vercategorias',
  templateUrl: './vercategorias.component.html',
  styleUrls: ['./vercategorias.component.css']
})
export class VercategoriasComponent implements OnInit {

  dataSource: categoria[] = [];
  selectedProd: boolean;
  producto: number;
  suscripcion: any;


  constructor(private ar: ActivatedRoute, private routes: Router, private cat: ProductosService) { }


  ngOnInit() {
    this.suscripcion = this.cat.getCategoria().subscribe(data => this.dataSource = data);
  }
  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();
  }
  agregarCategoria() {
    this.routes.navigate(['/crearCategoria']);
  }
  quitarCategoria(codigo: number) {
    this.cat.quitarCategoria(codigo);
    this.routes.navigate(['/eliminarCategoria']);
  }
  editarCategoria(codigo: number) {
    this.routes.navigate(['/editarCategoria', codigo]);
  }

}
