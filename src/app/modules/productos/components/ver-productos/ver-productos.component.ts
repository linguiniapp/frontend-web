import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductosService, ListaElement } from '../../services/productos.service';
import { Observable } from 'rxjs'


@Component({
  selector: 'app-ver-productos',
  templateUrl: './ver-productos.component.html',
  styleUrls: ['./ver-productos.component.css']
})
export class VerProductosComponent implements OnInit {

  //displayColumns: string[]=['codigo','nombre','acciones','agregar'];
  dataSource: ListaElement[]=[];
  dataSourceDefault: ListaElement[]=[];

  selectedProd: boolean;
  producto: number;
  suscripcion: any;

  constructor(private ar:ActivatedRoute, private routes: Router, private prod: ProductosService) { }

  ngOnInit() {
    this.suscripcion = this.prod.getProducto().subscribe(data =>{
      this.dataSourceDefault = data;
       this.dataSource=data
      }  ); 

  }
  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();    
  }  
  agregarProducto(){
    this.routes.navigate(['/crearProductos']);
  }
  quitarProducto(codigo: number){
    this.prod.quitarProducto(codigo);
    this.routes.navigate(['/eliminarProductos']);
  }
  editarProducto(codigo: number){    
    this.routes.navigate(['/editarProductos',codigo]);
  }


  onSearchChange(busqueda : string ){

    this.dataSource = this.dataSourceDefault;

    this.dataSource = this.dataSource.filter( data => data.nombre.includes( busqueda));
        
    

  }

  onSearchCodChange(busqueda){

    this.dataSource = this.dataSourceDefault;

    this.dataSource = this.dataSource.filter( data => data.categoria.includes( busqueda));
        
    
    }
    
  }

