import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CrearProductosComponent } from './components/crear-productos/crear-productos.component';
import { EditarProductosComponent } from './components/editar-productos/editar-productos.component';
import { EliminarProductosComponent } from './components/eliminar-productos/eliminar-productos.component';
import { VerProductosComponent } from './components/ver-productos/ver-productos.component'

import { CrearCategoriaComponent } from './components/crear-categoria/crear-categoria.component';
import { EditarCategoriaComponent } from './components/editar-categoria/editar-categoria.component';
import { EliminarCategoriaComponent } from './components/eliminar-categoria/eliminar-categoria.component';
import { VercategoriasComponent } from './components/vercategorias/vercategorias.component';

import { ReactiveFormsModule} from '@angular/forms';
import { ClarityModule, ClrFormsNextModule, ClrDatagridModule, /*ClrDatagridPagination, ClrDatagridActionOverflow*/} from '@clr/angular';

import { ProductosRoutingModule } from './productos-routing.module'; 

@NgModule({
  imports: [
    CommonModule,
    ProductosRoutingModule,
    FormsModule,
    ClarityModule,
    ClrFormsNextModule,
    ReactiveFormsModule,
    ClarityModule,
    ClrFormsNextModule,
    ClrDatagridModule,
    
  ],
  declarations: [VerProductosComponent, EditarProductosComponent, EliminarProductosComponent, CrearProductosComponent, CrearCategoriaComponent, EditarCategoriaComponent, EliminarCategoriaComponent, VercategoriasComponent]
})
export class ProductosModule { }
