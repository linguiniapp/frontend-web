import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CrearProductosComponent } from './components/crear-productos/crear-productos.component';
import { EliminarProductosComponent } from './components/eliminar-productos/eliminar-productos.component';
import { EditarProductosComponent } from './components/editar-productos/editar-productos.component';
import { VerProductosComponent } from './components/ver-productos/ver-productos.component';
import { CrearCategoriaComponent } from './components/crear-categoria/crear-categoria.component';
import { EditarCategoriaComponent } from './components/editar-categoria/editar-categoria.component';
import { EliminarCategoriaComponent } from './components/eliminar-categoria/eliminar-categoria.component';
import { VercategoriasComponent } from './components/vercategorias/vercategorias.component'; 
import { AuthService } from './/../../security/login/auth.service';
 
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';


const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'verProductos',
                component: VerProductosComponent, canActivate: [AuthService],
                data: {acciones: ["ver_productos"]}
            },
            {
                path: 'editarProductos/:id',
                component: EditarProductosComponent, canActivate: [AuthService],
                data: {acciones: ["modificar_producto"]}
            },
            {
                path: 'eliminarProductos',
                component: EliminarProductosComponent, canActivate: [AuthService],
                data: {acciones: ["deshabilitar_producto"]}
            },
            {
                path: 'crearProductos',
                component: CrearProductosComponent, canActivate: [AuthService],
                data: {acciones: ["crear_producto"]}
            },
            {
                path: 'crearCategoria',
                component: CrearCategoriaComponent, canActivate: [AuthService],
                data: {acciones: ["crear_categoria"]}
            },
            {
                path: 'editarCategoria/:id',
                component: EditarCategoriaComponent, canActivate: [AuthService],
                data: {acciones: ["modificar_categoria"]}
            },
            {
                path: 'eliminarCategoria',
                component: EliminarCategoriaComponent, canActivate: [AuthService],
                data: {acciones: ["eliminar_categoria"]}
            },
            {
                path: 'verCategorias',
                component: VercategoriasComponent, canActivate: [AuthService],
                data: {acciones: ["ver_categorias"]}
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductosRoutingModule { }
