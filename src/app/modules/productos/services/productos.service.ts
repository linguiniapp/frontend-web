import { Injectable } from '@angular/core';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Observable } from 'rxjs';
import { List } from 'lodash';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class ProductosService {

    producto: verProductos[] = [];
    listaProductos: ListaElement[] = [];
    listaCaract: categoria[] = [];
    private ELEMENT_DATA = new Subject<ListaElement[]>();
    private lelem = new Subject<ListaElement>();
    private ELEMENT_DATAcat = new Subject<categoria[]>();
    private ELEMENT_DATAcatBis = new Subject<categoria[]>();
    private lelemCategoria = new Subject<categoria>();

    constructor(private apollo: Apollo, private router: Router) {
        console.log("Ready to use");
    }

    getProducto(): Observable<ListaElement[]> {
        this.apollo.watchQuery<any>({
            query: gql
                `{
                verProductos{
                    edges{
                      node{
                        codigo,
                        nombre,
                        descripcion,
                        estado {
                          nombre
                        },
                        categorias {
                          edges {
                            node {
                              nombre
                            }
                          }
                        }
                        
                      }
                    }
                  }
                }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({ data }) => {
                let ELEMENT_DATA$: ListaElement[] = [];
                this.listaProductos = [];
                data.verProductos.edges.forEach(p => {
                    let categorias = "";
                    p.node.categorias.edges.forEach(element => {
                        categorias = element.node.nombre + ";" + categorias;
                    });
                    if (p.node.estado.nombre == "disponible") {
                        let elemento: ListaElement = {
                            codigo: p.node.codigo,
                            nombre: p.node.nombre,
                            descripcion: p.node.descripcion,
                            categoria: categorias,
                            codigoCat: null,
                            foto: ""
                        };

                        ELEMENT_DATA$.push(elemento);
                        this.ELEMENT_DATA.next(ELEMENT_DATA$);
                    }
                });

            });
        return this.ELEMENT_DATA.asObservable();
    }


    getProductoCategoria(idx): Observable<ListaElement[]> {
        this.apollo.query<any>({
            query: gql
                `{
                verProductos(categorias: [${idx}]) {
                  edges {
                    node {
                      nombre
                      codigo
                      estado{
                          nombre
                      }
                    }
                  }
                }
              }`, fetchPolicy: 'no-cache'
        })
            .subscribe(({ data }) => {
                let ELEMENT_DATA$: ListaElement[] = [];

                this.listaProductos = [];
                data.verProductos.edges.forEach(p => {

                    if (p.node.estado.nombre == "disponible") {
                        let elemento: ListaElement = {
                            codigo: p.node.codigo,
                            nombre: p.node.nombre,
                            categoria: "",
                            codigoCat: null,
                            foto: "",
                            descripcion: ""
                        };
                        ELEMENT_DATA$.push(elemento);
                    }
                });
                this.ELEMENT_DATA.next(ELEMENT_DATA$);

            });
        return this.ELEMENT_DATA.asObservable();
    }

    getElemento(codigo: number) {
        this.apollo.watchQuery<any>({
            query: gql
                `{
                verProducto(codigoProducto:${codigo}){
                  codigo
                  nombre
                  foto
                  categorias{
                    edges{
                      node{
                        nombre
                        codigo
                      }
                    }
                  }
                  
                }
              }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({ data }) => {

                let lelem: ListaElement = {
                    codigo: data.verProducto.codigo,
                    nombre: data.verProducto.nombre,
                    descripcion: data.verProducto.descripcion,
                    categoria: data.verProducto.categorias.edges[0].node.nombre,
                    codigoCat: data.verProducto.categorias.edges[0].node.codigo,
                    foto: data.verProducto.foto

                };

                this.lelem.next(lelem);
            });
        return this.lelem.asObservable();
    }

    agregarProducto(nombre, categoria, foto, descripcion) {
        this.apollo
            .mutate({
                mutation: gql
                    `mutation{
                crearProducto(nombre: "${nombre}", numerosCategorias: ${categoria},foto: "${foto}", descripcion: "${descripcion}"){
                    ok
                }
            }`,
            })
            .subscribe(({ data }) => {
                if (data.crearProducto.ok == true) {
                    this.getProducto();
                }
            }, (error) => {
                console.log('error', error);
            });
        this.router.navigateByUrl('/verProductos');
    }
    quitarProducto(codigo) {
        this.apollo
            .mutate({
                mutation: gql
                    `
        mutation{
            deshabilitarProducto(codigo: ${codigo}){
              ok
            }
          } `,
            }).subscribe(({ data }) => {
                this.getProducto();
            },
                (error) => {
                    alert("error: " + error);

                }

            );
    }
    editarProducto(producto: ListaElement) {
        this.apollo
            .mutate({
                mutation: gql
                    `mutation{
                modificarProducto(codigo:${producto.codigo}, nombre:"${producto.nombre}", numerosCategorias:[${producto.codigoCat}], descripcion:"${producto.descripcion}"){
                  ok
                  producto{
                    codigo
                    nombre
                  }
                }
              }`, fetchPolicy: 'no-cache',
            })
            .subscribe();
    }



    getCategoria(): Observable<categoria[]> {

        this.apollo.watchQuery<any>({
            query: gql
                `{verCategorias{
                  edges{
                    node{
                      nombre
                      codigo
                    }
                  }
                }  
                }`, fetchPolicy: 'no-cache'
        }).valueChanges

            .subscribe(({ data }) => {
                let ELEMENT_DATAcat$: categoria[] = [];
                this.listaCaract = [];
                data.verCategorias.edges.forEach(p => {
                    let lelem: categoria = {
                        codigo: p.node.codigo,
                        nombre: p.node.nombre,
                        foto: ""
                    };
                    ELEMENT_DATAcat$ = [...ELEMENT_DATAcat$, lelem];
                    //  ELEMENT_DATAcat$.push(lelem);

                });
                this.ELEMENT_DATAcat.next(ELEMENT_DATAcat$);
                console.log(ELEMENT_DATAcat$)

            });
        return this.ELEMENT_DATAcat.asObservable();
    }

    //es la misma de la de arriba se utiliza por el tema de la subscripcion 
    getCategoriaBis(): Observable<categoria[]> {

        this.apollo.watchQuery<any>({
            query: gql
                `{verCategorias{
                  edges{
                    node{
                      nombre
                      codigo
                    }
                  }
                }  
                }`, fetchPolicy: 'no-cache'
        })
            .valueChanges
            .subscribe(({ data }) => {
                let ELEMENT_DATAcat$: categoria[] = [];
                this.listaCaract = [];
                data.verCategorias.edges.forEach(p => {
                    let lelem: categoria = {
                        codigo: p.node.codigo,
                        nombre: p.node.nombre,
                        foto: ""
                    };
                    ELEMENT_DATAcat$.push(lelem);

                });
                this.ELEMENT_DATAcatBis.next(ELEMENT_DATAcat$);
                console.log(ELEMENT_DATAcat$)
            });
        return this.ELEMENT_DATAcatBis.asObservable();
    }


    agregarCategoria(nombre, foto) {
        this.apollo
            .mutate({
                mutation: gql
                    `mutation{
                crearCategoria(nombre: "${nombre}", foto: "${foto}"){
                  ok
                }
              }
            `,
            })
            .subscribe(({ data }) => {
            }, (error) => {
                console.log('error', error);
            });
    };


    getElementoCategoria(codigo: number) {
        this.apollo.watchQuery<any>({
            query: gql
                `{                
                    verCategoria(codCategoria: ${codigo}){
                      nombre
                      codigo
                      foto
                    }                  
              }`
        })
            .valueChanges
            .subscribe(({ data }) => {

                let lelem: categoria = {
                    codigo: data.verCategoria.codigo,
                    nombre: data.verCategoria.nombre,
                    foto: data.verCategoria.foto,
                };
                this.lelemCategoria.next(lelem);
            });
        return this.lelemCategoria.asObservable();
    }
    quitarCategoria(codigo) {
        this.apollo
            .mutate({
                mutation: gql
                    `mutation{
            eliminarCategoria(codigo: ${codigo}){
              ok
            }
          }`,
            }).subscribe(({ data }) => {
                this.getCategoria();

            },
                (error) => {
                    alert("error: " + error);

                }

            );

    }
    editarCategoria(cat: categoria) {
        this.apollo
            .mutate({
                mutation: gql
                    `mutation{
                modificarCategoria(codigo:${cat.codigo}, nombre:"${cat.nombre}", foto: "${cat.foto}"){
                  ok
                  categoria{
                    nombre
                    codigo
                  }
                }
              }
            `,
            })
            .subscribe();
    }

    // fin categorías
}


export interface ListaElement {
    codigo: number;
    nombre: string;
    categoria: string;
    codigoCat: number;
    foto: "";
    descripcion: string;

}

export interface SelectedProd {
    codigo: number;
    nombre: string;
    categoria: string;
    codigoCat: number;
    cantidad: number;
}

export interface SelectedCarta {
    codigo: number;
    nombre: string;
    categoria: string;
    codigoCat: number;
    precio: number;
}
export interface SelectedRecompensa {
    codigo: number;
    nombre: string;
    categoria: string;
    codigoCat: number;
    puntos: number;
    cantidad: number;
}

export interface SelectedProducto {
    codigo: number;
    nombre: string;
    cantidad: number;
}



export interface verProductos {
    codigo: number;
    nombre: string;
    estado: string;

}

export interface categoria {
    codigo: number;
    nombre: string;
    foto: "";
}