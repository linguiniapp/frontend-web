import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuxviewComponent } from '../../core/aux/components/auxview/auxview.component';
import { VentasComponent } from './components/ventas/ventas.component';
import { ImagenesComponent } from './components/imagenes/imagenes.component';
import {CajaReporteComponent} from './components/caja-reporte/caja-reporte.component';
import {MozoReporteComponent} from './components/mozo-reporte/mozo-reporte.component';
import {VentasReporteComponent} from './components/ventas-reporte/ventas-reporte.component';
import {ProductosReporteComponent} from './components/productos-reporte/productos-reporte.component';
import {CalificacionesReporteComponent} from './components/calificaciones-reporte/calificaciones-reporte.component';
const routes: Routes = [
  {
      path: '',
      component: AuxviewComponent,
      children: [
          {
              path: 'reportes/caja/sucursal/:idSucursal',
              component: CajaReporteComponent,
              data: {
                  breadcrumb: 'Reporte de caja'
              }
          },
          {
              path: 'reportes/mozo/sucursal/:nroSucursal',
              component: MozoReporteComponent,
              data: {
                  breadcrumb: 'Reporte Mozo'
              }
          },
          {
              path: 'reportes/ventas/sucursal/:nroSucursal',
              component: VentasReporteComponent,
              data: {
                  breadcrumb: 'Reporte Ventas'
              }
          },
          {
              path: 'reportes/productos/sucursal/:nroSucursal',
              component: ProductosReporteComponent,
              data: {
                  breadcrumb: 'Reporte de Productos'
              }
          },
          {
              path: 'reportes/calificaciones/sucursal/:nroSucursal',
              component: CalificacionesReporteComponent,
              data: {
                  breadcrumb: 'Reporte de Calificaciones'
              }
          },
          {
              path: 'Ventas',
              component: VentasComponent
          },
        {
          path: 'reportesVentas',
          component: ImagenesComponent
        }]
        }
      ];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }
