import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
//import { AfterViewInit } from "@angular/core";
import { ProductosService, categoria, ListaElement, SelectedCarta } from '../../../productos/services/productos.service';
import { VerCartaService, DetalleCartaProductoInput } from '../../../carta/services/ver-carta.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-imagenes',
  templateUrl: './imagenes.component.html',
  styleUrls: ['./imagenes.component.css']
})
export class ImagenesComponent implements OnInit {

  graficoBarra: Chart;
  graficoLineaReviews: Chart;
  graficoTorta: Chart;
  listaCategoria: categoria[] = [];
  selectedProd: SelectedCarta[] = [];
  observableCategoria: any = null;
  arrayProductos: any[] = [];
  codCategoria: string;
  numeroSucursal: number = 1;
  select: SelectedCarta[] = [];

  constructor(private serviceCarta: VerCartaService, private routes: Router, private activateRoute: ActivatedRoute, private serviceProducto: ProductosService) { }


  ngOnInit() {
    this.selectedProd = [];
    this.observableCategoria = this.serviceProducto.getCategoria();
    this.observableCategoria.subscribe(data => {
      this.listaCategoria = data;

    });
    this.serviceProducto.getProductoCategoria(this.codCategoria).subscribe(data => this.arrayProductos = data);

  }

  volver(){
    this.routes.navigate(['/Ventas']);
  }
  menu(){
    this.routes.navigate(['/admin-panel']);
  }

  categoriaChange() {
    console.log(this.codCategoria);
    this.metodoquegeneraelchart(this.codCategoria);


  }

  ngAfterViewInit() {
    this.graficoTorta = new Chart('pieChart', {
      type: 'pie',
      data: {
          labels: ["Sucursal Lingüini Uno","Sucursal Lingüini Dos"],
          datasets: [{
              label: 'Cantidad de ventas entre sucursales',
              data: [43, 65],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)'
              ],
              borderWidth: 1
          }]
      },
      
  });
    
    }
    metodoquegeneraelchart(cod: string){
      switch (cod){
        case "Carnes":
          this.graficoBarra ? this.graficoBarra.destroy() : console.log('No hay ninguno')
          this.graficoBarra = new Chart('barChart', {
            type: 'bar',
            data: {
                labels: ["Bifes de chorizo", "Milanesas", "Lomo de cerdo", "Parrillada", "Lomo a la mostaza", "Vacío de ternera"],
                datasets: [{
                    label: 'Cantidad',
                    data: [10, 19, 22, 25, 13, 15],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });break;
        case "Vinos":
        this.graficoBarra ? this.graficoBarra.destroy() : console.log('No hay ninguno')
          this.graficoBarra = new Chart('barChart', {
            type: 'bar',
            data: {
                labels: ["Malbec", "Cabernet Sauvignon", "Pinot Noir", "Merlot", "Sangiovese", "Moscato", "Chardonnay"],
                datasets: [{
                    label: 'Cantidad',
                    data: [30, 15, 9, 24, 2, 5, 10],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 168, 46, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 168, 46, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });break;
        case "Postres":
        this.graficoBarra ? this.graficoBarra.destroy() : console.log('No hay ninguno')
          this.graficoBarra = new Chart('barChart', {
            type: 'bar',
            data: {
                labels: ["Flan", "Tarta de queso", "Mousse de chocolate", "Durazno con crema", "Tarta helada de chocolate", "Ensalada de frutas"],
                datasets: [{
                    label: 'Cantidad',
                    data: [15, 12, 8, 12, 20, 10],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });break;
        

        default:
        this.graficoBarra ? this.graficoBarra.destroy() : console.log('No hay ninguno')
        this.graficoBarra = new Chart('barChart', {
          type: 'bar',
          data: {
              labels: ["", ""],
              datasets: [{
                  label: 'Cantidad',
                  data: [0, 0],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                  ],
                  borderColor: [
                      'rgba(255,99,132,1)',
                      'rgba(54, 162, 235, 1)',
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero:true
                      }
                  }]
              }
          }
      });
        
    }
  
  

}

}