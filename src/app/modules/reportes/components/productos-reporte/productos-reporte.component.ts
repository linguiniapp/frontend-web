import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import {ReportesService, ProductoReporte} from '../../services/reportes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { ValidadorFechas } from '../caja-reporte/caja-reporte.component';
@Component({
  selector: 'app-productos-reporte',
  templateUrl: './productos-reporte.component.html',
  styleUrls: ['./productos-reporte.component.css']
})
export class ProductosReporteComponent implements OnInit, OnDestroy {
  private obs1: any;
  busqueda: boolean;
  productos: ProductoReporte[];
  totalFacturado: number;
  reporteGroup = new FormGroup({
    fechaDesde: new FormControl('', [Validators.required]),
    fechaHasta: new FormControl('', [Validators.required])
  }, { validators: ValidadorFechas});
  constructor(private _reporteService: ReportesService, private ruta: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }
  ngOnDestroy(): void {
    if (this.obs1) {
      this.obs1.unsubscribe();
    }
  }
  enviarQuery() {
    const nroSucursal = this.ruta.snapshot.params.nroSucursal;
    const fechaDesde = formatDate(this.reporteGroup.value.fechaDesde, 'yyyy-MM-dd', 'en-US');
    const fechaHasta = formatDate(this.reporteGroup.value.fechaHasta, 'yyyy-MM-dd', 'en-US');
    this.obs1 = this._reporteService.getReporteProductos(nroSucursal, fechaDesde, fechaHasta).subscribe( data => {
      this.totalFacturado = 0;
      this.productos = data;
      this.productos.forEach( v => {
        this.totalFacturado += v.montoVentas;
      });
      this.busqueda = true;
    });
  }
  habilitarBusqueda() {
    this.busqueda = false;
  }
  volverPanel() {
    this.router.navigate(['/admin-panel']);
  }
}
