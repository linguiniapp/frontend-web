import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductosReporteComponent } from './productos-reporte.component';

describe('ProductosReporteComponent', () => {
  let component: ProductosReporteComponent;
  let fixture: ComponentFixture<ProductosReporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductosReporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductosReporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
