import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';
import { TimeFormatterPipe } from 'ngx-material-timepicker/src/app/material-timepicker/pipes/time-formatter.pipe';
import { Router } from '@angular/router';




@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css'],
  exportAs: 'ngModel'
})
export class VentasComponent implements OnInit {

  

  ventas= new FormGroup({
    date1: new FormControl('', [
      Validators.required,
    ]),
    date2: new FormControl('', [
      Validators.required,
    ]),
  });

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm){
    let fecha1=formatDate(f.value.date1, "yyyy-MM-dd", "en-US");
    let fecha2=formatDate(f.value.date2, "yyyy-MM-dd", "en-US");
    
  }
  verificarFecha(fecha1, fecha2){
    if(fecha2 >= fecha1){
      alert("fechas correctas");
    }
    else{
      //alert("error");
    }
  }

  obtenerReportes(){
    this.router.navigate(['/reportesVentas']);
                

  }
}
