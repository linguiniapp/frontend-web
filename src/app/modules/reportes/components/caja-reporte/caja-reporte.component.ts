import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {ReportesService} from '../../services/reportes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {formatDate} from '@angular/common';
import { CajaReporte} from '../../services/reportes.service';

@Component({
  selector: 'app-caja-reporte',
  templateUrl: './caja-reporte.component.html',
  styleUrls: ['./caja-reporte.component.css']
})
export class CajaReporteComponent implements OnInit {
    busqueda: boolean;
    cajaReporte: CajaReporte[];
    reporteGroup = new FormGroup({
      fechaDesde: new FormControl('', [Validators.required]),
      fechaHasta: new FormControl('', [Validators.required])
    }, { validators: ValidadorFechas});
    totalMontoEstimado = 0;
    totalMontoReal = 0;
    diferencia: number;

  constructor(private _reporteService: ReportesService, private ruta: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  enviarQuery() {

    const nroSucursal = this.ruta.snapshot.params.idSucursal;
    const fechaDesde = formatDate(this.reporteGroup.value.fechaDesde, 'yyyy-MM-dd', 'en-US');
    const fechaHasta = formatDate(this.reporteGroup.value.fechaHasta, 'yyyy-MM-dd', 'en-US');
    this._reporteService.getReporteCaja(nroSucursal, fechaDesde, fechaHasta).subscribe( data => {
      this.cajaReporte = data;
      this.busqueda = true;
      this.totalMontoReal = 0; this.totalMontoEstimado = 0;
      this.cajaReporte.forEach( r => {
      this.totalMontoEstimado += r.montoFinalEstimado;
      this.totalMontoReal += r.montoFinalReal;
      });
      this.diferencia = this.totalMontoReal - this.totalMontoEstimado ;
    });



  }

  habilitarBusqueda() {
    this.busqueda = false;
  }
  volverPanel() {
    this.router.navigate(['/admin-panel']);
  }
}
export function ValidadorFechas(control: AbstractControl) {
  if (control.get('fechaDesde').value !== '' && control.get('fechaHasta').value !== '') {
    const fechaDesde = new Date(control.get('fechaDesde').value).getTime();
    const fechaHasta = new Date(control.get('fechaHasta').value).getTime();
    if (fechaDesde < fechaHasta) {
      return null;
    } else {
      control.setErrors({rangoFechas: true});
      return {rangoFechas: true};
    }
  } else {
    return null;
  }
}
