import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaReporteComponent } from './caja-reporte.component';

describe('CajaReporteComponent', () => {
  let component: CajaReporteComponent;
  let fixture: ComponentFixture<CajaReporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajaReporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaReporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
