import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { ReportesService } from '../../services/reportes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { MozoReporte} from '../../services/reportes.service';
import { ValidadorFechas } from '../caja-reporte/caja-reporte.component';
@Component({
  selector: 'app-mozo-reporte',
  templateUrl: './mozo-reporte.component.html',
  styleUrls: ['./mozo-reporte.component.css']
})
export class MozoReporteComponent implements OnInit, OnDestroy {
  constructor(private _reporteService: ReportesService, private ruta: ActivatedRoute, private router: Router) { }
  busqueda: boolean;
  mozoReporte: MozoReporte[];
  obs1: any;
  reporteGroup = new FormGroup({
    fechaDesde: new FormControl('', [Validators.required]),
    fechaHasta: new FormControl('', [Validators.required])
  }, { validators: ValidadorFechas});
  ngOnInit() {
  }
  ngOnDestroy(): void {

    this.obs1 ? this.obs1.unsubscribe() : console.log('No hay nada');
  }
  enviarQuery() {
    const nroSucursal = this.ruta.snapshot.params.nroSucursal;
    const fechaDesde = formatDate(this.reporteGroup.value.fechaDesde, 'yyyy-MM-dd', 'en-US');
    const fechaHasta = formatDate(this.reporteGroup.value.fechaHasta, 'yyyy-MM-dd', 'en-US');
    this.obs1 = this._reporteService.getReporteMozosSucursal(nroSucursal, fechaDesde, fechaHasta).subscribe( data => {
      this.mozoReporte = data;
      this.busqueda = true;
    });
  }
  habilitarBusqueda() {
    this.busqueda = false;
  }
  volverPanel() {
    this.router.navigate(['/admin-panel']);
  }
}
