import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MozoReporteComponent } from './mozo-reporte.component';

describe('MozoReporteComponent', () => {
  let component: MozoReporteComponent;
  let fixture: ComponentFixture<MozoReporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MozoReporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MozoReporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
