import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalificacionesReporteComponent } from './calificaciones-reporte.component';

describe('CalificacionesReporteComponent', () => {
  let component: CalificacionesReporteComponent;
  let fixture: ComponentFixture<CalificacionesReporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalificacionesReporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalificacionesReporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
