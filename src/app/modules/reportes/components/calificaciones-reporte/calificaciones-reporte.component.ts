import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import {ReportesService, CalificacionReporte, DTOCalificaciones} from '../../services/reportes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { ValidadorFechas } from '../caja-reporte/caja-reporte.component';
@Component({
  selector: 'app-calificaciones-reporte',
  templateUrl: './calificaciones-reporte.component.html',
  styleUrls: ['./calificaciones-reporte.component.css']
})
export class CalificacionesReporteComponent implements OnInit, OnDestroy {
  private obs1: any;
  busqueda: boolean;
  calificaciones: CalificacionReporte[];
  promedio: number;
  promedioCalificaciones: number;
  reporteGroup = new FormGroup({
    fechaDesde: new FormControl('', [Validators.required]),
    fechaHasta: new FormControl('', [Validators.required])
  }, { validators: ValidadorFechas});

  constructor(private _reporteService: ReportesService, private ruta: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }
  ngOnDestroy(): void {
  }
  enviarQuery() {
    const nroSucursal = this.ruta.snapshot.params.nroSucursal;
    const fechaDesde = formatDate(this.reporteGroup.value.fechaDesde, 'yyyy-MM-dd', 'en-US');
    const fechaHasta = formatDate(this.reporteGroup.value.fechaHasta, 'yyyy-MM-dd', 'en-US');
    this.obs1 = this._reporteService.getReporteCalificaciones(nroSucursal, fechaDesde, fechaHasta).subscribe( data => {
      this.promedio = data.promedio;
      this.calificaciones = data.calificaciones;
      this.busqueda = true;
    });
  }
  habilitarBusqueda() {
    this.busqueda = false;
  }

  volverPanel() {
    this.router.navigate(['/admin-panel']);
  }
}
