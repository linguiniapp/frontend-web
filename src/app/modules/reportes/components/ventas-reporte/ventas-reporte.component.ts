import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import {ReportesService, VentaReporte} from '../../services/reportes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { ValidadorFechas } from '../caja-reporte/caja-reporte.component';
@Component({
  selector: 'app-ventas-reporte',
  templateUrl: './ventas-reporte.component.html',
  styleUrls: ['./ventas-reporte.component.css']
})
export class VentasReporteComponent implements OnInit, OnDestroy {

  constructor(private _reporteService: ReportesService, private ruta: ActivatedRoute, private router: Router) { }
  busqueda: boolean;
  obs1: any;
  ventas: VentaReporte[];
  totalFacturado: number;
  reporteGroup = new FormGroup({
    fechaDesde: new FormControl('', [Validators.required]),
    fechaHasta: new FormControl('', [Validators.required])
  }, { validators: ValidadorFechas});

  ngOnInit() {
  }
  ngOnDestroy(): void {
    if (this.obs1) {
      this.obs1.unsubscribe();
    }
  }
  enviarQuery() {
    const nroSucursal = this.ruta.snapshot.params.nroSucursal;
    const fechaDesde = formatDate(this.reporteGroup.value.fechaDesde, 'yyyy-MM-dd', 'en-US');
    const fechaHasta = formatDate(this.reporteGroup.value.fechaHasta, 'yyyy-MM-dd', 'en-US');
    this.obs1 = this._reporteService.getReporteVenta(nroSucursal, fechaDesde, fechaHasta).subscribe( data => {
      this.totalFacturado = 0;
      this.ventas = data;
      this.ventas.forEach( v => {
        this.totalFacturado += v.montoFacturado;
      });
      this.busqueda = true;
    });
  }
  habilitarBusqueda() {
    this.busqueda = false;
  }
  volverPanel() {
    this.router.navigate(['/admin-panel']);
  }
}
