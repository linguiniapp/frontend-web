import { Injectable } from '@angular/core';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ReportesService {
    constructor(private apollo: Apollo, private router: Router) {
    }

    dtoCaja = new Subject<CajaReporte[]>();
    dtoMozos = new Subject<MozoReporte[]>();
    dtoVentas = new Subject<VentaReporte[]>();
    dtoProductosVendidos = new Subject<ProductoReporte[]>();
    dtoCalificaciones = new Subject<DTOCalificaciones>();
    getReporteCaja(nroSucursal: number, fechaDesde: string, fechaHasta: string) {
        this.apollo.watchQuery<any>({
            query: gql`
            query{
              verReporteCaja(numeroSucursal: ${nroSucursal}, fechaDesde: "${fechaDesde}", fechaHasta: "${fechaHasta}"){
                fechaHora
                fechaDesde
                fechaHasta
                cajas{
                        edges{
                    node{
                      numero,
                      fechaHoraApertura,
                      fechaHoraCierre,
                      montoInicial,
                      montoFinalReal,
                      montoFinalEstimado,
                      comentarios
                    }
                  }
                }
              }
            }`, fetchPolicy: 'no-cache'

        }).valueChanges.subscribe(({data}) => {
            let arrayCajas = new Array<CajaReporte>();
            if (data.verReporteCaja.cajas.edges) {
                data.verReporteCaja.cajas.edges.forEach(caja => {
                    const cajita: CajaReporte = {
                        numero: caja.node.numero,
                        comentarios: caja.node.comentarios,
                        fechaHoraApertura: caja.node.fechaHoraApertura,
                        fechaHoraCierre: caja.node.fechaHoraCierre,
                        montoInicial: caja.node.montoInicial,
                        montoFinalEstimado: caja.node.montoFinalEstimado,
                        montoFinalReal: caja.node.montoFinalReal
                    };
                    arrayCajas.push(cajita);
                });
                this.dtoCaja.next(arrayCajas);
            }
        }, error1 => swal('ERROR', 'Ha ocurrido un error: ' + error1.message, 'error'));
        return this.dtoCaja.asObservable();
    }

    getReporteMozosSucursal(nroSucursal: number, fechaDesde: string, fechaHasta: string) {
        this.apollo.watchQuery<any>({
            query: gql`
            query{
              verReporteMozos(numeroSucursal: ${ nroSucursal }, fechaDesde: "${ fechaDesde }", fechaHasta: "${ fechaHasta }"){
                fechaHora
                fechaDesde
                fechaHasta
                mozos{
                  edges{
                    node{
                      empleado{
                        nombre,
                        apellido,
                        numero
                      }
                      cantidadPedidos,
                      cantidadProductos
                    }
                  }
                }
              }
            }`, fetchPolicy: 'no-cache'
        }).valueChanges.subscribe(({data}) => {
                let arrayMozo: MozoReporte[] = [];
                data.verReporteMozos.mozos.edges.forEach(fila => {
                    const mozo: MozoReporte = {
                        apellido: fila.node.empleado.nombre,
                        nombre: fila.node.empleado.nombre,
                        numero: fila.node.empleado.numero,
                        cantidadPedidos: fila.node.cantidadPedidos,
                        cantidadProductos: fila.node.cantidadProductos,
                        montoPedidos: 1000
                    };
                    arrayMozo.push(mozo);
                }, error1 => swal('ERROR', 'Ha ocurrido un error: ' + error1.message, 'error'));
                this.dtoMozos.next(arrayMozo);
            }
        );
        return this.dtoMozos.asObservable();
    }

    getReporteVenta(nroSucursal: number, fechaDesde: string, fechaHasta: string) {
        this.apollo.watchQuery<any>({
            query:
                gql`
                query{
                  verReporteVentas(numeroSucursal: ${nroSucursal}, fechaDesde: "${fechaDesde}", fechaHasta: "${fechaHasta}"){
                    fechaHora
                    fechaDesde
                    fechaHasta
                    ventas{
                      edges{
                        node{
                          fechaFactura
                          numeroFactura
                          numeroSucursal
                          nombreSucursal
                          montoFacturado
                        }
                      }
                    }
                  }
                }`,
            fetchPolicy: 'no-cache'
        }).valueChanges.subscribe(({data}) => {
            let arrayVentas: VentaReporte[] = [];
            data.verReporteVentas.ventas.edges.forEach(fila => {
                const dtoventa: VentaReporte = {
                    fechaFactura: fila.node.fechaFactura,
                    montoFacturado: fila.node.montoFacturado,
                    nombreSucursal: fila.node.nombreSucursal,
                    numeroFactura: fila.node.numeroFactura,
                    numeroSucursal: fila.node.numeroSucursal
                };
                arrayVentas.push(dtoventa);
            }, error1 => swal('ERROR', 'Ha ocurrido un error: ' + error1.message, 'error'));
            this.dtoVentas.next(arrayVentas);
        });
        return this.dtoVentas.asObservable();
    }
    getReporteProductos( nroSucursal: number, fechaDesde: string, fechaHasta: string) {
        this.apollo.watchQuery<any>({
            query: gql `
            query{
                  verReporteProductos(numeroSucursal: ${nroSucursal}, fechaDesde: "${fechaDesde}", fechaHasta: "${fechaHasta}"){
                    fechaHora
                    fechaDesde
                    fechaHasta
                    productos{
                      edges{
                        node{
                          codigo
                          nombre
                          precioUnitario
                          cantidadVendida
                          montoVentas
                        }
                      }
                    }
                  }
                }`
            , fetchPolicy: 'no-cache'
        }).valueChanges.subscribe(({data}) => {
            let arrayReporteProducto: ProductoReporte[] = [];
            data.verReporteProductos.productos.edges.forEach( p => {
                const fila: ProductoReporte = {
                   cantidadVendida: p.node.cantidadVendida,
                   codigo: p.node.codigo,
                   fechaVenta: p.node.fechaVenta,
                   montoVentas: p.node.montoVentas,
                   nombre: p.node.nombre,
                   precioUnitario: p.node.precioUnitario
                };
                arrayReporteProducto.push(fila);
            });
            this.dtoProductosVendidos.next(arrayReporteProducto);
        }, error1 => swal('ERROR', 'Ha ocurrido un error: ' + error1.message, 'error'));
        return this.dtoProductosVendidos.asObservable();
    }

    getReporteCalificaciones(nroSucursal: number, fechaDesde: string, fechaHasta: string): Observable<DTOCalificaciones> {
        this.apollo.watchQuery<any>({
            query: gql`           
            query{
              verReporteCalificaciones(numeroSucursal: ${nroSucursal}, fechaDesde: "${fechaDesde}", fechaHasta: "${fechaHasta}"){
                fechaHora
                fechaDesde
                fechaHasta
                calificaciones{
                  edges{
                    node{
                      calificacion
                      fechaHora
                      comentarios
                      cliente{
                        nombre
                        apellido
                      }
                      sucursal{
                        nombre
                      }
                    }
                  }
                }
                promedioSucursal
                detalleSucursal
              }
            }`, fetchPolicy: 'no-cache'
        }).valueChanges.subscribe(({data}) => {
            const promedio = data.verReporteCalificaciones.promedioSucursal;
            let arrayCalificaciones: CalificacionReporte[] = [];
            data.verReporteCalificaciones.calificaciones.edges.forEach(cali => {
                let fila: CalificacionReporte = {
                    apellidoCliente: cali.node.cliente.apellido,
                    nombreCliente: cali.node.cliente.nombre,
                    calificacion: cali.node.calificacion,
                    comentarios: cali.node.comentarios,
                    fechaHora: cali.node.fechaHora,
                    nombreSucursal: cali.node.sucursal.nombre
                };
                arrayCalificaciones.push(fila);
            });
            const dtoCalificaciones: DTOCalificaciones = {
                calificaciones: arrayCalificaciones,
                promedio: promedio
            };
            this.dtoCalificaciones.next(dtoCalificaciones);
        }, error1 => swal('ERROR', 'Ha ocurrido un error: ' + error1.message, 'error'));
        return this.dtoCalificaciones.asObservable();
    }
}
export class CajaReporte {
    numero: number;
    fechaHoraApertura: string;
    fechaHoraCierre: string;
    montoInicial: number;
    montoFinalEstimado: number;
    montoFinalReal: number;
    comentarios: string;
}
export class MozoReporte {
    nombre: string;
    apellido: string;
    numero: number;
    cantidadPedidos: number;
    cantidadProductos: number;
    montoPedidos: number;
}
export class VentaReporte {
    fechaFactura: string;
    numeroFactura: number;
    numeroSucursal: number;
    nombreSucursal: string;
    montoFacturado: number;
}
export class ProductoReporte {
    fechaVenta: string;
    codigo: number;
    nombre: string;
    precioUnitario: number;
    montoVentas: number;
    cantidadVendida: number
}

export class CalificacionReporte {
    fechaHora: string;
    calificacion: number;
    comentarios: string;
    nombreCliente: string;
    apellidoCliente: string;
    nombreSucursal: string
}
export class DTOCalificaciones {
    calificaciones: CalificacionReporte[];
    promedio: number;
}
