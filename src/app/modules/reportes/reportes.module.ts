import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportesRoutingModule } from './reportes-routing.module';
import { VentasComponent } from './components/ventas/ventas.component';
import { ReservasComponent } from './components/reservas/reservas.component';
import { ClarityModule, ClrFormsNextModule, ClrDatagridModule, ClrDatepickerModule} from '@clr/angular';
import { ImagenesComponent } from './components/imagenes/imagenes.component';
import { CajaReporteComponent } from './components/caja-reporte/caja-reporte.component';
import { MozoReporteComponent } from './components/mozo-reporte/mozo-reporte.component';
import { VentasReporteComponent } from './components/ventas-reporte/ventas-reporte.component';
import { ProductosReporteComponent } from './components/productos-reporte/productos-reporte.component';
import { CalificacionesReporteComponent } from './components/calificaciones-reporte/calificaciones-reporte.component';

@NgModule({
    imports: [
        CommonModule,
        ReportesRoutingModule,
        ClarityModule,
        ClrFormsNextModule,
        ClrDatagridModule,
        ClrDatepickerModule,
        FormsModule,
        ReactiveFormsModule

    ],
    declarations: [VentasComponent, ReservasComponent, ImagenesComponent, CajaReporteComponent, MozoReporteComponent, VentasReporteComponent, ProductosReporteComponent, CalificacionesReporteComponent]
})
export class ReportesModule { }
