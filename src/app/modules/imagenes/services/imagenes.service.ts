import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { Headers, RequestOptions, Response } from "@angular/http";
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ImagenesService {

  public url: string = "https://api.mercadopago.com/v1/card_tokens?public_key=TEST-4b5eb5b1-a882-4c18-ac0a-2d2c4c352ed8";

  constructor(private http: Http) { }

  protected getRestHeader(): RequestOptions {
    const headers = new Headers({
      "Content-Type": "application/json"
      // "Authorization": "KWT " + localStorage.getItem("token")
    });
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return options;
  }

  saveImage(foto: String): Promise<any> {


    return this.http
      .get("https://restcountries.eu/rest/v2/all")
      .toPromise()
      .then(response => {
        return 12345678;
      })
      .catch(
        error => {alert(error); console.log(error)}
      );



    //   return this.http
    //     .post(
    //       this.url,
    //       JSON.stringify(foto),
    //       this.getRestHeader()
    //     )
    //     .toPromise()
    //     .then(response => {

    //       alert("respondio")
    //       return response.json();
    //     })
    //     .catch();
    // }
  }


  apiMercadoPago(tarjeta: Tarjeta): Promise<any> {

    return this.http
      .post(
        this.url,
        JSON.stringify(tarjeta)
      )
      .toPromise()
      .then(response => {
        // alert("respondio")
        return response.json() as TarjetaResponse;
      })
      .catch(
        error => {console.log(error)}
      );
  }

  // {
  //     "id": "c1a16008791f7aa984c2b11b68addd6b",
  //     "public_key": "TEST-4b5eb5b1-a882-4c18-ac0a-2d2c4c352ed8",
  //     "first_six_digits": "450995",
  //     "expiration_month": 6,
  //     "expiration_year": 2019,
  //     "last_four_digits": "3704",
  //     "cardholder": {
  //         "identification": {
  //             "number": "12345678",
  //             "type": "DNI"
  //         },
  //         "name": "APRO"
  //     },
  //     "status": "active",
  //     "date_created": "2018-12-09T23:30:21.838-04:00",
  //     "date_last_updated": "2018-12-09T23:30:21.838-04:00",
  //     "date_due": "2018-12-17T23:30:21.838-04:00",
  //     "luhn_validation": true,
  //     "live_mode": false,
  //     "require_esc": false,
  //     "card_number_length": 16,
  //     "security_code_length": 3
  // }

  getImage(): Promise<string> {


    return this.http
      .get("https://restcountries.eu/rest/v2/all")
      .toPromise()
      .then(response => {
        return "12345678";
      })
      .catch();
  }

}

export interface Tarjeta {
  card_number: String;
  security_code: String;
  expiration_month: Number;
  expiration_year: Number;
  cardholder: cardholder
}

export interface TarjetaResponse { 
  id: String;
  public_key: String;
  first_six_digits: String;
  expiration_month: Number;
  expiration_year: Number;
  last_four_digits: Number;
  cardholder: cardholder;
  status: String;
  date_created: String;
  date_last_updated: String;
  date_due: String;
  luhn_validation: String;
  live_mode: String;
  require_esc: boolean;
  card_number_length: Number;
  security_code_length: Number;
}

export interface cardholder {

  name: String;
  identification: identification;
}


export interface identification {
  number: String;
  type: String;
}