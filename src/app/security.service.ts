import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Apollo, gql, } from 'apollo-angular-boost';
import { Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  private emailEnviado$ = new Subject<boolean>();
  private passwordCambiada$ = new Subject<boolean>();
  constructor(private apollo: Apollo, private router: Router) {}
  enviarMail(correo: string): Observable<boolean>{
    this.apollo
    .mutate({
      mutation: gql
      `mutation{
        recoverPassword(email: "${correo}"){
          ok
        }
      }`
    })
    .subscribe(({ data }) => {
      console.log(data);
      if (data.recoverPassword.ok == 0){
        this.emailEnviado$.next(true)
      }else{
        this.emailEnviado$.next(false);
      }
    });
    return this.emailEnviado$.asObservable();
  }
  setearNuevaPassword(token: string, password: string): Observable<boolean>{
    this.apollo
    .mutate({
      mutation: gql
      `  mutation{
        resetPassword(password: "${ password }", token: "${ token }"){
          ok
        }
      }`
    })
    .subscribe(({data}) => {
      if(data.resetPassword.ok == 0){
        this.passwordCambiada$.next(true);
      }else{
        this.passwordCambiada$.next(false);
      }
    })
    return this.passwordCambiada$.asObservable();
  }
}
