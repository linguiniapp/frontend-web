import { Component, OnInit } from '@angular/core';
import { RolesService, acciones } from '../app/modules/roles/services/roles.service';
import { AuthService } from './security/login/auth.service';

import { Apollo, gql } from 'apollo-angular-boost';
import { Observable } from 'rxjs';

import { Subject } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(private auth: AuthService) { }


    // listaAcciones = new Subject<acciones[]>();
    // acciones: acciones[] = []

    ngOnInit() {

    }

    //     this.getAccionesAsinc().subscribe(data => {
    //         this.acciones = data;
    //     })

    //     console.log("appcomponent")
    // }

    // getAcciones(): acciones[]{
    //     return this.acciones;
    // }
    // getAccionesAsinc(): Observable<acciones[]> {
    //     this.apollo.watchQuery<any>({
    //         query: gql
    //             `{
    //     verificarUsuario{
    //       persona{
    //         rol{
    //           acciones{
    //             edges{
    //               node{
    //                 nombre
    //                 codigo
    //               }
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }
    //     `, fetchPolicy: 'no-cache'
    //     })
    //         .valueChanges
    //         .subscribe(({ data }) => {
    //             this.acciones = [];
    //             // let ELEMENT_DATA$: ListaElement[] = [];
    //             data.verificarUsuario.persona.rol.acciones.edges.forEach(s => {
    //                 let lelem: acciones = {
    //                     nombre: s.node.nombre,
    //                     codigo: s.node.codigo
    //                 };

    //                 this.acciones.push(lelem);

    //             });


    //             this.listaAcciones.next(this.acciones);
    //         });

    //     return this.listaAcciones.asObservable();

    // }
}
