import { HttpClientModule } from "@angular/common/http";
import { ApolloBoostModule, ApolloBoost } from "apollo-angular-boost";
import { setContext } from 'apollo-link-context';
import { HttpHeaders } from '@angular/common/http';
import { Apollo } from 'apollo-angular';
import { HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
 // Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, RouterModule } from '@angular/router';

// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// App components
import { AppComponent } from './app.component';
import { ApolloClientModule } from './modules/apollo-client/apollo-client.module';
// App modules
import { ListaEsperaModule } from './modules/lista-espera/lista-espera.module';
import { ReservasModule } from './modules/reservas/reservas.module';
import { ReservasService } from './modules/reservas/services/reservas.service';
import { AbmClienteModule } from './modules/abm-cliente/abm-cliente.module';
import { CartaModule } from './modules/carta/carta.module';
import { SucursalesModule } from './modules/sucursales/sucursales.module';
import { CoreModule } from './core/core.module';
import { SecurityModule } from './security/security.module';
import { ProductosModule } from './modules/productos/productos.module';
import { PromocionesModule } from './modules/promociones/promociones.module';
import { PedidosModule } from './modules/pedidos/pedidos.module';
import { FacturacionModule } from './modules/facturacion/facturacion.module';
import { EstadiasModule } from './modules/estadias/estadias.module';


import { PerfilRestaurantModule } from './modules/perfil-restaurant/perfil-restaurant.module';
import { AbmEmpleadoModule } from './modules/abm-empleado/abm-empleado.module';
import { RolesModule } from './modules/roles/roles.module';
import { ReportesModule } from './modules/reportes/reportes.module';
import { CheckinModule } from './modules/checkin/checkin.module';


import { ClarityModule, ClrSignpostModule } from '@clr/angular';
import { AdminPanelModule } from './modules/admin-panel/admin-panel.module';


// App service
import { ListaEsperaService } from './modules/lista-espera/services/lista-espera.service';
import { SucursalService } from './modules/sucursales/services/sucursal.service';
import { PromocionesService } from './modules/promociones/services/promociones.service';
import { ProductosService } from './modules/productos/services/productos.service';
import { AbmEmpleadoService } from './modules/abm-empleado/service/abm-empleado.service';
import { RolesService } from './modules/roles/services/roles.service';
import { SecurityService } from "./security.service";


// App routes
import { AppRoutingModule } from './app-routing.module';
import { GraphQLModule } from './graphql.module';
import { CajaModule } from './modules/caja/caja.module';

//Sweet Alert
import swal from 'sweetalert2';


@NgModule({
    declarations: [
        AppComponent,
        
    ],
    imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        ClarityModule,
        ClrSignpostModule,
        ApolloClientModule,
        HttpClientModule,
        ApolloBoostModule,
        ListaEsperaModule,
        AbmClienteModule,
        CartaModule,
        SecurityModule,
        ReservasModule,
        SucursalesModule,
        PromocionesModule,
        CoreModule,
        ProductosModule,
        PedidosModule,
        PerfilRestaurantModule,
        AdminPanelModule,
        AbmEmpleadoModule,
        RolesModule,
        CajaModule,
        ReportesModule,
        FacturacionModule,
        EstadiasModule,
        CheckinModule,
        AppRoutingModule,
    ],
    providers: [ListaEsperaService,ReservasService,SucursalService,ProductosService,PromocionesService,AbmEmpleadoService,RolesService, SecurityService],
    bootstrap: [AppComponent]
})
export class AppModule { 

    constructor(boost: ApolloBoost, private route : Router) {
      
        boost.create({
          uri: "http://localhost:5000/graphql",
          request: async operation => {
            const token = await localStorage.getItem('token');
            // if (token == "" || token == undefined) {
            //     this.route.navigate(['/login']);
            //     return
            // }
            // else{
            operation.setContext({
              headers: {
                Authorization: token ? `JWT ${token}` : ''
              }
            });
        //    }
        }
        })
      }}
