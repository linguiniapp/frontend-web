import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { AuxRoutingModule } from './aux-routing.module';

import { AuxviewComponent } from './components/auxview/auxview.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ConoComponent } from './components/cono/cono.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ClarityModule , ClrFormsNextModule, ClrWizardModule} from '@clr/angular'; 
import {BreadcrumbsModule} from "ng6-breadcrumbs";
import { AyudaComponent } from './components/ayuda/ayuda.component';


@NgModule({
    imports: [
        CommonModule,
        AuxRoutingModule, 
        ClarityModule, ClrFormsNextModule, ClrWizardModule ,BreadcrumbsModule
    ],
    declarations: [
        AuxviewComponent,
        SidebarComponent,
        ConoComponent,
        NavbarComponent,
        AyudaComponent 
    ]
})
export class AuxModule { }
