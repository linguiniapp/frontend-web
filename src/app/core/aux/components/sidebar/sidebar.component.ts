import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../../security/login/auth.service';
import { acciones } from '../../../../modules/roles/services/roles.service';
import { AbmClienteService } from '../../../../modules/abm-cliente/services/abm-cliente.service';




@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

    isOpen = true;
    sucursal = -1;
    numeroReservaActual: string = "0";

    constructor(private router: Router, private authService: AuthService, private service : AbmClienteService) { }

    acciones: acciones[] = [];
    menuList = [];
    accion = [];

    // {
    //     icono: 'home',
    //     titulo: 'Inicio',
    //     tieneAcciones: true,
    //     acciones: [
    //         {
    //         nombre: 'Menú',
    //         url: '/admin-panel'
    //     },]
    // },
    // {
    //     icono: 'access_time',
    //     titulo: 'Pedidos',
    //     tieneAcciones: true,
    //     acciones: [
    //         {
    //             nombre: 'Ver pedidos',
    //             url: '/verPedidos'
    //         },{
    //             nombre: 'Nuevo pedido',
    //             url: '/nuevoPedido'
    //         }
    //     ]
    // },
    // {
    //     icono: 'access_time',
    //     titulo: 'Lista de espera',
    //     tieneAcciones: true,
    //     acciones: [
    //         {
    //             nombre: 'Ver lista de espera',
    //             url: '/lista'
    //         },
    //         {
    //             nombre: 'Agregar a la lista de espera',
    //             url: '/formularioLista',
    //         }
    //     ]
    // },
    // {
    //     icono: 'access_time',
    //     titulo: 'Sucursales',
    //     tieneAcciones: true,
    //     acciones: [
    //         {
    //             nombre: 'Ver sucursales',
    //             url: '/verSucursal'
    //         },
    //         {
    //             nombre: 'Agregar sucursal',
    //             url: '/cargarSucursal'
    //         },
    //         {
    //             nombre: 'Ver sucursales en el mapa',
    //             url: '/verSucursales'
    //         }
    //     ]
    // },{
    //     icono: 'access_time',
    //     titulo: 'Productos',
    //     tieneAcciones: true,
    //     acciones: [
    //         {
    //         nombre: 'Ver productos',
    //         url: '/verProductos'
    //     },{
    //         nombre: 'Agregar productos',
    //         url: '/crearProductos'
    //     }]
    // },{
    //     icono: 'access_time',
    //     titulo: 'Categorías',
    //     tieneAcciones: true,
    //     acciones: [
    //         {
    //         nombre: 'Ver categorías',
    //         url: '/verCategorias'
    //     },{
    //         nombre: 'Agregar categorías',
    //         url: '/crearCategoria'
    //     }]

    // },
    // {
    //     icono: 'access_time',
    //     titulo: 'Carta',
    //     tieneAcciones: true,
    //     acciones: [
    //         {
    //         nombre: 'Ver carta',
    //         url: '/listaCarta'
    //     },
    //     {
    //         nombre: 'Crear carta',
    //         url: '/crearCarta'
    //     }]
    // }
    ngOnInit() {

        let numeroReservaActual = localStorage.getItem("numeroPedido-reserva"); 
        //.split("-")[1];

        if (numeroReservaActual != null) {
            this.numeroReservaActual = numeroReservaActual.split("-")[1];
        }

        this.service.verifUsuario().subscribe(data => {
            this.sucursal = data.sucursal;
            //   if (data.id != null) {
            //     this.idCliente = data.id
            //   }
            });

        // this.authService.getAccionesAsinc().subscribe(data => {

        //         if (this.acciones != []) {
        //         this.acciones = data
                    
        //         } else {
                    
        //         }

        //     });
            this.acciones = JSON.parse(localStorage.getItem("accion")) as acciones[];
            // localStorage.removeItem("accion");
                // this.acciones = data
                if (this.menuList == [] || this.menuList.length == 0) {

                if (this.authService.verificarAccion("ver_panel_admin", this.acciones)) {
                    this.menuList.push({
                        icono: 'home',
                        titulo: 'Panel de configuración',
                        tieneAcciones: true,
                        acciones: [
                            {
                                nombre: 'Menú',
                                url: '/admin-panel'
                            },]
                    })
                }
                else{
                    this.menuList.push({
                        icono: 'home',
                        titulo: 'Menu Principal',
                        tieneAcciones: true,
                        acciones: [
                            {
                                nombre: 'Menú',
                                url: '/cliente-panel'
                            },]
                    })
                }

                this.accion = []
                if (this.authService.verificarAccion("ver_pedidos", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Ver pedidos',
                            url: '/verPedidos/' + this.sucursal
                        }
                    )
                }
                if (this.authService.verificarAccion("ver_pedidos", this.acciones) && this.numeroReservaActual != "0") {
                    this.accion.push(

                        {
                            nombre: 'Ver mi pedido actual',
                            url: '/verCarrito/' + this.numeroReservaActual
                        }
                    )
                }
                if (this.authService.verificarAccion("pedido_presencial", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Nuevo pedido',
                            url: '/nuevoPedido/' + this.sucursal
                        }

                    )
                }

                // pedido_remoto
                // anular_pedido
                if (this.authService.verificarAccion("ver_pedidos", this.acciones) || this.authService.verificarAccion("pedido_presencial", this.acciones)) {
                    this.menuList.push(
                        {
                            icono: 'access_time',
                            titulo: 'Pedidos',
                            tieneAcciones: true,
                            acciones: this.accion
                        }
                    )
                }

                this.accion = []
                // if (this.authService.verificarAccion("ver_pedidos", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Ver estadias',
                            url: '/estadias/' + this.sucursal
                        }
                    )
                // }

                this.menuList.push(
                    {
                        icono: 'access_time',
                        titulo: 'Estadias',
                        tieneAcciones: true,
                        acciones: this.accion
                    }
                )

                this.accion = [];

                this.accion = []
                if (this.authService.verificarAccion("ver_lista_espera", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Ver lista de espera',
                            url: '/lista/'

                        }
                    )
                }
                if (this.authService.verificarAccion("agregar_lista_espera", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Agregar a la lista de espera',
                            url: '/formularioLista'
                        }

                    )
                }

                if (this.authService.verificarAccion("ver_lista_espera", this.acciones) || this.authService.verificarAccion("agregar_lista_espera", this.acciones)) {
                    this.menuList.push(
                        {
                            icono: 'access_time',
                            titulo: 'Lista de espera',
                            tieneAcciones: true,
                            acciones: this.accion
                        }
                    )
                }


                this.accion = [];

                this.accion = []
                if (this.authService.verificarAccion("crear_sucursal", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Agregar sucursal',
                            url: '/cargarSucursal'
                        },
                    )
                }
                if (this.authService.verificarAccion("ver_sucursal", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Ver sucursales',
                            url: '/verSucursal'
                        },

                        {
                            nombre: 'Ver sucursales en el mapa',
                            url: '/verSucursales'
                        }

                    )
                }






                if (this.authService.verificarAccion("crear_sucursal", this.acciones) || this.authService.verificarAccion("ver_sucursal", this.acciones)) {
                    this.menuList.push(

                        {
                            icono: 'access_time',
                            titulo: 'Sucursales',
                            tieneAcciones: true,
                            acciones: this.accion
                        }
                    )
                }



                this.accion = [];

                this.accion = []
                if (this.authService.verificarAccion("ver_productos", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Ver productos',
                            url: '/verProductos'
                        },
                    )
                }
                if (this.authService.verificarAccion("crear_producto", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Agregar productos',
                            url: '/crearProductos'
                        }

                    )
                }



                if (this.authService.verificarAccion("ver_productos", this.acciones) || this.authService.verificarAccion("crear_producto", this.acciones)) {
                    this.menuList.push(
                        {
                            icono: 'access_time',
                            titulo: 'Productos',
                            tieneAcciones: true,
                            acciones: this.accion
                        }
                    )
                }



                this.accion = [];


                if (this.authService.verificarAccion("ver_categorias", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Ver categorías',
                            url: '/verCategorias'
                        },
                    )
                }
                if (this.authService.verificarAccion("crear_categoria", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Agregar categorías',
                            url: '/crearCategoria'
                        }

                    )
                }



                if (this.authService.verificarAccion("ver_categorias", this.acciones) || this.authService.verificarAccion("crear_categoria", this.acciones)) {
                    this.menuList.push(


                        {
                            icono: 'access_time',
                            titulo: 'Categorías',
                            tieneAcciones: true,
                            acciones: this.accion

                        }
                    )
                }

                this.accion = [];


                if (this.authService.verificarAccion("crear_rol", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Agregar Rol',
                            url: '/altaRol'
                        },
                    )
                }
                if (this.authService.verificarAccion("modificar_rol", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Modificar Rol',
                            url: '/editarRol'
                        }

                    )
                }

                if (this.authService.verificarAccion("crear_rol", this.acciones) || this.authService.verificarAccion("modificar_rol", this.acciones)) {
                    this.menuList.push(

                        {
                            icono: 'access_time',
                            titulo: 'Roles',
                            tieneAcciones: true,
                            acciones: this.accion

                        }
                    )
                }



                this.accion = [];


                if (this.authService.verificarAccion("crear_empleado", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Agregar Empleado',
                            url: '/altaEmpleado'
                        },
                    )
                }
                if (this.authService.verificarAccion("ver_empleados", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Ver Empleados',
                            url: '/listaEmpleado'
                        }

                    )
                }

                if (this.authService.verificarAccion("ver_empleados", this.acciones) || this.authService.verificarAccion("crear_empleado", this.acciones)) {
                    this.menuList.push(

                        {
                            icono: 'access_time',
                            titulo: 'Empleados',
                            tieneAcciones: true,
                            acciones: this.accion

                        }
                    )
                }


                this.accion = [];

                if (this.authService.verificarAccion("crear_promocion", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Agregar Promocion',
                            url: '/crearPromo'
                        },
                    )
                }
                if (this.authService.verificarAccion("ver_promociones", this.acciones)) {
                    this.accion.push(

                        {
                            nombre: 'Ver Promociones',
                            url: '/verPromo'
                        }

                    )
                }

                if (this.authService.verificarAccion("crear_promocion", this.acciones) || this.authService.verificarAccion("ver_promociones", this.acciones)) {
                    this.menuList.push(

                        {
                            icono: 'access_time',
                            titulo: 'Promociones',
                            tieneAcciones: true,
                            acciones: this.accion

                        }
                    )
                }

                this.accion = [];

                this.accion.push(

                    {
                        nombre: 'Agregar Recompensa',
                        url: '/crearRecompensa'
                    },
                )
                this.accion.push(

                    {
                        nombre: 'Ver Recompensas',
                        url: '/verRecompensas'
                    },
                )



                this.menuList.push(

                    {
                        icono: 'access_time',
                        titulo: 'Recompensas',
                        tieneAcciones: true,
                        acciones: this.accion

                    }
                )

                // this.accion = [];


                // if (this.authService.verificarAccion("ver_caja")) {
                //     this.accion.push(

                //         {
                //             nombre: 'Ver Cajas',
                //             url: '/ver'
                //         },
                //     )
                // }
                // if (this.authService.verificarAccion("ver_empleados")) {
                //     this.accion.push(

                //         {
                //             nombre: 'Ver Empleados',
                //             url: '/listaEmpleado'
                //         }

                //     )
                // }

                // if (this.authService.verificarAccion("ver_empleados") || this.authService.verificarAccion("crear_empleado") ) {
                //     this.menuList.push(

                //         {
                //             icono: 'access_time',
                //             titulo: 'Empleados',
                //             tieneAcciones: true,
                //             acciones: this.accion

                //         }
                //     )
                // }
                if (this.authService.verificarAccion("", this.acciones)) {
                    this.menuList.push(

                    )
                }





            }


        // });

    }


    // {
    //     icono: 'access_time',
    //         titulo: 'Carta',
    //             tieneAcciones: true,
    //                 acciones: [
    //                     {
    //                         nombre: 'Ver carta',
    //                         url: '/listaCarta'
    //                     },
    //                     {
    //                         nombre: 'Crear carta',
    //                         url: '/crearCarta'
    //                     }]
    // }





    // handleClick(matExpansionPanel: MatExpansionPanel, menu: any, event: Event): void {
    //     event.stopPropagation();
    //     if (!menu.tieneAcciones) {
    //         matExpansionPanel.close();
    //     }
    // }

    // private _isExpansionIndicator(target: EventTarget): boolean {
    //     const expansionIndicatorClass = 'mat-expansion-indicator';
    //     return ((<HTMLElement>target).classList && (<HTMLElement>target).classList.contains(expansionIndicatorClass));
    // }
}
