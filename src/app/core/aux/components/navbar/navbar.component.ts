import {Component, OnInit, Output, EventEmitter, ViewChild, OnDestroy} from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { AbmClienteService } from '../../../../modules/abm-cliente/services/abm-cliente.service';
import { AuthService } from '../../../../security/login/auth.service';



import { ClrWizard } from '@clr/angular';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  servicio: any;
  @Output() menuButtonClicked = new EventEmitter();

  constructor(private route: Router, private service: AbmClienteService, private authService: AuthService) { }

  ngOnInit() {
    this.servicio = this.service.verifCliente().subscribe(data => {
    this.nombreUsuario = data.nombre;
      if (data.id != null) {
        this.idCliente = data.id;
      }
    });
  }
  ngOnDestroy(): void {
    this.servicio.unsubscribe();
  }

  menuButtonClick() {
    this.menuButtonClicked.emit();
  }
  @ViewChild("wizardlg") wizardLarge: ClrWizard;
  mdOpen: boolean = false;
  lgOpen: boolean = false;
  xlOpen: boolean = false;
  nombreUsuario: string = "";
  idCliente: number = null;

  open() {
    this.route.navigate(['/verPerfil']);
  }
  redirAyuda(){
    this.route.navigate(['/ayuda']);
  }
  cerrarSesion() {

    this.authService.logout();

  }
}
