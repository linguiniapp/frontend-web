import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuxviewComponent } from './auxview.component';

describe('AuxviewComponent', () => {
  let component: AuxviewComponent;
  let fixture: ComponentFixture<AuxviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuxviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuxviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
