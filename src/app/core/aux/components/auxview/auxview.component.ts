import { Component, OnInit } from '@angular/core';

@Component({
    templateUrl: './auxview.component.html',
    styleUrls: ['./auxview.component.css']
})
export class AuxviewComponent implements OnInit {

    sidebarOpen: boolean;

    constructor() { }

    ngOnInit() {
    }

    sidebarChangeState() {
        this.sidebarOpen = !this.sidebarOpen;
    }
}
