import { Component, OnInit } from '@angular/core';
import {take} from 'rxjs/operators';
import {PerfilService} from '../../../../modules/perfil-restaurant/perfil.service';

@Component({
  selector: 'app-ayuda',
  templateUrl: './ayuda.component.html',
  styleUrls: ['./ayuda.component.css']
})
export class AyudaComponent implements OnInit {

  constructor(private _perfilService: PerfilService) {
  }

  obs1: any;
  tipoUsuario: string;

  ngOnInit() {
    this.obs1 = this._perfilService.getTipoUsuario().pipe(take(1)).subscribe(tipoUser => {
      this.tipoUsuario = tipoUser;
    });
  }
}
