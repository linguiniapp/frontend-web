import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthService } from './../../security/login/auth.service';
import { AuxviewComponent } from './components/auxview/auxview.component';
import { ConoComponent } from './components/cono/cono.component';
import { NgIf } from '@angular/common';
import {AyudaComponent} from './components/ayuda/ayuda.component';

const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'ayuda',
                component: AyudaComponent,
                data: {
                    breadcrumb: 'Ayuda y Preguntas frecuentes'
                }
            },
            {
                path: 'cono',
                component: ConoComponent
            }
        ],

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuxRoutingModule { }
