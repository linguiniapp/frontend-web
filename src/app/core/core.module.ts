import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuxModule } from './aux/aux.module';


@NgModule({
  imports: [
    CommonModule,
    AuxModule,


  ],
  declarations: []
})
export class CoreModule { }
