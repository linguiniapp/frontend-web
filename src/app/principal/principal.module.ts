import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VistaComponent } from './components/vista/vista.component';

import { PrincipalRoutingModule } from './principal-routing.module';

import { ClarityModule, ClrFormsNextModule } from '@clr/angular';

@NgModule({
  imports: [
    CommonModule,
    PrincipalRoutingModule,
    ClarityModule,
    ClrFormsNextModule
    
  ],
  declarations: [VistaComponent]
})
export class PrincipalModule { }
