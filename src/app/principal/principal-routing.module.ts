import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { VistaComponent } from '../principal/components/vista/vista.component';

import { AuxviewComponent } from '../core/aux/components/auxview/auxview.component';

const routes: Routes = [
    {
        path: '',
        component: AuxviewComponent,
        children: [
            {
                path: 'menu',
                component: VistaComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PrincipalRoutingModule { }
