import { Injectable, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Apollo, gql } from 'apollo-angular-boost';
import { acciones } from '../../modules/roles/services/roles.service';
import { Observable } from 'rxjs';
// import { AppComponent } from '../../app.component';


import { Subject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})


export class AuthService implements CanActivate {


  listaAcciones = new Subject<acciones[]>();
  response = new Subject<boolean>();
  acciones: acciones[];
  constructor(private router: Router, private apollo: Apollo) {
    // this.getAccionesAsinc().subscribe(data => {
    //   this.acciones = data;

    // })
  }

  ngOnInit() {

  }


  canActivate(route: ActivatedRouteSnapshot) {


    let roles = route.data["acciones"] as Array<string>;
    const token = localStorage.getItem('token');

    if (token == "" || token == undefined) {
      this.router.navigate(['/login']);
      return false;

    }


    else {

      if (this.acciones == null || this.acciones == [] || this.acciones.length == 0) {

        this.acciones = JSON.parse(localStorage.getItem("accion")) as acciones[];
        // localStorage.removeItem("accion");


        if (this.getAcciones(roles)) {
          return true;

        } else {
          console.log("no posee los permisos necesarios")
          this.router.navigate(['/errorPermiso']);
          return false;


        }


      }

      else {

        if (this.getAcciones(roles)) {
          return true;

        } else {
          console.log("no posee los permisos necesarios")
          this.router.navigate(['/errorPermiso']);
          return false;

        }
      }

    }


  }




  // getAllAcciones(): acciones[] {
  //   if (this.acciones == null || this.acciones == [] || this.acciones.length == 0) {
  //     this.crearAcciones();
  //   }
  //   return this.acciones;
  // }

  getAcciones(acciones: Array<string>): boolean {
    let ok: boolean = true;
    if (this.acciones == null || this.acciones == [] || this.acciones.length == 0) {
      

        this.acciones = JSON.parse(localStorage.getItem("accion")) as acciones[];
      // this.crearAcciones();

      return this.veridActivate(acciones);
    } else {
      return this.veridActivate(acciones);
    }

  }

  veridActivate(acciones: Array<string>): boolean {
    let ok = true;
    // this.acciones = [];
    // console.log(JSON.parse(localStorage.getItem("accion")) as acciones[]);
    // console.log(localStorage.getItem("token"))
    // console.log(this.acciones)
    if (this.acciones == null || this.acciones == [] || this.acciones.length == 0) {
      

      this.acciones = JSON.parse(localStorage.getItem("accion")) as acciones[];
    }

    acciones.forEach(elem => {

      let existe = this.acciones.find(d => d.nombre == elem)
      if (existe) {
      }
      else {
        ok = false;
      }
    });
    return ok;
  }

  verificarAccion(nombre: string, acciones: acciones[]): boolean {
    let existe = acciones.find(d => d.nombre == nombre);
    if (existe) {
      return true;
    } else {
      return false;
    }
  }


  getAccionesAsinc(): Observable<acciones[]> {
    this.apollo.watchQuery<any>({
      query: gql
        `{
          verPermisos{
            edges{
              node{
                nombre
                codigo
              }
            }
          }
        }`, fetchPolicy: 'no-cache'

    })
      .valueChanges
      .subscribe(({ data }) => {
        let acciones: acciones[] = [];
        data.verPermisos.edges.forEach(p => {
          let lelem: acciones = {
            nombre: p.node.nombre,
            codigo: p.node.codigo
          };

          acciones.push(lelem);
        });

        this.listaAcciones.next(acciones);

      });
    return this.listaAcciones.asObservable();
  }

  crearAcciones1() {
    this.acciones = [
      { codigo: 1, nombre: "ver_roles" },
      { codigo: 2, nombre: "ver_acciones" },
      { codigo: 3, nombre: "crear_rol" },
      { codigo: 4, nombre: "modificar_rol" },
      { codigo: 5, nombre: "eliminar_rol" },
      { codigo: 6, nombre: "crear_carta" },
      { codigo: 7, nombre: "ver_caja" },
      { codigo: 8, nombre: "abrir_caja" },
      { codigo: 9, nombre: "cerrar_caja" },
      { codigo: 10, nombre: "crear_sucursal" },
      { codigo: 11, nombre: "modificar_sucursal" },
      { codigo: 13, nombre: "ver_clientes" },
      { codigo: 14, nombre: "modificar_cliente" },
      { codigo: 15, nombre: "deshabilitar_cliente" },
      { codigo: 16, nombre: "suspender_cliente" },
      { codigo: 17, nombre: "ver_pedidos" },
      { codigo: 18, nombre: "pedido_remoto" },
      { codigo: 19, nombre: "pedido_presencial" },
      { codigo: 20, nombre: "ver_reserva" },
      { codigo: 21, nombre: "crear_reserva" },
      { codigo: 22, nombre: "modificar_reserva" },
      { codigo: 23, nombre: "anular_reserva" },
      { codigo: 24, nombre: "consumir_reserva" },
      { codigo: 25, nombre: "ver_empleados" },
      { codigo: 26, nombre: "crear_empleado" },
      { codigo: 27, nombre: "modificar_empleado" },
      { codigo: 28, nombre: "deshabilitar_empleado" },
      { codigo: 29, nombre: "suspender_empleado" },
      { codigo: 30, nombre: "habilitar_empleado" },
      { codigo: 31, nombre: "asignar_rol" },
      { codigo: 32, nombre: "quitar_rol" },
      { codigo: 33, nombre: "anular_pedido" },
      { codigo: 34, nombre: 'check_in' },
      { codigo: 35, nombre: 'check_out' },
      { codigo: 36, nombre: 'ver_panel_admin' },
      { codigo: 37, nombre: 'ver_lista_espera' },
      { codigo: 38, nombre: 'agregar_lista_espera' },
      { codigo: 39, nombre: 'quitar_lista_espera' },
      { codigo: 40, nombre: 'ver_sucursal' },
      { codigo: 41, nombre: 'modificar_sucursal' },
      { codigo: 42, nombre: 'cambiar_estado_sucursal' },
      { codigo: 43, nombre: 'ver_categorias' },
      { codigo: 44, nombre: 'crear_categoria' },
      { codigo: 45, nombre: 'modificar_categoria' },
      { codigo: 46, nombre: 'eliminar_categoria' },
      { codigo: 47, nombre: 'ver_productos' },
      { codigo: 48, nombre: 'crear_producto' },
      { codigo: 49, nombre: 'modificar_producto' },
      { codigo: 50, nombre: 'deshabilitar_producto' },


    ];
  }

  getAccionesAsinc1() {
    console.log("asinc1")
    // localStorage.removeItem("accion");
    this.apollo.watchQuery<any>({
      query: gql
        `{
          verPermisos{
            edges{
              node{
                nombre
                codigo
              }
            }
          }
        }`, fetchPolicy: 'no-cache'

    })
      .valueChanges
      .subscribe(({ data }) => {
        let acciones: acciones[] = [];
        this.acciones = [];
        data.verPermisos.edges.forEach(p => {
          let lelem: acciones = {
            nombre: p.node.nombre,
            codigo: p.node.codigo
          };


          this.acciones.push(lelem);
        });
    localStorage.removeItem("accion");

        localStorage.setItem("accion", JSON.stringify(this.acciones))

        // console.log(this.acciones)
      });
  }


  logout() {
    localStorage.clear();
    this.acciones = [];
    this.router.navigate(['/login']);
  }
}
