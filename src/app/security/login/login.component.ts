import { Component, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular-boost';
import { Router } from '@angular/router';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';

import { AuthService } from './auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  isInvalid: boolean = false;
  complete : boolean = false;
  isRecupero: boolean = false;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);



  constructor(private routes: Router, private apollo: Apollo, private auth: AuthService) { }

  ngOnInit() {
  }


  login() {
    this.isRecupero = false;
    this.isInvalid = false;
    this.complete =false;

    if (this.password == undefined || this.username == undefined || this.password == "" || this.username == "") {
      this.complete =true;
    
    }
    else {
      this.apollo
        .mutate({
          mutation: gql
            `mutation {
      login(username: "${this.username}", password: "${this.password}") {
        ok
        token
      }
    }`
          ,
        })
        .subscribe(({ data }) => {

          if(data.login.ok == false){
            this.isInvalid = true;

          }else{
            console.log("appcomponent")
            this.auth.getAccionesAsinc1();
              localStorage.setItem('token', data.login.token);

              if(this.username == 'admin'){
                this.routes.navigate(['/admin-panel']);
              }
              else{
                this.routes.navigate(['/cliente-panel']);
              }
          }

        }, (error) => {

          this.isInvalid = true;
          console.log('there was an error sending the query', error);
        });

    }
  }

  recuperar(){
  this.isRecupero = !this.isRecupero;
  this.routes.navigate(["/recuperarPassword"])
  }

registrarse(){
  this.routes.navigate(['/altaCliente']);

}
recuperarPassword(){
  this.routes.navigate(['/recuperarPassword']);
}

  enviar(){
    if(this.emailFormControl.valid){
    this.isRecupero = !this.isRecupero;
    }
  }


}
