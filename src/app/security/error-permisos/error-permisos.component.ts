import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router'; 
@Component({
  selector: 'app-error-permisos',
  templateUrl: './error-permisos.component.html',
  styleUrls: ['./error-permisos.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ErrorPermisosComponent  {

  complete : boolean = false;

  constructor(private routes: Router) { }
  volver(){
    localStorage.clear();
    // this.acciones = [];
    this.routes.navigate(['/login']);
  }
  volverMenu(){
    this.routes.navigate(['/']);
  }
  cerrar(){
    this.routes.navigate(['/']);
  }

}
