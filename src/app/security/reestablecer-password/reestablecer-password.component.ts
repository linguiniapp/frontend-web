import { Component, OnInit } from '@angular/core';
import { FormControl, AbstractControl, Validators, FormGroup } from '@angular/forms';
import { SecurityService } from 'src/app/security.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reestablecer-password',
  templateUrl: './reestablecer-password.component.html',
  styleUrls: ['./reestablecer-password.component.css']
})
export class ReestablecerPasswordComponent implements OnInit {
  private token: string; 
   passCambiada: boolean;
   error: boolean;
  
  reestablecerGroup = new FormGroup({
    pass1: new FormControl('', [Validators.minLength(8), Validators.required]),
    pass2: new FormControl('', [Validators.minLength(8), Validators.required])
  },{  validators: ValidadorPasswords});

  constructor(private router: Router, private _securityService: SecurityService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.token = this.activatedRoute.snapshot.params.token;
  }
  onSubmit(){
    this._securityService.setearNuevaPassword(this.token, this.reestablecerGroup.get('pass1').value).subscribe(
      data => {
        if(data == true){
          this.passCambiada = true;
          this.error = false;
        }else{
          this.passCambiada = false;
          this.error = true;
        }
      }
    )
  }
}
export function ValidadorPasswords(control: AbstractControl) {
  if (control.get('pass1').value == control.get('pass2').value) {
    return  null ;
  }else{
    control.setErrors({ passwordsDistintas: true })
    return {passwordsDistintas: true};
  }
}