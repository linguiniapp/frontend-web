import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorPermisosComponent } from './error-permisos/error-permisos.component';
import { RecuperarPasswordComponent } from './recuperar-password/recuperar-password.component';
import { ClarityModule, ClrFormsNextModule, ClrAlertModule } from '@clr/angular';
import { SecurityService } from '../security.service';
import { ReestablecerPasswordComponent } from './reestablecer-password/reestablecer-password.component';
import {Routes, RouterModule} from '@angular/router';


@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
     ReactiveFormsModule,
     ClarityModule,
     ClrFormsNextModule,
     ClrAlertModule
  ],
  declarations: [LoginComponent, ErrorPermisosComponent, RecuperarPasswordComponent, ReestablecerPasswordComponent]
})
export class SecurityModule { }
