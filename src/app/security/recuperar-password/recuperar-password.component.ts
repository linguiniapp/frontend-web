import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm, Validators, FormGroup } from '@angular/forms';
import { SecurityService } from 'src/app/security.service';

@Component({
  selector: 'app-recuperar-password',
  templateUrl: './recuperar-password.component.html',
  styleUrls: ['./recuperar-password.component.css']
})
export class RecuperarPasswordComponent implements OnInit {
  recuperarGroup = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required])
  });
  errorEmail: boolean ;
  emailEnviado: boolean;
  constructor(private _securityService: SecurityService) { }

  ngOnInit() {
    this.emailEnviado = false;
    this.errorEmail = false;
  }
  onSubmit(){
    this._securityService.enviarMail(this.recuperarGroup.get('email').value) 
    .subscribe( 
      data => {
        if(data == true){
          this.emailEnviado = true;
          this.errorEmail = false;
        }else{
          this.errorEmail = true;
          this.emailEnviado = false;
        }
    });
  }
}
