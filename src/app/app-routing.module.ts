import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClientApolloComponent } from './modules/apollo-client/client-apollo/client-apollo.component';
import { AltaClienteComponent } from './modules/abm-cliente/components/alta-cliente/alta-cliente.component';

import { LoginComponent } from './security/login/login.component';

import { ErrorPermisosComponent } from './security/error-permisos/error-permisos.component';
import { AuthService } from './security/login/auth.service';
import { AppComponent } from './app.component';
import { RecuperarPasswordComponent } from './security/recuperar-password/recuperar-password.component';
import { ReestablecerPasswordComponent } from './security/reestablecer-password/reestablecer-password.component';



const APP_ROUTES: Routes = [

    { path : 'apollo' , component: ClientApolloComponent},

    { path : 'login' , component: LoginComponent},          {
        path: 'altaCliente',
        component: AltaClienteComponent
    },
    {
        path: 'resetPassword/:token',
        component: ReestablecerPasswordComponent
    },
    {
        path: 'recuperarPassword',
        component: RecuperarPasswordComponent
    },
    {
        path: 'errorPermiso',
        component: ErrorPermisosComponent
    },
    // {
    //     path: '/',
    //      canActivate: [AuthService],
    //     data: {acciones: ["eliminar_categoria"]}
 
    // },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login'
        ,
    //      canActivate: [AuthService],
    //     data: {acciones: ["eliminar_categoria"]}

    },
    { path: '**', redirectTo: '/login', pathMatch: 'full'},

];

@NgModule({
    imports: [RouterModule.forRoot(APP_ROUTES)],
    exports: [RouterModule]
})
export class AppRoutingModule {}



